﻿using MeetingRoom.VDI.Core.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.Helper
{
    public static class SortingAndPaging
    {
        public static T[] ApplySortingPaging<T>(this IOrderedQueryable<T> query, SortingPaging sortingPaging)
        {
            var firstPass = true;
            foreach (var sortOrder in sortingPaging.SortOrders)
            {
                if (firstPass)
                {
                    firstPass = false;
                    query = sortOrder.ColumnOrder == Core.Helper.SortOrder.OrderBy.Ascending
                                ? query.OrderBy(sortOrder.ColumnName) :
                                  query.OrderByDescending(sortOrder.ColumnName);
                }
                else
                {
                    query = sortOrder.ColumnOrder == Core.Helper.SortOrder.OrderBy.Ascending
                                ? query.ThenBy(sortOrder.ColumnName) :
                                  query.ThenByDescending(sortOrder.ColumnName);
                }
            }

            var result = query.Skip((sortingPaging.PageNumber - 1) *
              sortingPaging.NumberRecords).Take(sortingPaging.NumberRecords).ToArray();

            return result;
        }
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder<T>(source, property, "OrderBy");
        }
        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder<T>(source, property, "OrderByDescending");
        }
        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string property)
        {
            return ApplyOrder<T>(source, property, "ThenBy");
        }
        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string property)
        {
            return ApplyOrder<T>(source, property, "ThenByDescending");
        }
        static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> source, string property, string methodName)
        {
            string[] props = property.Split('.');
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach (string prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                PropertyInfo pi = type.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);

            object result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == methodName
                            && method.IsGenericMethodDefinition
                            && method.GetGenericArguments().Length == 2
                            && method.GetParameters().Length == 2)
                    .MakeGenericMethod(typeof(T), type)
                    .Invoke(null, new object[] { source, lambda });
            return (IOrderedQueryable<T>)result;
        }

    }
}
