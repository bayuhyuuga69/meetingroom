﻿using AutoMapper;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using MeetingRoom.VDI.Infrastructure.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class UserService : IUser
    {
        public UserService()
        {
            Mapper.CreateMap<MstUser, User>();
            Mapper.CreateMap<User, MstUser>();
        }

        public User Create(User user)
        {
            var context = new MeetingRoomEntities();

            var newUser = Mapper.Map<User, MstUser>(user);
            newUser.passwordSalt = user.surName;
            newUser.Password = SecurityHelper.GenerateHashWithSalt(user.Password, newUser.passwordSalt);
            newUser.lastChangePassword = DateTime.Now;

            context.MstUsers.Add(newUser);
            context.SaveChanges();

            return Mapper.Map<MstUser, User>(newUser);
        }

        public void Delete(int id)
        {
            var context = new MeetingRoomEntities();
            var oldUser = context.MstUsers.FirstOrDefault(w => w.Id == id);

            context.MstUsers.Remove(oldUser);
            context.Entry(oldUser).State = System.Data.Entity.EntityState.Deleted;
            context.SaveChanges();
        }

        public PagedData<User> GetAll(List<User> filter, User search, int pageIndex = 1, int pageSize = 10)
        {
            var context = new MeetingRoomEntities();

            var data = context.MstUsers.AsQueryable();

            if (search != null)
            {
                data = data.Where(w => w.userName.ToLower().Contains(search.userName.ToLower()));
            }

            var result = new PagedData<User>
            {
                TotalRecord = data.Count(),
                CurrentIndex = pageIndex,
                Data = Mapper.Map<IEnumerable<MstUser>, IEnumerable<User>>(data.OrderBy(o => o.userName).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList().AsEnumerable())
            };

            return result;
        }

        public IEnumerable<User> GetAll(int pageIndex = 1, int pageSize = 10)
        {
            var context = new MeetingRoomEntities();
            var data = context.MstUsers.AsQueryable();

            return Mapper.Map<IEnumerable<MstUser>, IEnumerable<User>>(data);
        }

        public User GetUserByEmail(string email)
        {
            User returnUser = null;

            using (var context = new MeetingRoomEntities())
            {
                var user = context.MstUsers.FirstOrDefault(w => w.Email == email);

                if (user != null)
                {
                    returnUser = Mapper.Map<MstUser, User>(user);
                }
            }

            return returnUser;
        }

        public User GetUserById(int userId)
        {
            User returnUser = null;

            using (var context = new MeetingRoomEntities())
            {
                MstUser user = context.MstUsers.FirstOrDefault(w => w.Id == userId);

                if (user != null)
                {
                    returnUser = Mapper.Map<MstUser, User>(user);
                }
            }

            return returnUser;
        }

        public User GetUserByName(string userName)
        {
            var context = new MeetingRoomEntities();
            var user = context.MstUsers.FirstOrDefault(w => w.userName == userName);

            return Mapper.Map<MstUser, User>(user);
        }

        public bool IsUserExist(string email, string password)
        {
            var context = new MeetingRoomEntities();
            var user = context.MstUsers.FirstOrDefault(w => w.Email == email);
            if (user != null)
            {
                string Password = SecurityHelper.GenerateHashWithSalt(password, user.passwordSalt);
                user = context.MstUsers.FirstOrDefault(w => w.Email == email && w.Password.Equals(Password));

                if (user != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void Update(User user)
        {
            using (var context = new MeetingRoomEntities())
            {
                var userModel = context.MstUsers.FirstOrDefault(w => w.Id == user.Id);

                userModel.passwordSalt = user.userName;
                userModel.Password = SecurityHelper.GenerateHashWithSalt(user.Password, userModel.passwordSalt);
                userModel.lastChangePassword = DateTime.Now;

                context.MstUsers.Attach(userModel);
                context.Entry(userModel).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
