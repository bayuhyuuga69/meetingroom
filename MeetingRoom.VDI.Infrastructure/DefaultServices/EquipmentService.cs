﻿using AutoMapper;
using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class EquipmentService : IEquipment
    {
        public EquipmentService()
        {
            Mapper.CreateMap<MstMeetingRoomEquipment, Equipment>();
            Mapper.CreateMap<Equipment, MstMeetingRoomEquipment>();
        }

        public Equipment Create(Equipment equipment)
        {
            var context = new MeetingRoomEntities();

            var newEquipment = Mapper.Map<Equipment, MstMeetingRoomEquipment>(equipment);

            newEquipment.IsDeleted = false;
            newEquipment.CreationTime = System.DateTime.Now;

            context.MstMeetingRoomEquipments.Add(newEquipment);
            context.SaveChanges();

            return Mapper.Map<MstMeetingRoomEquipment, Equipment>(newEquipment);
        }

        public void Delete(int id)
        {
            var context = new MeetingRoomEntities();
            var oldEquipment = context.MstMeetingRoomEquipments.FirstOrDefault(w => w.Id == id);

            oldEquipment.IsDeleted = true;
            oldEquipment.DeletionTime = System.DateTime.Now;

            context.MstMeetingRoomEquipments.Attach(oldEquipment);
            context.Entry(oldEquipment).State = System.Data.Entity.EntityState.Modified;

            context.SaveChanges();

            //context.MstMeetingRoomEquipments.Remove(oldEquipment);
            //context.Entry(oldEquipment).State = System.Data.Entity.EntityState.Deleted;
            //context.SaveChanges();
        }

        public PagedData<Equipment> GetAll(List<Equipment> filter, Equipment search, int pageIndex = 1, int pageSize = GlobalSetting.DEFAULT_PAGESIZE)
        {
            var context = new MeetingRoomEntities();
            var data = context.MstMeetingRoomEquipments.AsQueryable();//.OrderBy(o => o.Id).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList().AsEnumerable();

            if (filter != null)
            {
                var filterId = filter.Where(w => w.Id > 0).Select(s => s.Id).ToArray();
                //var filterBrandId = filter.Where(w => w.Id > 0).Select(s => s.Id).ToArray();

                if (filterId.Any()) //Tipe integer
                    data = data.Where(w => filterId.Contains(w.Id));

                //if (filterBrandId.Any()) //Tipe integer
                //    data = data.Where(w => w.BrandId != null && filterBrandId.Contains(w.BrandId));
            }

            if (search != null)
            {
                data = data.Where(w => w.meetingRoomEquipmentTitle.ToLower().Contains(search.meetingRoomEquipmentTitle.ToLower()) ||
                    (w.meetingRoomEquipmentDescription != null && w.meetingRoomEquipmentDescription.ToLower().Contains(search.meetingRoomEquipmentDescription.ToLower()))
                    );
            }

            var result = new PagedData<Equipment>
            {
                TotalRecord = context.MstMeetingRoomEquipments.Count(),
                CurrentIndex = pageIndex,
                //Data = Mapper.Map<IEnumerable<M_ProductSubBrand>, IEnumerable<ProductSubBrand>>(data)
                Data = Mapper.Map<IEnumerable<MstMeetingRoomEquipment>, IEnumerable<Equipment>>(data.OrderBy(o => o.Id).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList().AsEnumerable())
            };

            return result;
        }

        public IEnumerable<Equipment> GetAll(int pageIndex = 1, int pageSize = 10000)
        {
            var context = new MeetingRoomEntities();
            var data = context.MstMeetingRoomEquipments.AsQueryable().Where(w => w.IsDeleted == false);//OrderBy(o => o.Id).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList().AsEnumerable();
            
            return Mapper.Map<IEnumerable<MstMeetingRoomEquipment>, IEnumerable<Equipment>>(data);
        }

        public int GetAllEquipment()
        {
            var context = new MeetingRoomEntities();
            var data = context.MstMeetingRoomEquipments.AsQueryable();

            return data.Count();
        }

        public Equipment GetById(int id)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstMeetingRoomEquipments.FirstOrDefault(w => w.Id == id);

            return Mapper.Map<MstMeetingRoomEquipment, Equipment>(result);
        }

        public Equipment GetByName(string name)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstMeetingRoomEquipments.FirstOrDefault(w => w.meetingRoomEquipmentTitle == name);

            return Mapper.Map<MstMeetingRoomEquipment, Equipment>(result);
        }

        public IEnumerable<Equipment> GetEquipment()
        {
            var context = new MeetingRoomEntities();
            return context.MstMeetingRoomEquipments
                           .OrderBy(x => x.Id)
                           .Select(x => new Equipment { Id = x.Id, meetingRoomEquipmentTitle = x.meetingRoomEquipmentTitle })
                           .ToList();
        }

        public EquipmentPrice GetPriceEquipment(int Id, int Qty)
        {
            var context = new MeetingRoomEntities();

            var equip = new EquipmentPrice();

            var data = (from a in context.MstMeetingRoomEquipments
                        where
                          a.Id == Id
                        select new EquipmentPrice
                        {
                            Id = a.Id,
                            meetingRoomEquipmentDescription = a.meetingRoomEquipmentDescription,
                            meetingRoomEquipmentPrice = a.meetingRoomEquipmentPrice,
                            meetingRoomEquipmentTitle = a.meetingRoomEquipmentTitle
                        });

            foreach (var item in data)
            {
                equip.Id = item.Id;
                equip.meetingRoomEquipmentDescription = item.meetingRoomEquipmentDescription;
                equip.meetingRoomEquipmentPrice = item.meetingRoomEquipmentPrice * Qty;
                equip.meetingRoomEquipmentTitle = item.meetingRoomEquipmentTitle;
                equip.qty = Qty;
            }

            return equip;
        }

        public void Update(Equipment equipment)
        {
            var context = new MeetingRoomEntities();
            var oldEquipment = context.MstMeetingRoomEquipments.FirstOrDefault(w => w.Id == equipment.Id);

            equipment.IsDeleted = false;
            equipment.CreationTime = oldEquipment.CreationTime;

            Mapper.Map(equipment, oldEquipment);
            oldEquipment.LastModificationTime = DateTime.Now;

            context.MstMeetingRoomEquipments.Attach(oldEquipment);
            context.Entry(oldEquipment).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
