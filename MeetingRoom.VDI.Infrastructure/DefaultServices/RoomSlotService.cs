﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class RoomSlotService : IRoomSlot
    {
        public RoomSlotService()
        {
            Mapper.CreateMap<MstRoomSlot, RoomSlot>();
            Mapper.CreateMap<RoomSlot, MstRoomSlot>();
        }

        public RoomSlot Create(RoomSlot roomSlot)
        {
            var context = new MeetingRoomEntities();

            var newSlot = Mapper.Map<RoomSlot, MstRoomSlot>(roomSlot);

            context.MstRoomSlots.Add(newSlot);
            context.SaveChanges();

            return Mapper.Map<MstRoomSlot, RoomSlot>(newSlot);
        }

        public void Delete(string month, string year, string day, string roomId)
        {
            var context = new MeetingRoomEntities();

            int m = Convert.ToInt32(month);
            int y = Convert.ToInt32(year);
            int d = Convert.ToInt32(day);
            int r = Convert.ToInt32(roomId);

            var roomData = (from MstRoomSlot in context.MstRoomSlots
                            where
                              MstRoomSlot.month == m &&
                              MstRoomSlot.year == y &&
                              MstRoomSlot.days == d &&
                              MstRoomSlot.roomId == r
                            select new RoomSlot
                            {
                                Id = MstRoomSlot.Id,
                                roomId = MstRoomSlot.roomId,
                                layoutId = MstRoomSlot.layoutId,
                                capacity = MstRoomSlot.capacity,
                                month = MstRoomSlot.month,
                                year = MstRoomSlot.year,
                                days = MstRoomSlot.days,
                                hour1 = MstRoomSlot.hour1,
                                hour2 = MstRoomSlot.hour2,
                                hour3 = MstRoomSlot.hour3,
                                hour4 = MstRoomSlot.hour4,
                                hour5 = MstRoomSlot.hour5,
                                hour6 = MstRoomSlot.hour6,
                                hour7 = MstRoomSlot.hour7,
                                hour8 = MstRoomSlot.hour8,
                                hour9 = MstRoomSlot.hour9,
                                hour10 = MstRoomSlot.hour10,
                                hour11 = MstRoomSlot.hour11,
                                hour12 = MstRoomSlot.hour12,
                                hour13 = MstRoomSlot.hour13,
                                isActive = MstRoomSlot.isActive
                            });

            var roomSlot = new RoomSlot();
            foreach (var data in roomData)
            {
                roomSlot.Id = data.Id;
            }

            var oldRoom = context.MstRoomSlots.FirstOrDefault(w => w.Id == roomSlot.Id);

            oldRoom.isActive = 0;

            context.MstRoomSlots.Attach(oldRoom);
            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public IEnumerable<RoomSlot> GetAll(int pageIndex = 1, int pageSize = 10)
        {
            var context = new MeetingRoomEntities();

            var data = (from a in context.MstRoomSlots
                        join b in context.MstMeetingRooms on new { roomId = a.roomId } equals new { roomId = b.Id } into b_join
                        from b in b_join.DefaultIfEmpty()
                        join c in context.MstMeetingRoomLayouts on new { LayoutId = (int)b.LayoutId } equals new { LayoutId = c.Id } into c_join
                        from c in c_join.DefaultIfEmpty()
                        where a.isActive == 1
                        select new RoomSlot
                        {
                            Id = a.Id,
                            roomId = a.roomId,
                            layoutId = a.layoutId,
                            capacity = a.capacity,
                            month = a.month,
                            year = a.year,
                            days = a.days,
                            hour1 = a.hour1,
                            hour2 = a.hour2,
                            hour3 = a.hour3,
                            hour4 = a.hour4,
                            hour5 = a.hour5,
                            hour6 = a.hour6,
                            hour7 = a.hour7,
                            hour8 = a.hour8,
                            hour9 = a.hour9,
                            hour10 = a.hour10,
                            hour11 = a.hour11,
                            hour12 = a.hour12,
                            hour13 = a.hour13,
                            isActive = a.isActive,
                            roomTitle = b.MeetingRoomTitle,
                            layoutTitle = c.meetingRoomLayoutTitle
                        }).AsEnumerable();

            return data;
        }

        public IEnumerable<RoomSlot> GetFilter(int? month, int? year, int? roomId)
        {
            var context = new MeetingRoomEntities();

            var data = (from a in context.MstRoomSlots
                        join b in context.MstMeetingRoomLayouts on new { layoutId = a.layoutId } equals new { layoutId = b.Id }
                        where a.month == month && a.year == year && a.layoutId == roomId
                        select new RoomSlot
                        {
                            Id = a.Id,
                            roomId = a.layoutId,
                            layoutId = a.layoutId,
                            capacity = a.capacity,
                            month = a.month,
                            year = a.year,
                            days = a.days,
                            hour1 = a.hour1,
                            hour2 = a.hour2,
                            hour3 = a.hour3,
                            hour4 = a.hour4,
                            hour5 = a.hour5,
                            hour6 = a.hour6,
                            hour7 = a.hour7,
                            hour8 = a.hour8,
                            hour9 = a.hour9,
                            hour10 = a.hour10,
                            hour11 = a.hour11,
                            hour12 = a.hour12,
                            hour13 = a.hour13,
                            isActive = a.isActive,
                            roomTitle = b.meetingRoomLayoutTitle
                        }).AsEnumerable();

            return data;
        }

        public RoomSlot GetById(int id)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstRoomSlots.FirstOrDefault(w => w.Id == id);

            return Mapper.Map<MstRoomSlot, RoomSlot>(result);
        }

        public IEnumerable<RoomSlot> GetRoom()
        {
            var context = new MeetingRoomEntities();
            return context.MstRoomSlots
                           .OrderBy(x => x.Id)
                           .Select(x => new RoomSlot { Id = x.Id })
                           .ToList();
        }

        public void Update(RoomSlot roomSlot)
        {
            var context = new MeetingRoomEntities();
            var oldRoom = context.MstRoomSlots.FirstOrDefault(w => w.Id == roomSlot.Id);

            Mapper.Map(roomSlot, oldRoom);

            context.MstRoomSlots.Attach(oldRoom);
            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public bool GenerateRoomSlot(int startMonth, int endMonth, int year, int layoutId)
        {
            bool isSuccess = false;

            using (MeetingRoomEntities context = new MeetingRoomEntities())
            {
                RoomSlot roomSlot = new RoomSlot();

                var newSlot = Mapper.Map<RoomSlot, MstRoomSlot>(roomSlot);
                var layout = from a in context.MstMeetingRoomLayouts
                             where a.Id == layoutId
                             select new
                             {
                                 a.Id,
                                 a.meetingRoomLayoutTitle,
                                 a.meetingRoomLayoutCapacity
                             };

                var mstRoom = layout.FirstOrDefault(w => w.Id == layoutId);

                var checkRoomSlot = from a in context.MstRoomSlots
                                    where
                                      a.layoutId == layoutId &&
                                      a.month == startMonth &&
                                      a.year == year
                                    select new
                                    {
                                        a.Id
                                    };

                if (checkRoomSlot.Count() > 0)
                {
                    return isSuccess = false;
                }

                DateTime StartDate = new DateTime(year, startMonth, 1);
                DateTime EndDate = new DateTime(year, endMonth, 1);

                for (DateTime date = StartDate.Date; date < EndDate.Date.AddMonths(1); date += TimeSpan.FromDays(1))
                {
                    newSlot.roomId = layoutId;
                    newSlot.layoutId = layoutId;
                    newSlot.capacity = mstRoom.meetingRoomLayoutCapacity.Value;
                    newSlot.month = Convert.ToInt32(date.ToString("MM"));
                    newSlot.year = year;
                    newSlot.days = Convert.ToInt32(date.ToString("dd"));
                    newSlot.hour1 = 0;
                    newSlot.hour2 = 0;
                    newSlot.hour3 = 0;
                    newSlot.hour4 = 0;
                    newSlot.hour5 = 0;
                    newSlot.hour6 = 0;
                    newSlot.hour7 = 0;
                    newSlot.hour8 = 0;
                    newSlot.hour9 = 0;
                    newSlot.hour10 = 0;
                    newSlot.hour11 = 0;
                    newSlot.hour12 = 0;
                    newSlot.hour13 = 0;
                    newSlot.isActive = 1;

                    context.MstRoomSlots.Add(newSlot);
                    context.SaveChanges();
                }
            }

            return isSuccess = true;
        }

        public bool CheckSlot(int year, int month, int day, int layoutId, string startTime, string finishTime)
        {
            bool isExist = false;

            var context = new MeetingRoomEntities();
            RoomSlot roomSlot = new RoomSlot();

            IEnumerable<RoomSlot> data;

            string[] strJam = { "{09:00:00}", "{10:00:00}", "{11:00:00}", "{12:00:00}", "{13:00:00}", "{14:00:00}", "{15:00:00}",
                                "{16:00:00}", "{17:00:00}", "{18:00:00}", "{19:00:00}", "{20:00:00}", "{21:00:00}" };

            int[] hour = new int[13];

            var indexA = Array.FindIndex(strJam, row => row.Contains(startTime));
            var indexB = Array.FindIndex(strJam, row => row.Contains(finishTime));

            int hourLoop = 1;
            for (int i = 0; i < 13; i++)
            {
                if ((i >= indexA) && (i <= indexB))
                {
                    hour[i] = 1;
                    hourLoop = hourLoop + 1;
                }
            }

            int result = indexB - indexA;
            int counter = 0;

            for (int i = 0; i < 13; i++)
            {
                int valueX = hour[i];

                if (counter >= result)
                {
                    return isExist;
                }

                if (valueX == 1)
                {
                    switch (i)
                    {
                        case 0:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour1 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        case 1:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour2 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        case 2:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour3 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        case 3:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour4 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;                                
                            }

                            counter++;
                            break;
                        case 4:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour5 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;                                
                            }

                            counter++;
                            break;
                        case 5:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour6 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }


                            counter++;
                            break;
                        case 6:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour7 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        case 7:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour8 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        case 8:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour9 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        case 9:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour10 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        case 10:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour11 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        case 11:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour12 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        case 12:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      MstRoomSlot.layoutId == layoutId &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour13 == 1 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }

                            counter++;
                            break;
                        default:
                            isExist = false;
                            break;
                    }

                    if (isExist == true)
                    {
                        return isExist;
                    }
                }
            }

            return isExist;
        }

        public bool CheckRoomSlot(int year, int month, int day, string startTime, string finishTime)
        {
            bool isExist = false;

            var context = new MeetingRoomEntities();
            RoomSlot roomSlot = new RoomSlot();

            IEnumerable<RoomSlot> data;

            string[] strJam = {"09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00",
                                "16:00", "17:00", "18:00", "19:00", "20:00", "21:00"};

            int[] hour = new int[13];

            var indexA = Array.FindIndex(strJam, row => row.Contains(startTime));
            var indexB = Array.FindIndex(strJam, row => row.Contains(finishTime));

            int hourLoop = 1;
            for (int i = 0; i < 13; i++)
            {
                if ((i >= indexA) && (i <= indexB))
                {
                    hour[i] = 1;
                    hourLoop = hourLoop + 1;
                }
            }

            for (int i = 0; i < 13; i++)
            {
                int valueX = hour[i];
                if (valueX == 1)
                {
                    switch (i)
                    {
                        case 0:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour1 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 1:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour2 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 2:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour3 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 3:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour4 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 4:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour5 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 5:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour6 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 6:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour7 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 7:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour8 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 8:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour9 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 9:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour10 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 10:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour11 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 11:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour12 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        case 12:
                            data = (from MstRoomSlot in context.MstRoomSlots
                                    where
                                      //(MstRoomSlot.capacity >= attendees || MstRoomSlot.capacity <= attendees) &&
                                      MstRoomSlot.month == month &&
                                      MstRoomSlot.year == year &&
                                      MstRoomSlot.days == day &&
                                      MstRoomSlot.hour13 == 0 &&
                                      MstRoomSlot.isActive == 1
                                    select new RoomSlot
                                    {
                                        Id = MstRoomSlot.Id,
                                        layoutId = MstRoomSlot.layoutId,
                                        capacity = MstRoomSlot.capacity,
                                        month = MstRoomSlot.month,
                                        year = MstRoomSlot.year,
                                        days = MstRoomSlot.days,
                                        hour1 = MstRoomSlot.hour1,
                                        hour2 = MstRoomSlot.hour2,
                                        hour3 = MstRoomSlot.hour3,
                                        hour4 = MstRoomSlot.hour4,
                                        hour5 = MstRoomSlot.hour5,
                                        hour6 = MstRoomSlot.hour6,
                                        hour7 = MstRoomSlot.hour7,
                                        hour8 = MstRoomSlot.hour8,
                                        hour9 = MstRoomSlot.hour9,
                                        hour10 = MstRoomSlot.hour10,
                                        hour11 = MstRoomSlot.hour11,
                                        hour12 = MstRoomSlot.hour12,
                                        hour13 = MstRoomSlot.hour13,
                                        isActive = MstRoomSlot.isActive
                                    }).AsEnumerable();

                            if (data.Count() > 0)
                            {
                                isExist = true;
                            }
                            break;
                        default:
                            isExist = false;
                            break;
                    }

                    if (isExist == true)
                    {
                        return isExist;
                    }
                }
            }

            return isExist;
        }

        public int BookingRoomSlot(Booking booking)
        {
            var context = new MeetingRoomEntities();
            RoomSlot roomSlot = new RoomSlot();

            int returnValue = -1;

            string[] strJam = { "{09:00:00}", "{10:00:00}", "{11:00:00}", "{12:00:00}", "{13:00:00}", "{14:00:00}", "{15:00:00}",
                                "{16:00:00}", "{17:00:00}", "{18:00:00}", "{19:00:00}", "{20:00:00}", "{21:00:00}" };
            int[] hour = new int[13];

            var indexA = Array.FindIndex(strJam, row => row.Contains(booking.StartTime.ToString()));
            var indexB = Array.FindIndex(strJam, row => row.Contains(booking.FinishTime.ToString()));

            for (int i = 0; i < 13; i++)
            {
                if ((i >= indexA) && (i < indexB))
                {
                    hour[i] = 1;
                }
            }

            DateTime dtBook = booking.BookingDate.Value;
            int year = dtBook.Year;
            int month = dtBook.Month;
            int days = dtBook.Day;
            int layoutId = booking.LayoutId.Value;

            var chkRoomSlot = (from MstRoomSlot in context.MstRoomSlots
                               where
                                  MstRoomSlot.layoutId == layoutId &&
                                  MstRoomSlot.month == month &&
                                  MstRoomSlot.year == year &&
                                  MstRoomSlot.days == days &&
                                  MstRoomSlot.isActive == 1
                               select new
                               {
                                   Id = MstRoomSlot.Id
                               });

            if (chkRoomSlot.Count() > 0)
            {
                bool isChecked = CheckSlot(year, month, days, layoutId, booking.StartTime.ToString(), booking.FinishTime.ToString());
                if (isChecked == true)
                {
                    returnValue = 0; // Room Full
                }
                else
                {
                    returnValue = 2; // Room Ready
                }
            }
            else
            {
                returnValue = 1; // Room Slot Not Generated
            }

            return returnValue;
        }

        public int GetRoomSlotId(Booking booking)
        {
            var context = new MeetingRoomEntities();
            RoomSlot roomSlot = new RoomSlot();

            string[] strJam = { "{09:00:00}", "{10:00:00}", "{11:00:00}", "{12:00:00}", "{13:00:00}", "{14:00:00}", "{15:00:00}",
                                "{16:00:00}", "{17:00:00}", "{18:00:00}", "{19:00:00}", "{20:00:00}", "{21:00:00}" };
            int[] hour = new int[13];

            var indexA = Array.FindIndex(strJam, row => row.Contains(booking.StartTime.ToString()));
            var indexB = Array.FindIndex(strJam, row => row.Contains(booking.FinishTime.ToString()));

            for (int i = 0; i < 13; i++)
            {
                if ((i >= indexA) && (i < indexB))
                {
                    hour[i] = 1;
                }
                //else
                //{
                //    hour[i] = 0;
                //}
            }

            DateTime dtBook = booking.BookingDate.Value;
            int year = dtBook.Year;
            int month = dtBook.Month;
            int days = dtBook.Day;
            int layoutId = booking.LayoutId.Value;

            var data = (from MstRoomSlot in context.MstRoomSlots
                        where
                          MstRoomSlot.layoutId == layoutId &&
                          MstRoomSlot.month == month &&
                          MstRoomSlot.year == year &&
                          MstRoomSlot.days == days &&
                          MstRoomSlot.isActive == 1
                        select new RoomSlot
                        {
                            Id = MstRoomSlot.Id,
                            layoutId = MstRoomSlot.layoutId,
                            capacity = MstRoomSlot.capacity,
                            month = MstRoomSlot.month,
                            year = MstRoomSlot.year,
                            days = MstRoomSlot.days,
                            hour1 = MstRoomSlot.hour1,
                            hour2 = MstRoomSlot.hour2,
                            hour3 = MstRoomSlot.hour3,
                            hour4 = MstRoomSlot.hour4,
                            hour5 = MstRoomSlot.hour5,
                            hour6 = MstRoomSlot.hour6,
                            hour7 = MstRoomSlot.hour7,
                            hour8 = MstRoomSlot.hour8,
                            hour9 = MstRoomSlot.hour9,
                            hour10 = MstRoomSlot.hour10,
                            hour11 = MstRoomSlot.hour11,
                            hour12 = MstRoomSlot.hour12,
                            hour13 = MstRoomSlot.hour13,
                            isActive = MstRoomSlot.isActive
                        }).AsEnumerable();

            foreach (var item in data)
            {
                roomSlot.Id = item.Id;
                roomSlot.layoutId = item.layoutId;
                roomSlot.days = item.days;
                roomSlot.month = item.month;
                roomSlot.year = item.year;
                roomSlot.capacity = item.capacity;
                roomSlot.isActive = item.isActive;
                roomSlot.roomId = item.roomId;

                roomSlot.hour1 = Convert.ToByte(hour[0]) == 1 ? 1 : item.hour1;
                roomSlot.hour2 = Convert.ToByte(hour[1]) == 1 ? 1 : item.hour2;
                roomSlot.hour3 = Convert.ToByte(hour[2]) == 1 ? 1 : item.hour3;
                roomSlot.hour4 = Convert.ToByte(hour[3]) == 1 ? 1 : item.hour4;
                roomSlot.hour5 = Convert.ToByte(hour[4]) == 1 ? 1 : item.hour5;
                roomSlot.hour6 = Convert.ToByte(hour[5]) == 1 ? 1 : item.hour6;
                roomSlot.hour7 = Convert.ToByte(hour[6]) == 1 ? 1 : item.hour7;
                roomSlot.hour8 = Convert.ToByte(hour[7]) == 1 ? 1 : item.hour8;
                roomSlot.hour9 = Convert.ToByte(hour[8]) == 1 ? 1 : item.hour9;
                roomSlot.hour10 = Convert.ToByte(hour[9]) == 1 ? 1 : item.hour10;
                roomSlot.hour11 = Convert.ToByte(hour[10]) == 1 ? 1 : item.hour11;
                roomSlot.hour12 = Convert.ToByte(hour[11]) == 1 ? 1 : item.hour12;
                roomSlot.hour13 = Convert.ToByte(hour[12]) == 1 ? 1 : item.hour13;
            }

            var oldRoom = context.MstRoomSlots.FirstOrDefault(w => w.Id == roomSlot.Id);

            if (oldRoom != null)
            {
                Mapper.Map(roomSlot, oldRoom);

                oldRoom.roomId = roomSlot.roomId;
                oldRoom.layoutId = roomSlot.layoutId;
                oldRoom.days = roomSlot.days;
                oldRoom.month = roomSlot.month;
                oldRoom.year = roomSlot.year;
                oldRoom.capacity = oldRoom.capacity;
                oldRoom.hour1 = roomSlot.hour1;
                oldRoom.hour2 = roomSlot.hour2;
                oldRoom.hour3 = roomSlot.hour3;
                oldRoom.hour4 = roomSlot.hour4;
                oldRoom.hour5 = roomSlot.hour5;
                oldRoom.hour6 = roomSlot.hour6;
                oldRoom.hour7 = roomSlot.hour7;
                oldRoom.hour8 = roomSlot.hour8;
                oldRoom.hour9 = roomSlot.hour9;
                oldRoom.hour10 = roomSlot.hour10;
                oldRoom.hour11 = roomSlot.hour11;
                oldRoom.hour12 = roomSlot.hour12;
                oldRoom.hour13 = roomSlot.hour13;
                oldRoom.isActive = roomSlot.isActive;

                context.MstRoomSlots.Attach(oldRoom);
                context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
            else
            {
                roomSlot.Id = 0;
            }

            return roomSlot.Id;
        }

        public void DeleteById(int id)
        {
            var context = new MeetingRoomEntities();
            RoomSlot roomSlot = new RoomSlot();

            var oldRoom = context.MstRoomSlots.FirstOrDefault(w => w.Id == id);
            oldRoom.isActive = 0;

            context.MstRoomSlots.Attach(oldRoom);
            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public RoomSlot GetRoomReserved(int year, int month, int day, string startTime, string finishTime)
        {
            var context = new MeetingRoomEntities();
            RoomSlot roomSlot = new RoomSlot();

            var data = (from MstRoomSlot in context.MstRoomSlots
                        where
                          MstRoomSlot.month == month &&
                          MstRoomSlot.year == year &&
                          MstRoomSlot.days == day &&
                          MstRoomSlot.isActive == 1
                        select new RoomSlot
                        {
                            Id = MstRoomSlot.Id,
                            layoutId = MstRoomSlot.layoutId,
                            capacity = MstRoomSlot.capacity,
                            month = MstRoomSlot.month,
                            year = MstRoomSlot.year,
                            days = MstRoomSlot.days,
                            hour1 = MstRoomSlot.hour1,
                            hour2 = MstRoomSlot.hour2,
                            hour3 = MstRoomSlot.hour3,
                            hour4 = MstRoomSlot.hour4,
                            hour5 = MstRoomSlot.hour5,
                            hour6 = MstRoomSlot.hour6,
                            hour7 = MstRoomSlot.hour7,
                            hour8 = MstRoomSlot.hour8,
                            hour9 = MstRoomSlot.hour9,
                            hour10 = MstRoomSlot.hour10,
                            hour11 = MstRoomSlot.hour11,
                            hour12 = MstRoomSlot.hour12,
                            hour13 = MstRoomSlot.hour13,
                            isActive = MstRoomSlot.isActive
                        }).AsEnumerable();

            foreach (var item in data)
            {
                roomSlot.Id = item.Id;
                roomSlot.layoutId = item.layoutId;
                roomSlot.days = item.days;
                roomSlot.month = item.month;
                roomSlot.year = item.year;
                roomSlot.capacity = item.capacity;
                roomSlot.isActive = item.isActive;
                roomSlot.roomId = item.roomId;

                roomSlot.hour1 = item.hour1;
                roomSlot.hour2 = item.hour2;
                roomSlot.hour3 = item.hour3;
                roomSlot.hour4 = item.hour4;
                roomSlot.hour5 = item.hour5;
                roomSlot.hour6 = item.hour6;
                roomSlot.hour7 = item.hour7;
                roomSlot.hour8 = item.hour8;
                roomSlot.hour9 = item.hour9;
                roomSlot.hour10 = item.hour10;
                roomSlot.hour11 = item.hour11;
                roomSlot.hour12 = item.hour12;
                roomSlot.hour13 = item.hour13;
            }


            return roomSlot;
        }

        public void UpdateRoomSlot(int id, string startTime, string endTime)
        {

            var context = new MeetingRoomEntities();
            var oldRoom = context.MstRoomSlots.FirstOrDefault(w => w.Id == id);

            RoomSlot roomSlot = new RoomSlot();

            string[] strJam = { "{09:00:00}", "{10:00:00}", "{11:00:00}", "{12:00:00}", "{13:00:00}", "{14:00:00}", "{15:00:00}",
                                "{16:00:00}", "{17:00:00}", "{18:00:00}", "{19:00:00}", "{20:00:00}", "{21:00:00}" };
            int[] hour = new int[13];

            var indexA = Array.FindIndex(strJam, row => row.Contains(startTime));
            var indexB = Array.FindIndex(strJam, row => row.Contains(endTime));

            for (int i = 0; i < 13; i++)
            {
                if ((i >= indexA) && (i < indexB))
                {
                    hour[i] = 1;
                }
            }

            for (int i = 0; i < 13; i++)
            {
                int valueX = hour[i];
                if (valueX == 1)
                {
                    switch (i)
                    {
                        case 0:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = 0;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour1 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();
                            break;
                        case 1:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = 0;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour2 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 2:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = 0;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour3 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 3:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = 0;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour4 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 4:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = 0;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour5 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 5:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = 0;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour6 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 6:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = 0;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour7 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 7:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = 0;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour8 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 8:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = 0;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour9 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 9:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = 0;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour10 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 10:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = 0;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour11 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 11:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = 0;
                            //roomSlot.hour13 = oldRoom.hour13;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour12 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        case 12:
                            //roomSlot.Id = oldRoom.Id;
                            //roomSlot.layoutId = oldRoom.layoutId;
                            //roomSlot.days = oldRoom.days;
                            //roomSlot.month = oldRoom.month;
                            //roomSlot.year = oldRoom.year;
                            //roomSlot.capacity = oldRoom.capacity;
                            //roomSlot.isActive = oldRoom.isActive;
                            //roomSlot.roomId = oldRoom.roomId;

                            //roomSlot.hour1 = oldRoom.hour1;
                            //roomSlot.hour2 = oldRoom.hour2;
                            //roomSlot.hour3 = oldRoom.hour3;
                            //roomSlot.hour4 = oldRoom.hour4;
                            //roomSlot.hour5 = oldRoom.hour5;
                            //roomSlot.hour6 = oldRoom.hour6;
                            //roomSlot.hour7 = oldRoom.hour7;
                            //roomSlot.hour8 = oldRoom.hour8;
                            //roomSlot.hour9 = oldRoom.hour9;
                            //roomSlot.hour10 = oldRoom.hour10;
                            //roomSlot.hour11 = oldRoom.hour11;
                            //roomSlot.hour12 = oldRoom.hour12;
                            //roomSlot.hour13 = 0;

                            //Mapper.Map(roomSlot, oldRoom);

                            oldRoom.hour13 = 0;

                            context.MstRoomSlots.Attach(oldRoom);
                            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
