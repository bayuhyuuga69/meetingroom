﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class GlobalService : IGlobal
    {
        public GlobalService()
        {
            Mapper.CreateMap<MstGlobal, Global>();
            Mapper.CreateMap<Global, MstGlobal>();
        }
        public Global Get()
        {
            var context = new MeetingRoomEntities();

            var model = context.MstGlobals.FirstOrDefault();

            return Mapper.Map<MstGlobal, Global>(model);
        }
    }
}
