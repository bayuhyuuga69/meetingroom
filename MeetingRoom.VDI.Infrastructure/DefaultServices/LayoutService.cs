﻿using AutoMapper;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class LayoutService : ILayout
    {
        public LayoutService()
        {
            Mapper.CreateMap<MstMeetingRoomLayout, Layout>();
            Mapper.CreateMap<Layout, MstMeetingRoomLayout>();
        }

        public Layout Create(Layout layout)
        {
            var context = new MeetingRoomEntities();

            var newLayout = Mapper.Map<Layout, MstMeetingRoomLayout>(layout);

            newLayout.CreationTime = DateTime.Now;
            newLayout.IsDeleted = false;

            context.MstMeetingRoomLayouts.Add(newLayout);
            context.SaveChanges();

            return Mapper.Map<MstMeetingRoomLayout, Layout>(newLayout);
        }

        public void Delete(int id)
        {
            var context = new MeetingRoomEntities();
            var oldLayout = context.MstMeetingRoomLayouts.FirstOrDefault(w => w.Id == id);

            oldLayout.IsDeleted = true;
            oldLayout.DeletionTime = System.DateTime.Now;

            context.MstMeetingRoomLayouts.Attach(oldLayout);
            context.Entry(oldLayout).State = System.Data.Entity.EntityState.Modified;

            context.SaveChanges();

            //context.MstMeetingRoomLayouts.Remove(oldLayout);
            //context.Entry(oldLayout).State = System.Data.Entity.EntityState.Deleted;
            //context.SaveChanges();
        }

        public IEnumerable<Layout> GetAll(int pageIndex = 1, int pageSize = 10)
        {
            var context = new MeetingRoomEntities();
            var data = context.MstMeetingRoomLayouts.AsQueryable().Where(w => w.IsDeleted == false);

            return Mapper.Map<IEnumerable<MstMeetingRoomLayout>, IEnumerable<Layout>>(data);
        }

        public IEnumerable<Layout> GetLayout()
        {
            var context = new MeetingRoomEntities();
            return context.MstMeetingRoomLayouts
                           .Where(w => w.IsDeleted == false)
                           .OrderBy(x => x.Id)
                           .Select(x => new Layout { Id = x.Id, meetingRoomLayoutTitle = x.meetingRoomLayoutTitle })
                           .ToList();

        }

        public Layout GetById(int id)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstMeetingRoomLayouts.FirstOrDefault(w => w.Id == id);

            return Mapper.Map<MstMeetingRoomLayout, Layout>(result);
        }

        public int GetCapacity(int id)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstMeetingRoomLayouts.FirstOrDefault(w => w.Id == id);

            return result.Id;
        }

        public Layout GetByName(string name)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstMeetingRoomLayouts.FirstOrDefault(w => w.meetingRoomLayoutTitle == name);

            return Mapper.Map<MstMeetingRoomLayout, Layout>(result);
        }

        public void Update(Layout layout)
        {
            var context = new MeetingRoomEntities();
            var oldLayout = context.MstMeetingRoomLayouts.FirstOrDefault(w => w.Id == layout.Id);

            layout.IsDeleted = false;
            layout.CreationTime = oldLayout.CreationTime;

            if ((layout.meetingRoomLayoutImages == null) || (layout.meetingRoomLayoutImages.Length == 0))
            {
                layout.meetingRoomLayoutImages = oldLayout.meetingRoomLayoutImages;
            }

            Mapper.Map(layout, oldLayout);
            oldLayout.LastModificationTime = DateTime.Now;

            context.MstMeetingRoomLayouts.Attach(oldLayout);
            context.Entry(oldLayout).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public List<Layout> GetLayoutImage()
        {
            var context = new MeetingRoomEntities();

            var data = (from a in context.MstMeetingRoomLayouts
                        where a.IsDeleted == false
                        select new Layout
                        {
                            Id = a.Id,
                            meetingRoomLayoutTitle = a.meetingRoomLayoutTitle,
                            meetingRoomLayoutImages = a.meetingRoomLayoutImages
                        }).AsEnumerable();

            List<Layout> layoutList = new List<Layout>();

            foreach (var item in data)
            {
                Layout layout = new Layout();
                layout.Id = item.Id;
                layout.meetingRoomLayoutTitle = item.meetingRoomLayoutTitle;
                layout.imageString = Convert.ToBase64String(item.meetingRoomLayoutImages);

                layoutList.Add(layout);
            }

            return layoutList;
        }

        public Layout GetPriceLayout(int Id)
        {
            var context = new MeetingRoomEntities();

            var data = (from a in context.MstMeetingRoomLayouts
                        where
                          a.Id == Id
                        select new Layout
                        {
                            Id = a.Id,
                            meetingRoomLayoutTitle = a.meetingRoomLayoutTitle,
                            meetingRoomLayoutPrice = a.meetingRoomLayoutPrice.Value,
                            meetingRoomLayoutCapacity = a.meetingRoomLayoutCapacity.Value
                        }).AsEnumerable();

            Layout layout = new Layout();

            foreach (var item in data)
            {
                layout.Id = item.Id;
                layout.meetingRoomLayoutTitle = item.meetingRoomLayoutTitle;
                layout.meetingRoomLayoutPrice = item.meetingRoomLayoutPrice;
                layout.meetingRoomLayoutCapacity = item.meetingRoomLayoutCapacity;
            }

            return layout;
        }

        public Layout GetLayoutImageById(int id)
        {
            var context = new MeetingRoomEntities();

            var data = (from a in context.MstMeetingRoomLayouts
                        where a.Id == id && a.IsDeleted == false
                        select new Layout
                        {
                            Id = a.Id,
                            meetingRoomLayoutTitle = a.meetingRoomLayoutTitle,
                            meetingRoomLayoutCapacity = a.meetingRoomLayoutCapacity.Value,
                            meetingRoomLayoutImages = a.meetingRoomLayoutImages
                        }).AsEnumerable();


            Layout layout = new Layout();

            foreach (var item in data)
            {
                layout.Id = item.Id;
                layout.meetingRoomLayoutTitle = item.meetingRoomLayoutTitle;
                layout.meetingRoomLayoutCapacity = item.meetingRoomLayoutCapacity;
                layout.imageString = Convert.ToBase64String(item.meetingRoomLayoutImages);
            }

            return layout;
        }
    }
}
