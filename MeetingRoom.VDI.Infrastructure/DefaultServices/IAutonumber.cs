﻿namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public interface IAutonumber
    {
        string GenerateName(string formatName, int seq, string prefix = "");
    }
}