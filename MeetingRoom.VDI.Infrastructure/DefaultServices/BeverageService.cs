﻿using AutoMapper;
using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class BeverageService : IBeverage
    {
        public BeverageService()
        {
            Mapper.CreateMap<MstMeetingRoomFoodDrink, Beverage>();
            Mapper.CreateMap<Beverage, MstMeetingRoomFoodDrink>();
        }

        public Beverage Create(Beverage beverage)
        {
            var context = new MeetingRoomEntities();

            var newBeverage = Mapper.Map<Beverage, MstMeetingRoomFoodDrink>(beverage);

            newBeverage.IsDeleted = false;
            newBeverage.CreationTime = System.DateTime.Now;
            context.MstMeetingRoomFoodDrinks.Add(newBeverage);
            context.SaveChanges();

            return Mapper.Map<MstMeetingRoomFoodDrink, Beverage>(newBeverage);
        }

        public void Delete(int id)
        {
            var context = new MeetingRoomEntities();
            var oldBeverage = context.MstMeetingRoomFoodDrinks.FirstOrDefault(w => w.Id == id);

            oldBeverage.IsDeleted = true;
            oldBeverage.DeletionTime = System.DateTime.Now;

            context.MstMeetingRoomFoodDrinks.Attach(oldBeverage);
            context.Entry(oldBeverage).State = System.Data.Entity.EntityState.Modified;

            //context.MstMeetingRoomFoodDrinks.Remove(oldBeverage);
            //context.Entry(oldBeverage).State = System.Data.Entity.EntityState.Deleted;
            context.SaveChanges();
        }

        public IEnumerable<Beverage> GetAll(int pageIndex = 1, int pageSize = 10000)
        {
            var context = new MeetingRoomEntities();
            var data = context.MstMeetingRoomFoodDrinks.AsQueryable().Where(w=>w.IsDeleted == false);

            return Mapper.Map<IEnumerable<MstMeetingRoomFoodDrink>, IEnumerable<Beverage>>(data);
        }

        public IEnumerable<Beverage> GetBeverage()
        {
            var context = new MeetingRoomEntities();
            return context.MstMeetingRoomFoodDrinks
                           .OrderBy(x => x.Id)
                           .Select(x => new Beverage { Id = x.Id, meetingRoomFoodDrinkTitle = x.meetingRoomFoodDrinkTitle })
                           .ToList();
        }

        public Beverage GetById(int id)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstMeetingRoomFoodDrinks.FirstOrDefault(w => w.Id == id);

            return Mapper.Map<MstMeetingRoomFoodDrink, Beverage>(result);
        }

        public Beverage GetByName(string name)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstMeetingRoomFoodDrinks.FirstOrDefault(w => w.meetingRoomFoodDrinkTitle == name);

            return Mapper.Map<MstMeetingRoomFoodDrink, Beverage>(result);
        }

        public void Update(Beverage beverage)
        {
            var context = new MeetingRoomEntities();
            var oldBeverage = context.MstMeetingRoomFoodDrinks.FirstOrDefault(w => w.Id == beverage.Id);

            beverage.IsDeleted = false;
            beverage.CreationTime = oldBeverage.CreationTime;

            Mapper.Map(beverage, oldBeverage);
            oldBeverage.LastModificationTime = DateTime.Now;

            context.MstMeetingRoomFoodDrinks.Attach(oldBeverage);
            context.Entry(oldBeverage).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public PagedData<Beverage> GetAll(List<Beverage> filter, Beverage search, int pageIndex, int pageSize = GlobalSetting.DEFAULT_PAGESIZE)
        {
            var context = new MeetingRoomEntities();

            var data = context.MstMeetingRoomFoodDrinks.AsQueryable();

            if (filter != null)
            {
                var filterId = filter.Where(w => w.Id > 0).Select(s => s.Id).ToArray();
                //var filterMobilId = filter.Where(w => w.MobilId > 0).Select(s => s.MobilId).ToArray();
                var filterPoolId = filter.Where(w => w.Id > 0).Select(s => s.Id).ToArray();
                //var filterStatus = filter.Where(w => w.meetingRoomFoodDrinkTitle > 0).Select(s => (int)s.Status).ToArray();
                //var filterType = filter.Where(w => w.TypeLepas > 0).Select(s => (int)s.TypeLepas).ToArray();


                if (filterId.Any()) //Tipe integer
                    data = data.Where(w => filterId.Contains(w.Id));

                if (filterPoolId.Any()) //Tipe integer
                    data = data.Where(w => w.Id != null && filterPoolId.Contains(w.Id));

                //if (filterStatus.Any()) //Tipe integer
                //    data = data.Where(w => filterStatus.Contains(w.Status));

                //if (filterType.Any()) //Tipe integer
                //    data = data.Where(w => filterType.Contains(w.TypeLepas));
            }

            if (search != null)
            {
                data = data.Where(w => w.meetingRoomFoodDrinkTitle.ToLower().Contains(search.meetingRoomFoodDrinkTitle.ToLower()));
            }

            var result = new PagedData<Beverage>
            {
                TotalRecord = data.Count(),
                CurrentIndex = pageIndex,
                Data = Mapper.Map<IEnumerable<MstMeetingRoomFoodDrink>, IEnumerable<Beverage>>(data.OrderBy(o => o.meetingRoomFoodDrinkTitle).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList().AsEnumerable())
            };

            return result = filter == null ? new PagedData<Beverage>() : result;
        }

        public BeveragePrice GetPriceFood(int Id, int Qty)
        {
            var context = new MeetingRoomEntities();

            var food = new BeveragePrice();

            var data = (from a in context.MstMeetingRoomFoodDrinks
                        where
                          a.Id == Id
                        select new BeveragePrice
                        {
                            Id = a.Id,
                            meetingRoomFoodDrinkDescription = a.meetingRoomFoodDrinkDescription,
                            meetingRoomFoodDrinkPrice = a.meetingRoomFoodDrinkPrice,
                            meetingRoomFoodDrinkTitle = a.meetingRoomFoodDrinkTitle                            
                        });

            foreach (var item in data)
            {
                food.Id = item.Id;
                food.meetingRoomFoodDrinkDescription = item.meetingRoomFoodDrinkDescription;
                food.meetingRoomFoodDrinkPrice = item.meetingRoomFoodDrinkPrice * Qty;
                food.meetingRoomFoodDrinkTitle = item.meetingRoomFoodDrinkTitle;
                food.qty = Qty;
            }

            return food;
        }

        public int GetAllBeverage()
        {
            var context = new MeetingRoomEntities();
            var data = context.MstMeetingRoomFoodDrinks.AsQueryable();

            return data.Count();
        }
    }
}
