﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class OfflinePaymentService : IOfflinePayment
    {
        private readonly IAutonumber _autoNumberService;
        private readonly IBooking _bookingService;

        public OfflinePaymentService(IAutonumber autoNumberService, IBooking bookingService)
        {
            Mapper.CreateMap<TrPayment, Payment>();
            Mapper.CreateMap<Payment, TrPayment>();

            _autoNumberService = autoNumberService;
            _bookingService = bookingService;
        }

        public Payment Create(Payment payment)
        {
            var context = new MeetingRoomEntities();

            var newPayment = Mapper.Map<Payment, TrPayment>(payment);

            context.TrPayments.Add(newPayment);
            context.SaveChanges();

            return Mapper.Map<TrPayment, Payment>(newPayment);
        }

        public IEnumerable<Payment> GetAll(int pageIndex = 1, int pageSize = 10)
        {
            var context = new MeetingRoomEntities();
            var data = context.TrPayments.Where(w => w.PaymentMethod == 2 && w.Paid == false).OrderByDescending(w=> w.BookingDate).AsQueryable();

            return Mapper.Map<IEnumerable<TrPayment>, IEnumerable<Payment>>(data);
        }

        public Payment GetById(int id)
        {
            //var context = new MeetingRoomEntities();
            //var result = context.TrPayments.FirstOrDefault(w => w.Id == id);

            //return Mapper.Map<TrPayment, Payment>(result);

            var context = new MeetingRoomEntities();

            var payment = new Payment();

            var data = (from a in context.TrPayments
                        join b in context.MstMeetingRoomLayouts on new { LayoutId = (int)a.RoomId } equals new { LayoutId = b.Id }
                        where
                          a.Id == id
                        select new Payment
                        {
                            Id = a.Id,
                            BookingCode = a.BookingCode,
                            BookingDate = a.BookingDate,
                            ClientName = a.ClientName,
                            PaymentMethod = a.PaymentMethod,
                            PaymentType =
                                  a.PaymentMethod == 1 ? "OVO" :
                                  a.PaymentMethod == 2 ? "CASH" : null,
                            RoomLayout = b.meetingRoomLayoutTitle,
                            TotalPayment = a.TotalPayment,
                            Room = b.meetingRoomLayoutTitle,
                            Amount = a.Amount,
                            AmountToBePaid = a.AmountToBePaid,
                            PhoneNumber = a.PhoneNumber,
                            StrukImage = a.StrukImage,
                            Paid = a.Paid.Value
                        }).AsEnumerable();

            foreach (var item in data)
            {
                payment.Id = item.Id;
                payment.BookingCode = item.BookingCode;
                payment.BookingDate = item.BookingDate;
                payment.ClientName = item.ClientName;
                payment.PaymentType = item.PaymentType;
                payment.TotalPayment = item.TotalPayment;
                payment.RoomLayout = item.RoomLayout;
                payment.Amount = item.Amount;
                payment.AmountToBePaid = item.AmountToBePaid;
                payment.PhoneNumber = item.PhoneNumber;
                payment.RoomLayout = item.RoomLayout;
                payment.StrukImage = item.StrukImage;
                payment.Paid = item.Paid;
            }

            return payment;
        }

        public Payment GetByName(string name)
        {
            var context = new MeetingRoomEntities();
            var result = context.TrPayments.FirstOrDefault(w => w.BookingCode == name);

            return Mapper.Map<TrPayment, Payment>(result);
        }

        public Payment GetByResCode(string name)
        {
            var context = new MeetingRoomEntities();
            var result = context.TrPayments.FirstOrDefault(w => w.ReservedCode == name);

            return Mapper.Map<TrPayment, Payment>(result);
        }

        public Payment GetByReservedCode(string resCode)
        {
            var context = new MeetingRoomEntities();
            var result = context.TrPayments.FirstOrDefault(w => w.ReservedCode == resCode);

            return Mapper.Map<TrPayment, Payment>(result);
        }

        public string Update(Payment payment)
        {
            var context = new MeetingRoomEntities();
            var oldPayment = context.TrPayments.FirstOrDefault(w => w.Id == payment.Id);

            if ((payment.StrukImage == null) || (payment.StrukImage.Length == 0))
            {
                payment.StrukImage = oldPayment.StrukImage;
            }

            var oldBooking = context.TrBookings.FirstOrDefault(w => w.ReservedCode == oldPayment.ReservedCode);

            var countExits = context.TrBookings.Where(w => w.BookingCode.StartsWith("Book") && w.IsDeleted == false).Count();
            oldBooking.BookingCode = _autoNumberService.GenerateName("BOOKING", ++countExits, "BOOK");
            payment.BookingCode = oldBooking.BookingCode;

            Booking newBooking = new Booking();
            newBooking.Id = oldBooking.Id;
            newBooking.Name = oldBooking.Name;
            newBooking.Phone = oldBooking.Phone;
            newBooking.Email = oldBooking.Email;
            newBooking.Company = oldBooking.Company;
            newBooking.TranDate = oldBooking.TranDate.Value;
            newBooking.BookingCode = oldBooking.BookingCode;
            newBooking.ReservedCode = oldBooking.ReservedCode;
            newBooking.BookingDate = oldBooking.BookingDate;
            newBooking.SpecialRequest = oldBooking.SpecialRequest;
            newBooking.StartTime = oldBooking.StartTime;
            newBooking.FinishTime = oldBooking.FinishTime;
            newBooking.MeetingRoomType = oldBooking.MeetingRoomType;
            newBooking.NumberOfPeople = oldBooking.NumberOfPeople;
            newBooking.Facilities = oldBooking.Facilities;
            newBooking.LayoutId = oldBooking.LayoutId;
            newBooking.PaymentMethod = oldBooking.PaymentMethod;
            newBooking.RoomPrice = oldBooking.RoomPrice;
            newBooking.EquipmentPrice = oldBooking.EquipmentPrice;
            newBooking.FoodAndDrinkPrice = oldBooking.FoodAndDrinkPrice;
            newBooking.SubTotal = oldBooking.SubTotal;
            newBooking.Tax = oldBooking.Tax;
            newBooking.Total = oldBooking.Total;
            newBooking.Deposit = oldBooking.Deposit;
            newBooking.RoomSlotId = oldBooking.RoomSlotId;

            _bookingService.Update(newBooking);

            oldPayment.RoomId = oldPayment.RoomId;
            oldPayment.PaymentMethod = oldPayment.PaymentMethod;
            oldPayment.BookingCode = oldBooking.BookingCode; 
            oldPayment.RoomId = oldPayment.RoomId;
            oldPayment.ReservedCode = oldPayment.ReservedCode;
            oldPayment.PaymentMethod = oldPayment.PaymentMethod;
            oldPayment.StrukImage = payment.StrukImage;
            oldPayment.Paid = true;

            //Mapper.Map(payment, oldPayment);           

            context.TrPayments.Attach(oldPayment);
            context.Entry(oldPayment).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();

            return oldPayment.BookingCode;

            //string URL = "https://innodev.vnetcloud.com/liveinpayment/payment";
            //string DATA = @"{
            //                                 ""name"" :""Eko Priyatno"",
            //                                 ""email"":""_aimee.utami4@gmail.com"",
            //                                 ""siteid"":5,
            //                                 ""orgid"":1,
            //                                 ""paymenttype"":3,
            //                                 ""amount"":120000,
            //                                 ""ovoid"":""085655300252""
            //                            }";


            //System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            //client.BaseAddress = new System.Uri(URL);
            //client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            //System.Net.Http.HttpContent content = new StringContent(DATA, UTF8Encoding.UTF8, "application/json");
            //HttpResponseMessage messge = client.PostAsync(URL, content).Result;
            //string description = string.Empty;
            //if (messge.IsSuccessStatusCode)
            //{
            //    string result = messge.Content.ReadAsStringAsync().Result;
            //    description = result;   
            //}

            //return description;
        }
    }
}
