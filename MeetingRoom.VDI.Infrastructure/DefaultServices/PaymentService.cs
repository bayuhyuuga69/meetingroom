﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class PaymentService : IPayment
    {
        public PaymentService()
        {
            Mapper.CreateMap<TrPayment, Payment>();
            Mapper.CreateMap<Payment, TrPayment>();
        }

        public Payment Create(Payment payment)
        {
            var context = new MeetingRoomEntities();

            var newPayment = Mapper.Map<Payment, TrPayment>(payment);

            context.TrPayments.Add(newPayment);
            context.SaveChanges();

            //string x = ExecutePaymentApi(payment);

            return Mapper.Map<TrPayment, Payment>(newPayment);
        }

        public string ExecutePaymentApi(Payment payment)
        {
            string URL = "https://innodev.vnetcloud.com/liveinpayment/payment";
            string DATA = @"{
	                                            ""name"" :""Eko Priyatno"",
	                                            ""email"":""_aimee.utami4@gmail.com"",
	                                            ""siteid"":5,
	                                            ""orgid"":1,
	                                            ""paymenttype"":3,
	                                            ""amount"":120000,
	                                            ""ovoid"":""085655300252""
                                        }";


            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            client.BaseAddress = new System.Uri(URL);
            //byte[] cred = UTF8Encoding.UTF8.GetBytes("username:password");
            //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(cred));
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            System.Net.Http.HttpContent content = new StringContent(DATA, UTF8Encoding.UTF8, "application/json");
            HttpResponseMessage messge = client.PostAsync(URL, content).Result;
            string description = string.Empty;
            if (messge.IsSuccessStatusCode)
            {
                string result = messge.Content.ReadAsStringAsync().Result;
                description = result;
            }

            return description;
        }

        public void IsDeleted(int id)
        {
            var context = new MeetingRoomEntities();
            var oldPayment = context.TrPayments.FirstOrDefault(w => w.Id == id);
            oldPayment.IsDeleted = true;

            context.TrPayments.Attach(oldPayment);
            context.Entry(oldPayment).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
