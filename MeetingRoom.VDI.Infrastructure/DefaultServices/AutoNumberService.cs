﻿using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class AutoNumberService : IAutonumber
    {

        public string GenerateName(string formatName, int seq, string prefix = "")
        {

            var context = new MeetingRoomEntities();

            var format = context.MstAutoNumbers.FirstOrDefault(w => w.Name == formatName);
            
            if (format == null)
                throw new Exception("Tidak diketemukan nama tabel");

            var result = format.FormatDocument;

            //Replace Mask
            var year = DateTime.Now.ToString("yy");
            var month = DateTime.Now.ToString("MM");
            var seqNumber = seq.ToString(new string('0', format.SeqLength)).PadLeft(4,'0');

            result = result.Replace("{yy}", year);
            result = result.Replace("{mm}", month);
            result = result.Replace("{seq}", seqNumber);

            if (String.IsNullOrEmpty(prefix))
            {
                result = result.Replace("{PREFIX}-", "");
            }
            else
            {
                result = result.Replace("{PREFIX}", prefix);
            }

            return result;
        }
    }
}
