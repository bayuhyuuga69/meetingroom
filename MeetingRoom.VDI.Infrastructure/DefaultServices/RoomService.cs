﻿using AutoMapper;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class RoomService : IRoom
    {
        public RoomService()
        {
            Mapper.CreateMap<MstMeetingRoom, Room>();
            Mapper.CreateMap<Room, MstMeetingRoom>();
        }

        public Room Create(Room room)
        {
            var context = new MeetingRoomEntities();

            var newRoom = Mapper.Map<Room, MstMeetingRoom>(room);

            newRoom.IsDeleted = false;
            newRoom.CreationTime = System.DateTime.Now;
            context.MstMeetingRooms.Add(newRoom);
            context.SaveChanges();

            return Mapper.Map<MstMeetingRoom, Room>(newRoom);
        }

        public void Delete(int id)
        {
            var context = new MeetingRoomEntities();
            var oldRoom = context.MstMeetingRooms.FirstOrDefault(w => w.Id == id);

            context.MstMeetingRooms.Remove(oldRoom);
            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Deleted;
            context.SaveChanges();
        }

        public IEnumerable<Room> GetAll(int pageIndex = 1, int pageSize = 10)
        {
            var context = new MeetingRoomEntities();
            var data = context.MstMeetingRooms.AsQueryable();//OrderBy(o => o.Id).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList().AsEnumerable();

            //var result = new PagedData<Room>
            //{
            //    TotalRecord = context.MstMeetingRooms.Count(),
            //    CurrentIndex = pageIndex,
            //    Data = Mapper.Map<IEnumerable<MstMeetingRoom>, IEnumerable<Room>>(data)
            //};

            //return result;
            return Mapper.Map<IEnumerable<MstMeetingRoom>, IEnumerable<Room>>(data);
        }

        public IEnumerable<Room> GetRoom(int id)
        {
            var context = new MeetingRoomEntities();
            return context.MstMeetingRooms
                           .Where(w => w.LayoutId == id)
                           .OrderBy(x => x.Id)
                           .Select(x => new Room { Id = x.Id, MeetingRoomTitle = x.MeetingRoomTitle })
                           .ToList();

        }

        public IEnumerable<Room> GetAllRoom()
        {
            var context = new MeetingRoomEntities();
            return context.MstMeetingRooms
                           .OrderBy(x => x.Id)
                           .Select(x => new Room { Id = x.Id, MeetingRoomTitle = x.MeetingRoomTitle })
                           .ToList();

        }

        public Room GetById(int id)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstMeetingRooms.FirstOrDefault(w => w.Id == id);

            return Mapper.Map<MstMeetingRoom, Room>(result);
        }

        public Room GetByName(string name)
        {
            var context = new MeetingRoomEntities();
            var result = context.MstMeetingRooms.FirstOrDefault(w => w.MeetingRoomTitle == name);

            return Mapper.Map<MstMeetingRoom, Room>(result);
        }

        public void Update(Room room)
        {
            var context = new MeetingRoomEntities();
            var oldRoom = context.MstMeetingRooms.FirstOrDefault(w => w.Id == room.Id);

            room.IsDeleted = false;
            room.CreationTime = oldRoom.CreationTime;

            if ((room.MeetingRoomImage == null) || (room.MeetingRoomImage.Length == 0))
            {
                room.MeetingRoomImage = oldRoom.MeetingRoomImage;
            }

            Mapper.Map(room, oldRoom);
            oldRoom.LastModificationTime = DateTime.Now;

            context.MstMeetingRooms.Attach(oldRoom);
            context.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public RoomSlot GetLayoutImageByRoomId(int id)
        {
            var context = new MeetingRoomEntities();

            var roomSlot = new RoomSlot();

            var data = (from a in context.MstMeetingRooms
                        join b in context.MstMeetingRoomLayouts on new { LayoutId = (int)a.LayoutId } equals new { LayoutId = b.Id }
                        where
                          a.Id == id
                        select new RoomSlot
                        {
                            Id = a.Id,
                            capacity = a.MeetingRoomCapacity,
                            LayoutImages = b.meetingRoomLayoutImages
                        }).AsEnumerable();

            foreach (var item in data)
            {
                roomSlot.Id = item.Id;
                roomSlot.capacity = item.capacity;
                roomSlot.imageString = Convert.ToBase64String(item.LayoutImages);
            }

            return roomSlot;
        }
    }
}
