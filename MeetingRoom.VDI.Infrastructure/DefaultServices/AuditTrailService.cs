﻿using MeetingRoom.VDI.Core.Enums;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class AuditTrailService : IDisposable
    {
        //private static readonly object SessionHelper;

        private static bool SucceedToInvoke(PropertyInfo info, object newModule, object oldModule, EnumFormActionMode auditTrailOperation)
        {
            bool isSucceed = false;
            try
            {
                string strGet = "";
                string strGetNew = "";
                switch (auditTrailOperation)
                {
                    case EnumFormActionMode.Add:
                        strGetNew = (info.GetValue(newModule, null) ?? "").ToString();
                        break;
                    case EnumFormActionMode.Delete:
                        strGet = (info.GetValue(oldModule, null) ?? "").ToString();
                        break;
                    case EnumFormActionMode.Edit:
                        strGet = (info.GetValue(oldModule, null) ?? "").ToString();
                        strGetNew = (info.GetValue(newModule, null) ?? "").ToString();
                        break;
                }
                isSucceed = true;
            }
            catch (Exception)
            {
                isSucceed = false;
            }
            return isSucceed;
        }

        public static long InsertAuditTrail(EnumFormActionMode actionMode, EnumFormApprovalMode approvalMode, object oldModule,
            object newModule, string formName, long dataRowId = 0)
        {
            long pkAuditTrailId = 0;

            try
            {
                using (MeetingRoomEntities context = new MeetingRoomEntities())
                {
                    switch (actionMode)
                    {
                        case EnumFormActionMode.Add:

                            #region Add Mode

                            Type objTypeNew = newModule.GetType();
                            PropertyInfo[] propInfoNew = objTypeNew.GetProperties();

                            if (propInfoNew.Length > 0)
                            {
                                //insert ke audit trail header
                                TrAuditTrailHeader ath = new TrAuditTrailHeader();
                                ath.FormName = formName;
                                ath.FormApprovalMode = (byte)approvalMode;
                                ath.FormActionMode = (byte)actionMode;
                                ath.ExecuteBy = SessionHelper.SessionUserName();
                                ath.ExecuteDate = DateTime.Now;
                                ath.DataRowId = dataRowId;

                                if (approvalMode == EnumFormApprovalMode.WithApproval)
                                {
                                    ath.ApprovalStatus = (byte)approvalMode;
                                }

                                //get audittraildetail
                                foreach (PropertyInfo info in propInfoNew)
                                {
                                    if (info.Name != "EntityState" && info.Name != "EntityKey")
                                    {
                                        if (!info.Name.Contains("Reference"))
                                        {
                                            //if (info.PropertyType != typeof(EntityReference<>) || info.PropertyType != typeof(EntityReference) || info.PropertyType.FullName != "System.Data.Objects.DataClasses.EntityReference")
                                            if (info.PropertyType.BaseType.Name != "EntityObject" && !info.PropertyType.FullName.Contains("System.Data.Linq.EntitySet"))
                                            {
                                                if (SucceedToInvoke(info, newModule, oldModule, actionMode))
                                                {
                                                    TrAuditTrailDetail detail = new TrAuditTrailDetail();
                                                    detail.AuditTrailHeaderId = ath.Id;
                                                    detail.FieldName = info.Name;
                                                    detail.NewValue = info.GetValue(newModule, null) == null
                                                                                ? ""
                                                                                : info.GetValue(newModule, null).ToString();

                                                    context.TrAuditTrailDetails.Add(detail);
                                                }
                                            }
                                        }
                                    }
                                }

                                context.TrAuditTrailHeaders.Add(ath);
                                context.SaveChanges();

                                pkAuditTrailId = ath.Id;
                            }

                            #endregion

                            break;
                        case EnumFormActionMode.Edit:

                            #region Edit Mode

                            Type objTypeNew1 = newModule.GetType();
                            PropertyInfo[] propInfoNew1 = objTypeNew1.GetProperties();

                            Type objTypeOld1 = newModule.GetType();
                            PropertyInfo[] propInfoOld1 = objTypeOld1.GetProperties();

                            if (propInfoNew1.Length > 0)
                            {
                                //insert ke audit trail header
                                TrAuditTrailHeader ath = new TrAuditTrailHeader();
                                ath.FormName = formName;
                                ath.FormApprovalMode = (byte)approvalMode;
                                ath.FormActionMode = (byte)actionMode;
                                ath.ExecuteBy = SessionHelper.SessionUserName();
                                ath.ExecuteDate = DateTime.Now;
                                ath.DataRowId = dataRowId;

                                if (approvalMode == EnumFormApprovalMode.WithApproval)
                                {
                                    ath.ApprovalStatus = (byte)approvalMode;
                                }

                                //get audittraildetail
                                foreach (PropertyInfo info in propInfoNew1)
                                {
                                    foreach (PropertyInfo infoOld in propInfoOld1)
                                    {
                                        if (info.Name != "EntityState" && info.Name != "EntityKey")
                                        {
                                            if (!info.Name.Contains("Reference"))
                                            {
                                                if (info.Name.Trim() == infoOld.Name.Trim())
                                                {
                                                    //if (info.PropertyType != typeof(EntityReference<>) || info.PropertyType != typeof(EntityReference) || info.PropertyType.FullName != "System.Data.Objects.DataClasses.EntityReference")
                                                    if (info.PropertyType.BaseType.Name != "EntityObject" && !info.PropertyType.FullName.Contains("System.Data.Linq.EntitySet"))
                                                    {
                                                        if (SucceedToInvoke(info, newModule, oldModule, actionMode))
                                                        {
                                                            TrAuditTrailDetail detail = new TrAuditTrailDetail();
                                                            detail.AuditTrailHeaderId = ath.Id;
                                                            detail.FieldName = info.Name;
                                                            detail.NewValue = info.GetValue(newModule, null) == null
                                                                                  ? ""
                                                                                  : info.GetValue(newModule, null)
                                                                                        .ToString();
                                                            detail.OldValue = infoOld.GetValue(oldModule, null) == null
                                                                                  ? ""
                                                                                  : infoOld.GetValue(oldModule, null)
                                                                                           .ToString();

                                                            context.TrAuditTrailDetails.Add(detail);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                context.TrAuditTrailHeaders.Add(ath);
                                context.SaveChanges();

                                //pkAuditTrailHeader = ath.PK_AuditTrailHeader_Id;
                            }

                            #endregion

                            break;
                        case EnumFormActionMode.Delete:

                            #region Delete Mode

                            Type objTypeOld = oldModule.GetType();
                            PropertyInfo[] propInfoOld = objTypeOld.GetProperties();

                            if (propInfoOld.Length > 0)
                            {
                                //insert ke audit trail header
                                TrAuditTrailHeader ath = new TrAuditTrailHeader();
                                ath.FormName = formName;
                                ath.FormApprovalMode = (byte)approvalMode;
                                ath.FormActionMode = (byte)actionMode;
                                ath.ExecuteBy = SessionHelper.SessionUserName();
                                ath.ExecuteDate = DateTime.Now;
                                ath.DataRowId = dataRowId;

                                if (approvalMode == EnumFormApprovalMode.WithApproval)
                                {
                                    ath.ApprovalStatus = (byte)approvalMode;
                                }

                                //get audittraildetail
                                foreach (PropertyInfo info in propInfoOld)
                                {
                                    if (info.Name != "EntityState" && info.Name != "EntityKey")
                                    {
                                        if (!info.Name.Contains("Reference"))
                                        {
                                            //if (info.PropertyType != typeof(EntityReference<>) || info.PropertyType != typeof(EntityReference) || info.PropertyType.FullName != "System.Data.Objects.DataClasses.EntityReference")
                                            if (info.PropertyType.BaseType.Name != "EntityObject" && !info.PropertyType.FullName.Contains("System.Data.Linq.EntitySet"))
                                            {
                                                if (SucceedToInvoke(info, newModule, oldModule, actionMode))
                                                {
                                                    TrAuditTrailDetail detail = new TrAuditTrailDetail();
                                                    detail.AuditTrailHeaderId = ath.Id;
                                                    detail.FieldName = info.Name;
                                                    detail.OldValue = info.GetValue(oldModule, null) == null
                                                                          ? ""
                                                                          : info.GetValue(oldModule, null).ToString();

                                                    context.TrAuditTrailDetails.Add(detail);
                                                }
                                            }
                                        }
                                    }
                                }

                                context.TrAuditTrailHeaders.Add(ath);
                                context.SaveChanges();

                                pkAuditTrailId = ath.Id;
                            }

                            #endregion

                            break;
                        case EnumFormActionMode.Download:
                            ///TODO: belum tahu bentuk audit trailnya
                            break;
                        case EnumFormActionMode.Export:
                            ///TODO: belum tahu bentuk audit trailnya
                            break;
                        case EnumFormActionMode.Upload:
                            ///TODO: belum tahu bentuk audit trailnya
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: belum tahu bentuk throw errornya
            }

            return pkAuditTrailId;
        }

        #region IDisposable Implementation

        public void Dispose()
        {
            Dispose();
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
