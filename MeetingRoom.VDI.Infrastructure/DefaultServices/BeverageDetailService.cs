﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class BeverageDetailService : IBeverageDetail
    {
        private static IBeverage _beverageService;

        public BeverageDetailService(IBeverage beverageService)
        {
            Mapper.CreateMap<TrBeverageDetail, BeverageDetail>();
            Mapper.CreateMap<BeverageDetail, TrBeverageDetail>();

            _beverageService = beverageService;
        }

        public BeverageDetail Create(BeverageDetail beverage)
        {
            var context = new MeetingRoomEntities();

            var newBeverageDetail = Mapper.Map<BeverageDetail, TrBeverageDetail>(beverage);

            newBeverageDetail.IsDeleted = false;
            newBeverageDetail.CreationTime = System.DateTime.Now;
            context.TrBeverageDetails.Add(newBeverageDetail);
            context.SaveChanges();

            return Mapper.Map<TrBeverageDetail, BeverageDetail>(newBeverageDetail);
        }

        public IEnumerable<BeverageDetail> GetBeverage()
        {
            var context = new MeetingRoomEntities();
            return context.TrBeverageDetails
                           .OrderBy(x => x.Id)
                           .Select(x => new BeverageDetail { Id = x.Id })
                           .ToList();
        }

        public BeverageDetail GetById(int id)
        {
            var context = new MeetingRoomEntities();
            var result = context.TrBeverageDetails.FirstOrDefault(w => w.Id == id);

            return Mapper.Map<TrBeverageDetail, BeverageDetail>(result);
        }

        public IEnumerable<BeverageDetailList> GetBeverageDetailsById(int Id)
        {
            var context = new MeetingRoomEntities();
            var data = (from a in context.TrBeverageDetails
                        join b in context.MstMeetingRoomFoodDrinks on new { BeverageId = (int)a.BeverageId } equals new { BeverageId = b.Id }
                        where
                          a.BookingId == Id
                        select new BeverageDetailList
                        {
                            Id = a.Id,
                            BookingId = a.BookingId,
                            BeverageId = a.BeverageId,
                            Qty = a.Qty,
                            Price = a.Price,
                            BeverageName = b.meetingRoomFoodDrinkTitle,
                            BeverageDescription = b.meetingRoomFoodDrinkDescription
                        }).ToList();

            return data;
        }

        public float GetBeverageDetailTotalById(int Id)
        {
            float Price = 0;
            var context = new MeetingRoomEntities();
            var data = (from TrBeverageDetails in
                        (from TrBeverageDetails in context.TrBeverageDetails
                         where
                           TrBeverageDetails.BookingId == Id
                         select new
                         {
                             TrBeverageDetails.Price,
                             Dummy = "x"
                         })

                        group TrBeverageDetails by new { TrBeverageDetails.Dummy } into g
                        select new
                        {
                            Price = g.Sum(p => p.Price)
                        });

            foreach (var item in data)
            {
                Price = item.Price;
            }

            return Price;
        }

        public void DeleteBeverageById(int id)
        {
            var context = new MeetingRoomEntities();
            var oldBeverage = context.TrBeverageDetails.FirstOrDefault(w => w.Id == id);

            context.TrBeverageDetails.Remove(oldBeverage);
            context.Entry(oldBeverage).State = System.Data.Entity.EntityState.Deleted;
            context.SaveChanges();
        }

        public void Update(int Id, int Qty)
        {
            var context = new MeetingRoomEntities();
            var oldBvrDetails = context.TrBeverageDetails.FirstOrDefault(w => w.Id == Id);

            Beverage bvrPrice = _beverageService.GetById(oldBvrDetails.BeverageId);

            oldBvrDetails.Price = Qty * Convert.ToInt32(bvrPrice.meetingRoomFoodDrinkPrice);
            oldBvrDetails.Qty = Qty;

            context.TrBeverageDetails.Attach(oldBvrDetails);
            context.Entry(oldBvrDetails).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
