﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class BookingService : IBooking
    {
        private readonly IRoomSlot _roomSlotService;
        private readonly IAutonumber _autoNumberService;
        private readonly IPayment _paymentService;

        public BookingService(IRoomSlot roomSlotService, IAutonumber autoNumberService, IPayment paymentService)
        {
            Mapper.CreateMap<TrBooking, Booking>();
            Mapper.CreateMap<Booking, TrBooking>();

            _roomSlotService = roomSlotService;
            _autoNumberService = autoNumberService;
            _paymentService = paymentService;
        }

        public string Create(Booking Booking)
        {
            var context = new MeetingRoomEntities();

            var newBooking = Mapper.Map<Booking, TrBooking>(Booking);

            SmtpServer smtpServer = new SmtpServer();


            #region Generate Auto Number
            var countExits = context.TrBookings.Count();
            newBooking.ReservedCode = _autoNumberService.GenerateName("RESERVED", ++countExits, "Res");
            newBooking.TranDate = DateTime.Now;
            //newBooking.Tanggal = DateTime.Now;
            #endregion           

            //_roomSlotService.BookingRoomSlot(Booking);           

            string strMessage = "";
            int isRoom = _roomSlotService.BookingRoomSlot(Booking);

            switch (isRoom)
            {
                case 0:
                    strMessage = "Full";
                    break;
                case 1:
                    strMessage = "NotGenerated";
                    break;
                case 2:
                    try
                    {
                        newBooking.RoomSlotId = _roomSlotService.GetRoomSlotId(Booking);
                        if (newBooking.RoomSlotId == 0)
                        {
                            return strMessage = "RoomSlotFailed"; //Mapper.Map<TrBooking, Booking>(newBooking);
                        }

                        context.TrBookings.Add(newBooking);
                        context.SaveChanges();

                        Payment payment = new Payment();

                        payment.BookingCode = newBooking.BookingCode;
                        payment.ReservedCode = newBooking.ReservedCode;
                        payment.BookingDate = newBooking.BookingDate;
                        payment.ClientName = newBooking.Name;
                        payment.RoomId = newBooking.LayoutId;
                        payment.TotalPayment = newBooking.Total;
                        payment.PaymentMethod = newBooking.PaymentMethod;
                        payment.Amount = newBooking.Total;
                        payment.AmountToBePaid = newBooking.Total;
                        payment.PhoneNumber = Convert.ToInt32(newBooking.Phone);
                        payment.Paid = false;

                        if (newBooking.PaymentMethod == 2)
                        {
                            _paymentService.Create(payment);
                        }

                        //SendMail(newBooking.Email, newBooking.Id.ToString(), smtpServer);

                        strMessage = "Success" + newBooking.Id.ToString();
                    }
                    catch (Exception exception)
                    {
                        strMessage = exception.Message;
                    }

                    break;
                default:
                    strMessage = "Unknown";
                    break;
            }

            return strMessage;
        }

        public void Delete(int id)
        {
            var context = new MeetingRoomEntities();
            var oldBooking = context.TrBookings.FirstOrDefault(w => w.Id == id);

            context.TrBookings.Remove(oldBooking);
            context.Entry(oldBooking).State = System.Data.Entity.EntityState.Deleted;
            context.SaveChanges();
        }

        public void IsDeleted(int id)
        {
            var context = new MeetingRoomEntities();
            var oldBooking = context.TrBookings.FirstOrDefault(w => w.Id == id);
            oldBooking.IsDeleted = true;

            context.TrBookings.Attach(oldBooking);
            context.Entry(oldBooking).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public IEnumerable<Booking> GetAll(int pageIndex = 1, int pageSize = 10000)
        {
            var context = new MeetingRoomEntities();
            //var data = context.TrBookings.Where(w=>w.IsDeleted == false).OrderByDescending(x => x.BookingDate).AsQueryable();

            //return Mapper.Map<IEnumerable<TrBooking>, IEnumerable<Booking>>(data);            

            var data = (from a in context.TrBookings
                        join b in context.TrPayments on a.ReservedCode equals b.ReservedCode into b_join
                        from b in b_join.DefaultIfEmpty()
                        orderby
                          a.TranDate descending
                        select new Booking
                        {
                            Id = a.Id,
                            TranDate = a.TranDate.Value,
                            Name = a.Name,
                            Phone = a.Phone,
                            Email = a.Email,
                            Company = a.Company,
                            BookingCode = a.BookingCode,
                            ReservedCode = a.ReservedCode,
                            BookingDate = a.BookingDate,
                            SpecialRequest = a.SpecialRequest,
                            StartTime = a.StartTime,
                            FinishTime = a.FinishTime,
                            MeetingRoomType = a.MeetingRoomType,
                            NumberOfPeople = a.NumberOfPeople,
                            Facilities = a.Facilities,
                            LayoutId = a.LayoutId,
                            PaymentMethod = a.PaymentMethod,
                            RoomPrice = a.RoomPrice,
                            EquipmentPrice = a.EquipmentPrice,
                            FoodAndDrinkPrice = a.FoodAndDrinkPrice,
                            SubTotal = a.SubTotal,
                            Tax = a.Tax,
                            Total = a.Total
                        }).ToList();

            //List<Booking> bookingList = new List<Booking>();

            //foreach (var item in data)
            //{
            //    Booking booking = new Booking();

            //    booking.Id = item.Id;
            //    booking.Name = item.Name;
            //    booking.Phone = item.Phone;
            //    booking.Email = item.Email;
            //    booking.Company = item.Company;
            //    booking.BookingCode = item.BookingCode;
            //    booking.ReservedCode = item.ReservedCode;
            //    booking.BookingDate = Convert.ToDateTime(item.BookingDate.Value.ToString("MM/dd/yyyy"));
            //    booking.SpecialRequest = item.SpecialRequest;
            //    booking.StartTime = item.StartTime;
            //    booking.FinishTime = item.FinishTime;
            //    booking.MeetingRoomType = item.MeetingRoomType;
            //    booking.NumberOfPeople = item.NumberOfPeople;
            //    booking.Facilities = item.Facilities;
            //    booking.LayoutId = item.LayoutId;
            //    booking.PaymentMethod = item.PaymentMethod;
            //    booking.RoomPrice = item.RoomPrice;
            //    booking.EquipmentPrice = item.EquipmentPrice;
            //    booking.FoodAndDrinkPrice = item.FoodAndDrinkPrice;
            //    booking.SubTotal = item.SubTotal;
            //    booking.Tax = item.Tax;
            //    booking.Total = item.Total;

            //    bookingList.Add(booking);
            //}

            return data;
        }

        public Booking GetById(int id)
        {
            var context = new MeetingRoomEntities();
            var result = context.TrBookings.FirstOrDefault(w => w.Id == id);

            return Mapper.Map<TrBooking, Booking>(result);
        }

        public BookPayment GetBookingPayRoom(int id)
        {
            var context = new MeetingRoomEntities();

            var result = (from a in context.TrBookings
                          join b in context.MstRoomSlots on new { RoomSlotId = (int)a.RoomSlotId } equals new { RoomSlotId = b.Id }
                          join c in context.MstMeetingRoomLayouts on new { layoutId = b.layoutId } equals new { layoutId = c.Id }
                          where
                            a.Id == id
                          select new BookPayment
                          {
                              Id = a.Id,
                              Name = a.Name,
                              Phone = a.Phone,
                              Email = a.Email,
                              Company = a.Company,
                              BookingCode = a.BookingCode,
                              BookingDate = a.BookingDate.Value,
                              Total = a.Total.Value,
                              roomId = b.roomId,
                              Room = c.meetingRoomLayoutTitle
                          });

            var bookPayment = new BookPayment();
            foreach (var data in result)
            {
                bookPayment.Id = data.Id;
                bookPayment.Name = data.Name;
                bookPayment.Phone = data.Phone;
                bookPayment.Email = data.Email;
                bookPayment.Company = data.Company;
                bookPayment.BookingCode = data.BookingCode;
                bookPayment.BookingDate = data.BookingDate;
                bookPayment.Total = data.Total;
                bookPayment.roomId = data.roomId;
                bookPayment.Room = data.Room;
            }

            return bookPayment;
        }

        public void Update(Booking Booking)
        {
            var context = new MeetingRoomEntities();
            var oldBooking = context.TrBookings.FirstOrDefault(w => w.Id == Booking.Id);

            //#region Generate Auto Number
            //var countExits = context.TrBookings.Where(w => w.BookingCode.StartsWith("Book")).Count();
            //oldBooking.BookingCode = _autoNumberService.GenerateName("BOOKING", ++countExits, "BOOK");
            //#endregion         

            Mapper.Map(Booking, oldBooking);

            context.TrBookings.Attach(oldBooking);
            context.Entry(oldBooking).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void UpdateData(Booking Booking)
        {
            var context = new MeetingRoomEntities();
            var oldBooking = context.TrBookings.FirstOrDefault(w => w.Id == Booking.Id);

            int bvrTotalPrice = 0;
            int equTotalPrice = 0;

            var dataBvr = (from a in context.TrBeverageDetails
                           where
                             a.BookingId == oldBooking.Id
                           select new
                           {
                               a.Price,
                               a.Qty
                           });

            foreach (var item in dataBvr)
            {
                bvrTotalPrice = bvrTotalPrice + Convert.ToInt32(item.Price);// * Convert.ToInt32(item.Qty));
            }

            var dataEqu = (from TrEquipmentDetails in context.TrEquipmentDetails
                           where
                             TrEquipmentDetails.BookingId == oldBooking.Id
                           select new
                           {
                               TrEquipmentDetails.Price,
                               TrEquipmentDetails.Qty
                           });

            foreach (var item in dataEqu)
            {
                equTotalPrice = equTotalPrice + Convert.ToInt32(item.Price); //* Convert.ToInt32(item.Qty));
            }

            int SubTotal = Convert.ToInt32(oldBooking.RoomPrice) + bvrTotalPrice + equTotalPrice;
            decimal Tax = SubTotal * Convert.ToDecimal(0.1);
            int Total = SubTotal + Convert.ToInt32(Tax);

            oldBooking.FoodAndDrinkPrice = bvrTotalPrice;
            oldBooking.EquipmentPrice = equTotalPrice;
            oldBooking.SubTotal = SubTotal;
            oldBooking.Tax = Tax;
            oldBooking.Total = Total;

            //Mapper.Map(Booking, oldBooking);

            context.TrBookings.Attach(oldBooking);
            context.Entry(oldBooking).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public IEnumerable<BookingDetailList> GetBookingByBookCode(string bookCode)
        {
            var context = new MeetingRoomEntities();
            var data = (from a in context.TrBookings
                        join b in context.MstMeetingRoomLayouts on new { LayoutId = (int)a.LayoutId } equals new { LayoutId = b.Id }
                        join c in context.TrBeverageDetails on new { Id = a.Id } equals new { Id = c.BookingId }
                        join d in context.TrEquipmentDetails on new { Id = a.Id } equals new { Id = d.BookingId }
                        join e in context.MstMeetingRoomFoodDrinks on new { beverageId = (int)c.BeverageId } equals new { beverageId = e.Id }
                        join f in context.MstMeetingRoomEquipments on new { EquipmentId = d.EquipmentId } equals new { EquipmentId = f.Id }
                        where
                          a.BookingCode == bookCode
                        select new BookingDetailList
                        {
                            Name = a.Name,
                            Phone = a.Phone,
                            Email = a.Email,
                            Company = a.Company,
                            BookingCode = a.BookingCode,
                            BookingDate = a.BookingDate,
                            SpecialRequest = a.SpecialRequest,
                            StartTime = a.StartTime,
                            FinishTime = a.FinishTime,
                            MeetingRoomType = a.MeetingRoomType,
                            NumberOfPeople = a.NumberOfPeople,
                            Facilities = a.Facilities,
                            LayoutId = a.LayoutId,
                            LayoutName = b.meetingRoomLayoutTitle,
                            PaymentMethod = a.PaymentMethod,
                            RoomPrice = a.RoomPrice,
                            EquipmentPrice = a.EquipmentPrice,
                            FoodAndDrinkPrice = a.FoodAndDrinkPrice,
                            SubTotal = a.SubTotal,
                            Tax = a.Tax,
                            Total = a.Total,
                            Deposit = a.Deposit,

                            BookingBvrId = c.BookingId,
                            BeverageId = c.BeverageId,
                            QtyBvr = c.Qty,
                            PriceBvr = c.Price,

                            BookingEquId = d.BookingId,
                            EquipmentId = d.EquipmentId,
                            QtyEqu = d.Qty,
                            PriceEqu = d.Price,

                            BeverageName = e.meetingRoomFoodDrinkTitle,
                            BeverageDescription = e.meetingRoomFoodDrinkDescription,

                            EquipmentDescription = f.meetingRoomEquipmentDescription,
                            EquipmentName = f.meetingRoomEquipmentTitle
                        }).ToList();

            return data;
        }

        public void ConvertHTMLToPDF(string html, string fileName)
        {
            //Byte[] res = null;
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A2);
            //    pdf.Save(ms);
            //    res = ms.ToArray();
            //}

            //return res;

            //string pdf_page_size = "A4";
            //String pdf_orientation = "Portrait";

            //PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), pdf_page_size, true);
            //PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), pdf_orientation, true);

            //int webPageWidth = 0;
            //int webPageHeight = 0;

            SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();

            //converter.Options.PdfPageSize = pageSize;
            //converter.Options.PdfPageOrientation = pdfOrientation;
            //converter.Options.WebPageWidth = webPageWidth;
            //converter.Options.WebPageHeight = webPageHeight;
            //converter.Options.MarginLeft = 10;
            //converter.Options.MarginRight = 10;
            //converter.Options.MarginTop = 10;
            //converter.Options.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            //converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            //converter.Options.MarginBottom = 10;

            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(html);
            doc.Save(fileName);
            doc.Close();
        }

        public void SendMailOR(string emailClient, string id, string messageBody, SmtpServer smtpServer, string attachmentFile)
        {
            smtpServer.server = System.Configuration.ConfigurationManager.AppSettings["Server"].ToString();
            smtpServer.username = System.Configuration.ConfigurationManager.AppSettings["UserName"].ToString();
            smtpServer.password = System.Configuration.ConfigurationManager.AppSettings["Password"].ToString();
            smtpServer.port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Port"].ToString());
            smtpServer.mailSender = System.Configuration.ConfigurationManager.AppSettings["MailSender"].ToString();

            string htmlBody = messageBody;

            string from = smtpServer.mailSender;
            string to = emailClient;

            MailMessage Message = new MailMessage(from, to);
            SmtpClient client = new SmtpClient();
            client.Host = smtpServer.server;
            client.Port = smtpServer.port;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential(smtpServer.username, smtpServer.password);
            client.Timeout = 10000;

            Message.CC.Add("jic@lippo-cikarang.com");
            Message.IsBodyHtml = true;
            Message.Subject = "JIC Official Receipt";
            Message.Body = htmlBody;
            Message.Attachments.Add(new Attachment(attachmentFile));

            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate,
                X509Chain chain, SslPolicyErrors sslPolicyErrors)
            { return true; };

            client.Send(Message);
        }

        public string SendMailWOA(string emailClient, string id, string messageBody, SmtpServer smtpServer)
        {
            smtpServer.server = System.Configuration.ConfigurationManager.AppSettings["Server"].ToString();
            smtpServer.username = System.Configuration.ConfigurationManager.AppSettings["UserName"].ToString();
            smtpServer.password = System.Configuration.ConfigurationManager.AppSettings["Password"].ToString();
            smtpServer.port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Port"].ToString());
            smtpServer.mailSender = System.Configuration.ConfigurationManager.AppSettings["MailSender"].ToString();

            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(emailClient);
                mail.From = new MailAddress(smtpServer.mailSender);
                mail.Subject = "Detail Transaksi";

                mail.Body = messageBody;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = smtpServer.server;
                smtp.Timeout = 10000;
                smtp.Credentials = new System.Net.NetworkCredential
                     (smtpServer.username, smtpServer.password);
                smtp.Port = Convert.ToInt32(smtpServer.port);
                smtp.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate,
                X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
                smtp.Send(mail);
                return "sent";
            }
            catch (Exception e)
            {
                return "fail";
            }
        }

        public void SendMailInvoice(string emailClient, string id, string messageBody, SmtpServer smtpServer)//, string attachmentFile)
        {
            smtpServer.server = System.Configuration.ConfigurationManager.AppSettings["Server"].ToString();
            smtpServer.username = System.Configuration.ConfigurationManager.AppSettings["UserName"].ToString();
            smtpServer.password = System.Configuration.ConfigurationManager.AppSettings["Password"].ToString();
            smtpServer.port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Port"].ToString());
            smtpServer.mailSender = System.Configuration.ConfigurationManager.AppSettings["MailSender"].ToString();

            string htmlBody = messageBody;

            string from = smtpServer.mailSender;
            string to = emailClient;

            MailMessage Message = new MailMessage(from, to);
            SmtpClient client = new SmtpClient();
            client.Host = smtpServer.server;
            client.Port = smtpServer.port;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential(smtpServer.username, smtpServer.password);
            client.Timeout = 10000;

            Message.CC.Add("jic@lippo-cikarang.com");
            Message.IsBodyHtml = true;
            Message.Subject = "JIC Reservation Confirmation";
            Message.Body = htmlBody;
            //Message.Attachments.Add(new Attachment(attachmentFile));

            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate,
                X509Chain chain, SslPolicyErrors sslPolicyErrors)
            { return true; };

            client.Send(Message);
        }

        public IEnumerable<Booking> GetSubmitted(int submit)
        {
            var context = new MeetingRoomEntities();
            List<Booking> booking = new List<Booking>();
            IEnumerable<Booking> data = booking;

            if (submit == 1)
            {
                data = (from a in context.TrBookings
                        join b in context.TrPayments on a.ReservedCode equals b.ReservedCode into b_join
                        from b in b_join.DefaultIfEmpty()
                        where
                          b.Paid == true && a.IsDeleted == false
                        orderby
                          a.TranDate descending
                        select new Booking
                        {
                            Id = a.Id,
                            Name = a.Name,
                            Phone = a.Phone,
                            Email = a.Email,
                            Company = a.Company,
                            BookingCode = a.BookingCode,
                            ReservedCode = a.ReservedCode,
                            BookingDate = a.BookingDate,
                            SpecialRequest = a.SpecialRequest,
                            StartTime = a.StartTime,
                            FinishTime = a.FinishTime,
                            MeetingRoomType = a.MeetingRoomType,
                            NumberOfPeople = a.NumberOfPeople,
                            Facilities = a.Facilities,
                            LayoutId = a.LayoutId,
                            PaymentMethod = a.PaymentMethod,
                            RoomPrice = a.RoomPrice,
                            EquipmentPrice = a.EquipmentPrice,
                            FoodAndDrinkPrice = a.FoodAndDrinkPrice,
                            SubTotal = a.SubTotal,
                            Tax = a.Tax,
                            Total = a.Total
                        }).ToList();
            }
            else if (submit == 2)
            {
                data = (from a in context.TrBookings
                        join b in context.TrPayments on a.ReservedCode equals b.ReservedCode into b_join
                        from b in b_join.DefaultIfEmpty()
                        where
                          a.IsDeleted == false
                        orderby
                          a.TranDate descending
                        select new Booking
                        {
                            Id = a.Id,
                            Name = a.Name,
                            Phone = a.Phone,
                            Email = a.Email,
                            Company = a.Company,
                            BookingCode = a.BookingCode,
                            ReservedCode = a.ReservedCode,
                            BookingDate = a.BookingDate,
                            SpecialRequest = a.SpecialRequest,
                            StartTime = a.StartTime,
                            FinishTime = a.FinishTime,
                            MeetingRoomType = a.MeetingRoomType,
                            NumberOfPeople = a.NumberOfPeople,
                            Facilities = a.Facilities,
                            LayoutId = a.LayoutId,
                            PaymentMethod = a.PaymentMethod,
                            RoomPrice = a.RoomPrice,
                            EquipmentPrice = a.EquipmentPrice,
                            FoodAndDrinkPrice = a.FoodAndDrinkPrice,
                            SubTotal = a.SubTotal,
                            Tax = a.Tax,
                            Total = a.Total
                        }).ToList();
            }
            else
            {
                data = (from a in context.TrBookings
                        join b in context.TrPayments on a.ReservedCode equals b.ReservedCode into b_join
                        from b in b_join.DefaultIfEmpty()
                        orderby
                          a.TranDate descending
                        select new Booking
                        {
                            Id = a.Id,
                            Name = a.Name,
                            Phone = a.Phone,
                            Email = a.Email,
                            Company = a.Company,
                            BookingCode = a.BookingCode,
                            ReservedCode = a.ReservedCode,
                            BookingDate = a.BookingDate,
                            SpecialRequest = a.SpecialRequest,
                            StartTime = a.StartTime,
                            FinishTime = a.FinishTime,
                            MeetingRoomType = a.MeetingRoomType,
                            NumberOfPeople = a.NumberOfPeople,
                            Facilities = a.Facilities,
                            LayoutId = a.LayoutId,
                            PaymentMethod = a.PaymentMethod,
                            RoomPrice = a.RoomPrice,
                            EquipmentPrice = a.EquipmentPrice,
                            FoodAndDrinkPrice = a.FoodAndDrinkPrice,
                            SubTotal = a.SubTotal,
                            Tax = a.Tax,
                            Total = a.Total
                        }).ToList();
            }

            return data;
        }
    }
}
