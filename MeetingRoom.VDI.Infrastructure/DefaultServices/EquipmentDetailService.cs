﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Infrastructure.DefaultServices
{
    public class EquipmentDetailService : IEquipmentDetail
    {
        private static IEquipment _equipmentService;
        public EquipmentDetailService(IEquipment equipmentService)
        {
            Mapper.CreateMap<TrEquipmentDetail, EquipmentDetail>();
            Mapper.CreateMap<EquipmentDetail, TrEquipmentDetail>();

            _equipmentService = equipmentService;
        }

        public EquipmentDetail Create(EquipmentDetail equipment)
        {
            var context = new MeetingRoomEntities();

            var newEquipmentDetail = Mapper.Map<EquipmentDetail, TrEquipmentDetail>(equipment);

            newEquipmentDetail.IsDeleted = false;
            newEquipmentDetail.CreationTime = System.DateTime.Now;
            context.TrEquipmentDetails.Add(newEquipmentDetail);
            context.SaveChanges();

            return Mapper.Map<TrEquipmentDetail, EquipmentDetail>(newEquipmentDetail);
        }

        public EquipmentDetail GetById(int id)
        {
            var context = new MeetingRoomEntities();
            var result = context.TrEquipmentDetails.FirstOrDefault(w => w.Id == id);

            return Mapper.Map<TrEquipmentDetail, EquipmentDetail>(result);            
        }

        public IEnumerable<EquipmentDetail> GetEquipment()
        {
            var context = new MeetingRoomEntities();
            return context.TrEquipmentDetails
                           .OrderBy(x => x.Id)
                           .Select(x => new EquipmentDetail { Id = x.Id })
                           .ToList();
        }

        public IEnumerable<EquipmentDetailList> GetEquipmentDetailsById(int Id)
        {
            var context = new MeetingRoomEntities();
            var data = (from a in context.TrEquipmentDetails
                        join b in context.MstMeetingRoomEquipments on new { EquipmentId = (int)a.EquipmentId } equals new { EquipmentId = b.Id }
                        where
                          a.BookingId == Id
                        select new EquipmentDetailList
                        {
                            Id = a.Id,
                            BookingId = a.BookingId,
                            EquipmentId = a.EquipmentId,
                            Qty = a.Qty,
                            Price = a.Price,
                            EquipmentName = b.meetingRoomEquipmentTitle,
                            EquipmentDescription = b.meetingRoomEquipmentDescription
                        }).ToList();

            return data;
        }

        public void DeleteEquipmentById(int id)
        {
            var context = new MeetingRoomEntities();
            var oldEquipment = context.TrEquipmentDetails.FirstOrDefault(w => w.Id == id);

            context.TrEquipmentDetails.Remove(oldEquipment);
            context.Entry(oldEquipment).State = System.Data.Entity.EntityState.Deleted;
            context.SaveChanges();
        }

        public float GetEquipmentDetailTotalById(int Id)
        {
            float Price = 0;
            var context = new MeetingRoomEntities();
            var data = (from TrEquipmentDetails in
                        (from TrEquipmentDetails in context.TrEquipmentDetails
                         where
                           TrEquipmentDetails.BookingId == Id
                         select new
                         {
                             TrEquipmentDetails.Price,
                             Dummy = "x"
                         })

                        group TrEquipmentDetails by new { TrEquipmentDetails.Dummy } into g
                        select new
                        {
                            Price = g.Sum(p => p.Price)
                        });

            foreach (var item in data)
            {
                Price = item.Price;
            }

            return Price;
        }

        public void Update(int Id, int Qty)
        {
            var context = new MeetingRoomEntities();
            var oldEquDetails = context.TrEquipmentDetails.FirstOrDefault(w => w.Id == Id);

            Equipment equPrice = _equipmentService.GetById(oldEquDetails.EquipmentId);

            oldEquDetails.Price = Qty * equPrice.meetingRoomEquipmentPrice;
            oldEquDetails.Qty = Qty;
                        
            context.TrEquipmentDetails.Attach(oldEquDetails);
            context.Entry(oldEquDetails).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
