﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Models
{
    public class Layout
    {
        public int Id { get; set; }
        public string meetingRoomLayoutDescription { get; set; }
        public byte[] meetingRoomLayoutImages { get; set; }
        public string imageString { get; set; }
        public string meetingRoomLayoutFilePath { get; set; }
        public string meetingRoomLayoutTitle { get; set; }
        public int meetingRoomLayoutPrice { get; set; }
        public int meetingRoomLayoutCapacity { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }
}
