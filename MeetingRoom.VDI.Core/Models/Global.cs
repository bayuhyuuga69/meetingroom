﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Models
{
    public class Global
    {
        public bool IsMaintenance { get; set; }
        public int MainPoolId { get; set; }
        public int DefaultCurrencyId { get; set; }
        public int DefaultProductSubCon { get; set; }
    }
}
