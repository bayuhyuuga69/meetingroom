﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Models
{
    public class BookPayment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public string BookingCode { get; set; }
        public DateTime BookingDate { get; set; }
        public decimal Total { get; set; }
        public int roomId { get; set; }
        public string Room { get; set; }
    }
}
