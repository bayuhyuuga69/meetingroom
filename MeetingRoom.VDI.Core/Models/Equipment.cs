﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Models
{
    public class Equipment
    {
        public int Id { get; set; }
        public string meetingRoomEquipmentDescription { get; set; }
        public float meetingRoomEquipmentPrice { get; set; }
        public string meetingRoomEquipmentTitle { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }

    public class EquipmentPrice
    {
        public int Id { get; set; }
        public string meetingRoomEquipmentDescription { get; set; }
        public float meetingRoomEquipmentPrice { get; set; }
        public string meetingRoomEquipmentTitle { get; set; }
        public int qty { get; set; }
    }
}
