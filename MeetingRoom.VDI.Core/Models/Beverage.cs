﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Models
{
    public class Beverage
    {
        public int Id { get; set; }
        public string meetingRoomFoodDrinkDescription { get; set; }
        public decimal meetingRoomFoodDrinkPrice { get; set; }
        public string meetingRoomFoodDrinkTitle { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }

    public class BeveragePrice
    {
        public int Id { get; set; }
        public decimal meetingRoomFoodDrinkPrice { get; set; }
        public int qty { get; set; }
        public string meetingRoomFoodDrinkDescription { get; set; }
        public string meetingRoomFoodDrinkTitle { get; set; }
    }
}
