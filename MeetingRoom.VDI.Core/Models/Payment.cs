﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Models
{
    public class Payment
    {
        public int Id { get; set; }
        public string BookingCode { get; set; }
        public string ReservedCode { get; set; }
        public Nullable<System.DateTime> BookingDate { get; set; }
        public string ClientName { get; set; }
        public string Room { get; set; }
        public Nullable<int> RoomId { get; set; }
        public Nullable<decimal> TotalPayment { get; set; }
        public Nullable<int> PaymentMethod { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> AmountToBePaid { get; set; }
        public Nullable<int> PhoneNumber { get; set; }
        public byte[] StrukImage { get; set; }
        public string PaymentType { get; set; }
        public string RoomLayout { get; set; }
        public bool Paid { get; set; }
        public bool IsDeleted { get; set; }
    }
}
