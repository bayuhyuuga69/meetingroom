﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Models
{
    public class Room
    {
        public int Id { get; set; }
        public int MeetingRoomCapacity { get; set; }
        public string MeetingRoomDescription { get; set; }
        public float MeetingRoomPrice { get; set; }
        public bool MeetingRoomStatus { get; set; }
        public string MeetingRoomTitle { get; set; }
        public byte[] MeetingRoomImage { get; set; }
        public string MeetingRoomFilePath { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public int meetingRoomLayoutId { get; set; }
        public byte meetingRoomBookFor { get; set; }
    }
}
