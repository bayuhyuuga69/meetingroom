﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Models
{
    public class RoomSlot
    {
        public int Id { get; set; }
        public int roomId { get; set; }
        public string roomTitle { get; set; }
        public int layoutId { get; set; }
        public string layoutTitle { get; set; }
        public int capacity { get; set; }
        public byte[] LayoutImages { get; set; }
        public string imageString { get; set; }
        public Nullable<int> month { get; set; }
        public Nullable<int> year { get; set; }
        public Nullable<int> days { get; set; }
        public Nullable<byte> hour1 { get; set; }
        public Nullable<byte> hour2 { get; set; }
        public Nullable<byte> hour3 { get; set; }
        public Nullable<byte> hour4 { get; set; }
        public Nullable<byte> hour5 { get; set; }
        public Nullable<byte> hour6 { get; set; }
        public Nullable<byte> hour7 { get; set; }
        public Nullable<byte> hour8 { get; set; }
        public Nullable<byte> hour9 { get; set; }
        public Nullable<byte> hour10 { get; set; }
        public Nullable<byte> hour11 { get; set; }
        public Nullable<byte> hour12 { get; set; }
        public Nullable<byte> hour13 { get; set; }
        public Nullable<byte> isActive { get; set; }
    }
}
