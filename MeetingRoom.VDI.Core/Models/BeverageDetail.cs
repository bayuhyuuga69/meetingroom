﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Models
{
    public class BeverageDetail
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public int BeverageId { get; set; }
        public int Qty { get; set; }
        public float Price { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }

    public class BeverageDetailList
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public int BeverageId { get; set; }
        public string BeverageName { get; set; }
        public string BeverageDescription { get; set; }
        public int Qty { get; set; }
        public float Price { get; set; }
        public float Total { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }
}
