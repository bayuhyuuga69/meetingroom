﻿using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IEquipmentDetail
    {
        EquipmentDetail GetById(int id);
        IEnumerable<EquipmentDetail> GetEquipment();
        EquipmentDetail Create(EquipmentDetail equipment);
        IEnumerable<EquipmentDetailList> GetEquipmentDetailsById(int Id);
        void DeleteEquipmentById(int id);
        void Update(int Id, int Qty);

        float GetEquipmentDetailTotalById(int Id);
    }
}
