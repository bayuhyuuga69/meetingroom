﻿using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IBooking
    {
        Booking GetById(int id);
        IEnumerable<Booking> GetAll(int pageIndex = 1, int pageSize = 10);
        IEnumerable<BookingDetailList> GetBookingByBookCode(string bookCode);
        IEnumerable<Booking> GetSubmitted(int submit);
        BookPayment GetBookingPayRoom(int id);
        string Create(Booking booking);
        void Update(Booking booking);
        void UpdateData(Booking booking);
        void Delete(int id);
        void IsDeleted(int id);

        void ConvertHTMLToPDF(string html, string fileName);
        void SendMailOR(string emailClient, string id, string messageBody, SmtpServer smtpServer, string attachmentString);
        string SendMailWOA(string emailClient, string id, string messageBody, SmtpServer smtpServer);
        void SendMailInvoice(string emailClient, string id, string messageBody, SmtpServer smtpServer);//, string attachmentString);
    }
}
