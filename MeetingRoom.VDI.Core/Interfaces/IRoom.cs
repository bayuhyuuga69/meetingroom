﻿using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IRoom
    {
        Room GetById(int id);
        Room GetByName(string name);
        IEnumerable<Room> GetAll(int pageIndex = 1, int pageSize = 10);
        IEnumerable<Room> GetRoom(int id);
        IEnumerable<Room> GetAllRoom();
        RoomSlot GetLayoutImageByRoomId(int id);
        Room Create(Room room);
        void Update(Room room);
        void Delete(int id);
    }
}
