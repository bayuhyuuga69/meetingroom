﻿using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IEquipment
    {
        Equipment GetById(int id);
        Equipment GetByName(string name);
        EquipmentPrice GetPriceEquipment(int Id, int Qty);
        IEnumerable<Equipment> GetAll(int pageIndex = 1, int pageSize = GlobalSetting.DEFAULT_PAGESIZE);
        PagedData<Equipment> GetAll(List<Equipment> filter, Equipment search, int pageIndex = 1, int pageSize = GlobalSetting.DEFAULT_PAGESIZE);
        IEnumerable<Equipment> GetEquipment();
        Equipment Create(Equipment equipment);
        void Update(Equipment equipment);
        void Delete(int id);
        int GetAllEquipment();
    }
}
