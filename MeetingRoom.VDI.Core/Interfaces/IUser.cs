﻿using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IUser
    {
        User GetUserById(int userId);
        User GetUserByName(string userName);
        User GetUserByEmail(string email);

        bool IsUserExist(string email, string password);

        PagedData<User> GetAll(List<User> filter, User search, int pageIndex = 1,
            int pageSize = GlobalSetting.DEFAULT_PAGESIZE);
        IEnumerable<User> GetAll(int pageIndex = 1, int pageSize = 10);
        User Create(User user);
        void Update(User user);
        void Delete(int id);
    }
}
