﻿using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IBeverage
    {
        Beverage GetById(int id);
        Beverage GetByName(string name);
        IEnumerable<Beverage> GetAll(int pageIndex = 1, int pageSize = GlobalSetting.DEFAULT_PAGESIZE);
        PagedData<Beverage> GetAll(List<Beverage> filter, Beverage search, int pageIndex = 1, int pageSize = GlobalSetting.DEFAULT_PAGESIZE);
        BeveragePrice GetPriceFood(int Id, int Qty);
        IEnumerable<Beverage> GetBeverage();
        Beverage Create(Beverage beverage);
        void Update(Beverage beverage);
        void Delete(int id);
        int GetAllBeverage();
    }
}
