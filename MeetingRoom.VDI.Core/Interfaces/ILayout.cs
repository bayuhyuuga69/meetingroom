﻿using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface ILayout
    {
        Layout GetById(int id);
        Layout GetByName(string name);
        IEnumerable<Layout> GetAll(int pageIndex = 1, int pageSize = 10);
        IEnumerable<Layout> GetLayout();
        int GetCapacity(int capacity);
        Layout GetPriceLayout(int Id);
        Layout Create(Layout layout);
        List<Layout> GetLayoutImage();
        Layout GetLayoutImageById(int id);
        void Update(Layout layout);
        void Delete(int id);
    }
}
