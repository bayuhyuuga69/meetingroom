﻿using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Helper;
using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IBeverageDetail
    {
        BeverageDetail GetById(int id);
        IEnumerable<BeverageDetail> GetBeverage();
        BeverageDetail Create(BeverageDetail beverage);
        IEnumerable<BeverageDetailList> GetBeverageDetailsById(int Id);
        void DeleteBeverageById(int id);
        void Update(int Id, int Qty);

        float GetBeverageDetailTotalById(int Id);
    }
}
