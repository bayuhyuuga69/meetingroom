﻿using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IPayment
    {
        Payment Create(Payment Payment);
        void IsDeleted(int id);
    }
}
