﻿using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IOfflinePayment
    {
        Payment GetById(int id);
        Payment GetByName(string name);
        Payment GetByResCode(string name);
        IEnumerable<Payment> GetAll(int pageIndex = 1, int pageSize = GlobalSetting.DEFAULT_PAGESIZE);
        Payment Create(Payment payment);
        string Update(Payment payment);
        Payment GetByReservedCode(string resCode);
    }
}
