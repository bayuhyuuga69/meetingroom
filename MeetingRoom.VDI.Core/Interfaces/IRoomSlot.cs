﻿using MeetingRoom.VDI.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IRoomSlot
    {
        RoomSlot GetById(int id);
        //RoomSlot GetByName(string name);
        IEnumerable<RoomSlot> GetAll(int pageIndex = 1, int pageSize = 10);
        IEnumerable<RoomSlot> GetFilter(int? month, int? year, int? roomId);
        IEnumerable<RoomSlot> GetRoom();
        RoomSlot Create(RoomSlot roomSlot);
        void Update(RoomSlot roomSlot);
        void Delete(string month, string year, string day, string roomId);
        void DeleteById(int id);
        bool GenerateRoomSlot(int startMonth, int endMonth, int year, int roomId);
        bool CheckRoomSlot(int year, int month, int day, string startTime, string finishTime);
        int BookingRoomSlot(Booking booking);
        int GetRoomSlotId(Booking booking);

        RoomSlot GetRoomReserved(int year, int month, int day, string startTime, string finishTime);
        void UpdateRoomSlot(int id, string startTime, string endTime);
    }
}
