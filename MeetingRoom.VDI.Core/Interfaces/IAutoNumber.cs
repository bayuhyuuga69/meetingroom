﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Interfaces
{
    public interface IAutoNumber
    {
        string GenerateName(string formatName, int seq, string prefix = "");
    }
}
