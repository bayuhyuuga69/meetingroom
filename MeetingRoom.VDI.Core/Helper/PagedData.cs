﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Helper
{
    public class PagedData<T> where T : class
    {
        public IEnumerable<T> Data { get; set; }

        public int TotalRecord { get; set; }

        public int CurrentIndex { get; set; }
    }
}
