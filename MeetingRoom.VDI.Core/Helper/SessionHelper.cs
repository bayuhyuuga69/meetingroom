﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Helper
{
    public class SessionHelper
    {
        public static void SetSession(string hostName, string userName, string fullName, DateTime loginTime, int userRoleId)
        {
            SessionData.UserName = userName;
            SessionData.FullName = fullName;
            //SessionData.UserRole = new UserRoleRepository().RetrieveUserRoleByRoleId(userRoleId);
            SessionData.HostName = hostName;
            SessionData.LoginTime = loginTime;
        }

        public static string SessionUserName()
        {
            return SessionData.UserName;
        }

        public static string SessionFullName()
        {
            return SessionData.FullName;
        }

        //public static int SessionUserRoleId()
        //{
        //    return SessionData.UserRole.UserRoleID;
        //}

        //public static string SessionUserRoleName()
        //{
        //    return SessionData.UserRole.RoleName;
        //}

        public static DateTime SessionLoginTime()
        {
            return SessionData.LoginTime;
        }

        public static void ClearSession()
        {
            SessionData.UserName = null;
            SessionData.FullName = null;
            //SessionData.UserRole = null;
            SessionData.HostName = null;
            SessionData.LoginTime = new DateTime();
        }
    }

    class SessionData
    {
        public static string HostName { get; set; }

        public static DateTime LoginTime { get; set; }

        //public static secUserRole UserRole { get; set; }

        public static string UserName { get; set; }

        public static string FullName { get; set; }
    }
}
