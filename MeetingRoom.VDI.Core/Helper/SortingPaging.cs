﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Helper
{
    public class SortingPaging
    {
        public SortOrder[] SortOrders { get; set; }
        public int PageNumber { get; set; }
        public int NumberRecords { get; set; }
    }
}
