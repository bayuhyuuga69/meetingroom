﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Helper
{
    public class SortOrder
    {
        public enum OrderBy
        {
            Ascending,
            Descending
        }
        public string ColumnName { get; set; }
        public OrderBy ColumnOrder { get; set; }
    }
}
