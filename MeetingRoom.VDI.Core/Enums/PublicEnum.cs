﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingRoom.VDI.Core.Enums
{
    public enum EnumFormActionMode
    {
        Add = 1,
        Edit = 2,
        Delete = 3,
        Export = 4,
        Download = 5,
        Upload = 6
    }

    public enum EnumFormApprovalMode
    {
        WithApproval, WithoutApproval
    }
}
