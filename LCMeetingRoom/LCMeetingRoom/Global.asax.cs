﻿using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Infrastructure.DefaultServices;
using NLog;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace LCMeetingRoom
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = new Container();
            container.Register<IEquipment, EquipmentService>(Lifestyle.Singleton);
            container.Register<IBeverage, BeverageService>(Lifestyle.Singleton);
            container.Register<ILayout, LayoutService>(Lifestyle.Singleton);
            container.Register<IRoom, RoomService>(Lifestyle.Singleton);
            container.Register<IBooking, BookingService>(Lifestyle.Singleton);
            container.Register<IRoomSlot, RoomSlotService>(Lifestyle.Singleton);
            container.Register<IUser, UserService>(Lifestyle.Singleton);
            container.Register<IPayment, PaymentService>(Lifestyle.Singleton);
            container.Register<IAutonumber, AutoNumberService>(Lifestyle.Singleton);
            container.Register<IOfflinePayment, OfflinePaymentService>(Lifestyle.Singleton);
            container.Register<IBeverageDetail, BeverageDetailService>(Lifestyle.Singleton);
            container.Register<IEquipmentDetail, EquipmentDetailService>(Lifestyle.Singleton);
            //container.Register<ILogger, LoggerService>(Lifestyle.Singleton);
            
            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }

}
