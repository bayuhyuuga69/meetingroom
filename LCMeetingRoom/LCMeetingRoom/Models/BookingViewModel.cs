﻿using Mvc.JQuery.DataTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Models
{
    public class BookingViewModel
    {
        public int Id { get; set; }
        [DataTables(DisplayName = "Name", Sortable = false)]
        public string Name { get; set; }
        [DataTables(DisplayName = "Phone Number", Sortable = false)]
        public string Phone { get; set; }
        [DataTables(DisplayName = "E-Mail", Sortable = false)]
        public string Email { get; set; }
        [DataTables(DisplayName = "Company Name", Sortable = false)]
        public string Company { get; set; }
        [DataTables(DisplayName = "Booking Code", Sortable = false)]
        public string BookingCode { get; set; }
        [DataTables(DisplayName = "Booking Date", Sortable = false)]
        public Nullable<System.DateTime> BookingDate { get; set; }
        [DataTables(DisplayName = "Special Request", Sortable = false)]
        public string SpecialRequest { get; set; }
        [DataTables(DisplayName = "Start Time", Sortable = false)]
        public Nullable<System.TimeSpan> StartTime { get; set; }
        [DataTables(DisplayName = "Finish Time", Sortable = false)]
        public Nullable<System.TimeSpan> FinishTime { get; set; }
        [DataTables(DisplayName = "Meeting Room Type", Sortable = false)]
        public Nullable<int> MeetingRoomType { get; set; }
        [DataTables(DisplayName = "Number Of People", Sortable = false)]
        public Nullable<int> NumberOfPeople { get; set; }
        [DataTables(DisplayName = "Facilities", Sortable = false)]
        public Nullable<int> Facilities { get; set; }
        [DataTables(DisplayName = "Layout", Sortable = false)]
        public Nullable<int> LayoutId { get; set; }
        [DataTables(DisplayName = "Payment Method", Sortable = false)]
        public Nullable<int> PaymentMethod { get; set; }
        [DataTables(DisplayName = "Room Price", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> RoomPrice { get; set; }
        [DataTables(DisplayName = "Equipment Price", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> EquipmentPrice { get; set; }
        [DataTables(DisplayName = "Food And Drink", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> FoodAndDrinkPrice { get; set; }
        [DataTables(DisplayName = "Sub Total", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> SubTotal { get; set; }
        [DataTables(DisplayName = "Tax", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> Tax { get; set; }
        [DataTables(DisplayName = "Total", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> Total { get; set; }
        [DataTables(DisplayName = "Deposit", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> Deposit { get; set; }
    }

    public class CreateBookingViewModel
    {
        public int Id { get; set; }
        [DataTables(DisplayName = "Name", Sortable = false)]
        public string Name { get; set; }
        [DataTables(DisplayName = "Phone Number", Sortable = false)]
        public string Phone { get; set; }
        [DataTables(DisplayName = "E-Mail", Sortable = false)]
        public string Email { get; set; }
        [DataTables(DisplayName = "Company Name", Sortable = false)]
        public string Company { get; set; }
        [DataTables(DisplayName = "Booking Code", Sortable = false)]
        public string BookingCode { get; set; }
        [DataTables(DisplayName = "Booking Date", Sortable = false)]
        public Nullable<System.DateTime> BookingDate { get; set; }
        [DataTables(DisplayName = "Special Request", Sortable = false)]
        public string SpecialRequest { get; set; }
        [DataTables(DisplayName = "Start Time", Sortable = false)]
        public Nullable<System.TimeSpan> StartTime { get; set; }
        [DataTables(DisplayName = "Finish Time", Sortable = false)]
        public Nullable<System.TimeSpan> FinishTime { get; set; }
        [DataTables(DisplayName = "Meeting Room Type", Sortable = false)]
        public Nullable<int> MeetingRoomType { get; set; }
        [DataTables(DisplayName = "Number Of People", Sortable = false)]
        public Nullable<int> NumberOfPeople { get; set; }
        [DataTables(DisplayName = "Facilities", Sortable = false)]
        public Nullable<int> Facilities { get; set; }
        [DataTables(DisplayName = "Layout", Sortable = false)]
        public Nullable<int> LayoutId { get; set; }
        [DataTables(DisplayName = "Payment Method", Sortable = false)]
        public Nullable<int> PaymentMethod { get; set; }
        [DataTables(DisplayName = "Room Price", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> RoomPrice { get; set; }
        [DataTables(DisplayName = "Equipment Price", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> EquipmentPrice { get; set; }
        [DataTables(DisplayName = "Food And Drink", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> FoodAndDrinkPrice { get; set; }
        [DataTables(DisplayName = "Sub Total", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> SubTotal { get; set; }
        [DataTables(DisplayName = "Tax", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> Tax { get; set; }
        [DataTables(DisplayName = "Total", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> Total { get; set; }
        [DataTables(DisplayName = "Deposit", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> Deposit { get; set; }
    }

    public class EditBookingViewModel
    {
        public int Id { get; set; }
        [DataTables(DisplayName = "Name", Sortable = false)]
        public string Name { get; set; }
        [DataTables(DisplayName = "Phone Number", Sortable = false)]
        public string Phone { get; set; }
        [DataTables(DisplayName = "E-Mail", Sortable = false)]
        public string Email { get; set; }
        [DataTables(DisplayName = "Company Name", Sortable = false)]
        public string Company { get; set; }
        [DataTables(DisplayName = "Booking Code", Sortable = false)]
        public string BookingCode { get; set; }
        [DataTables(DisplayName = "Booking Date", Sortable = false)]
        public Nullable<System.DateTime> BookingDate { get; set; }
        [DataTables(DisplayName = "Special Request", Sortable = false)]
        public string SpecialRequest { get; set; }
        [DataTables(DisplayName = "Start Time", Sortable = false)]
        public Nullable<System.TimeSpan> StartTime { get; set; }
        [DataTables(DisplayName = "Finish Time", Sortable = false)]
        public Nullable<System.TimeSpan> FinishTime { get; set; }
        [DataTables(DisplayName = "Meeting Room Type", Sortable = false)]
        public Nullable<int> MeetingRoomType { get; set; }
        [DataTables(DisplayName = "Number Of People", Sortable = false)]
        public Nullable<int> NumberOfPeople { get; set; }
        [DataTables(DisplayName = "Facilities", Sortable = false)]
        public Nullable<int> Facilities { get; set; }
        [DataTables(DisplayName = "Layout", Sortable = false)]
        public Nullable<int> LayoutId { get; set; }
        [DataTables(DisplayName = "Payment Method", Sortable = false)]
        public Nullable<int> PaymentMethod { get; set; }
        [DataTables(DisplayName = "Room Price", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> RoomPrice { get; set; }
        [DataTables(DisplayName = "Equipment Price", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> EquipmentPrice { get; set; }
        [DataTables(DisplayName = "Food And Drink", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> FoodAndDrinkPrice { get; set; }
        [DataTables(DisplayName = "Sub Total", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> SubTotal { get; set; }
        [DataTables(DisplayName = "Tax", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> Tax { get; set; }
        [DataTables(DisplayName = "Total", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> Total { get; set; }
        [DataTables(DisplayName = "Deposit", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public Nullable<decimal> Deposit { get; set; }
    }

    public class CreateBookingBeverageDetail
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public int BeverageId { get; set; }
        public int Qty { get; set; }
        public float Price { get; set; }
    }

    public class CreateBookingEquipmentDetail
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public int EquipmentId { get; set; }
        public int Qty { get; set; }
        public float Price { get; set; }
    }

}