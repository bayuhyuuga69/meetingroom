﻿using Mvc.JQuery.DataTables;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Models
{
    public class BeverageViewModel
    {
        public int Id { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string meetingRoomFoodDrinkDescription { get; set; }
        [DisplayName("Price")]
        [DataTables(Width = "50px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public float meetingRoomFoodDrinkPrice { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "50px")]
        public string meetingRoomFoodDrinkTitle { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }

    public class CreateBeverageViewModel
    {
        public int Id { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string meetingRoomFoodDrinkDescription { get; set; }
        [DisplayName("Price")]
        [DataTables(Width = "50px")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public float meetingRoomFoodDrinkPrice { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "50px")]
        public string meetingRoomFoodDrinkTitle { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }

    public class EditBeverageViewModel
    {
        public int Id { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string meetingRoomFoodDrinkDescription { get; set; }
        [DisplayName("Price")]
        [DataTables(Width = "50px")]
        public float meetingRoomFoodDrinkPrice { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "50px")]
        public string meetingRoomFoodDrinkTitle { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }
}