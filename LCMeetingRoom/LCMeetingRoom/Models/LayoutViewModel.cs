﻿using Mvc.JQuery.DataTables;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Models
{
    public class LayoutViewModel
    {
        public int Id { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string meetingRoomLayoutDescription { get; set; }
        [DisplayName("Image Layout")]
        [DataTables(Width = "50px")]
        public byte[] meetingRoomLayoutImages { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "50px")]
        public string meetingRoomLayoutTitle { get; set; }
        [DisplayName("Price")]
        [DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        public int meetingRoomLayoutPrice { get; set; }
        [DisplayName("Capacity")]
        public int meetingRoomLayoutCapacity { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }

    public class CreateLayoutViewModel
    {
        public int Id { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string meetingRoomLayoutDescription { get; set; }
        [DisplayName("Image Layout")]
        [DataTables(Width = "50px")]
        public byte[] meetingRoomLayoutImages { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "50px")]
        public string meetingRoomLayoutTitle { get; set; }
        [DisplayName("Price")]
        [DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        public int meetingRoomLayoutPrice { get; set; }
        [DisplayName("Capacity")]
        public int meetingRoomLayoutCapacity { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }

    public class EditLayoutViewModel
    {
        public int Id { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string meetingRoomLayoutDescription { get; set; }
        [DisplayName("Image Layout")]
        [DataTables(Width = "50px")]
        public byte[] meetingRoomLayoutImages { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "50px")]
        public string meetingRoomLayoutTitle { get; set; }
        [DisplayName("Price")]
        //[DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        public int meetingRoomLayoutPrice { get; set; }
        [DisplayName("Capacity")]
        public int meetingRoomLayoutCapacity { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }
}