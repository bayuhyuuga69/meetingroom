﻿//using Mvc.JQuery.Datatables;
using Mvc.JQuery.DataTables;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        [DisplayName("User Name")]
        [DataTables(Width = "100px")]
        public string userName { get; set; }
        [DisplayName("Name")]
        [DataTables(Width = "100px")]
        public string surName { get; set; }
        [DisplayName("Last Name")]
        [DataTables(Width = "100px")]
        public string lastName { get; set; }
        [DisplayName("Email Name")]
        [DataTables(Width = "100px")]
        public string Email { get; set; }
        [DisplayName("No. HP")]
        [DataTables(Width = "100px")]
        public int NoHP { get; set; }
        [DisplayName("Password")]
        [DataTables(Width = "100px")]
        public string Password { get; set; }
        public Nullable<byte> IsAdmin { get; set; }
    }

    public class CreateUserViewModel
    {
        public int Id { get; set; }
        [DisplayName("User Name")]
        [DataTables(Width = "100px")]
        public string userName { get; set; }
        [DisplayName("Name")]
        [DataTables(Width = "100px")]
        public string surName { get; set; }
        [DisplayName("Last Name")]
        [DataTables(Width = "100px")]
        public string lastName { get; set; }
        [DisplayName("Email Name")]
        [DataTables(Width = "100px")]
        public string Email { get; set; }
        [DisplayName("No. HP")]
        [DataTables(Width = "100px")]
        public int NoHP { get; set; }
        [DisplayName("Password")]
        [DataTables(Width = "100px")]
        public string Password { get; set; }
        public Nullable<byte> IsAdmin { get; set; }
    }

    public class EditUserViewModel
    {
        public int Id { get; set; }
        [DisplayName("User Name")]
        [DataTables(Width = "100px")]
        public string userName { get; set; }
        [DisplayName("Name")]
        [DataTables(Width = "100px")]
        public string surName { get; set; }
        [DisplayName("Last Name")]
        [DataTables(Width = "100px")]
        public string lastName { get; set; }
        [DisplayName("Email Name")]
        [DataTables(Width = "100px")]
        public string Email { get; set; }
        [DisplayName("No. HP")]
        [DataTables(Width = "100px")]
        public int NoHP { get; set; }
        [DisplayName("Password")]
        [DataTables(Width = "100px")]
        public string Password { get; set; }
        public Nullable<byte> IsAdmin { get; set; }
    }

    public class ChangePasswordViewModel
    {
        public int Id { get; set; }
        [DisplayName("User Name")]
        [DataTables(Width = "100px")]
        public string userName { get; set; }
        [DisplayName("Name")]
        [DataTables(Width = "100px")]
        public string surName { get; set; }
        [DisplayName("Last Name")]
        [DataTables(Width = "100px")]
        public string lastName { get; set; }
        [DisplayName("Email Name")]
        [DataTables(Width = "100px")]
        public string Email { get; set; }
        [DisplayName("No. HP")]
        [DataTables(Width = "100px")]
        public int NoHP { get; set; }
        [DisplayName("Current Password")]
        [DataTables(Width = "100px")]
        public string CurrentPassword { get; set; }
        [DisplayName("Password")]
        [DataTables(Width = "100px")]
        public string Password { get; set; }
        public Nullable<byte> IsAdmin { get; set; }
    }

    public class LoginViewModel
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}