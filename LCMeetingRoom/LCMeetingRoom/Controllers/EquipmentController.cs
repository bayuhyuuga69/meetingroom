﻿using AutoMapper;
using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
//using Mvc.JQuery.Datatables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class EquipmentController : Controller
    {
        private readonly IEquipment _equipmentService;

        public EquipmentController(IEquipment equipmentService)
        {
            _equipmentService = equipmentService;
        }
        public ActionResult EquipmentList()
        {
            return PartialView();
        }

        // GET: Equipment
        public ActionResult Index()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _equipmentService.GetAll();

            return View(output);
        }

        // GET: Equipment/Details/5
        public ActionResult Details(int id)
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var equipment = _equipmentService.GetById(id);

            Mapper.CreateMap<Equipment, EquipmentViewModel>();
            var model = Mapper.Map<Equipment, EquipmentViewModel>(equipment);

            return View(model);
        }

        // GET: Equipment/Create
        public ActionResult Create()
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var model = new CreateEquipmentViewModel();

            return View(model);
        }

        // POST: Equipment/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateEquipmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<CreateEquipmentViewModel, Equipment>();
                    //.ForMember(s => s.Brand, o => o.Ignore());

                var newEquipment = Mapper.Map<CreateEquipmentViewModel, Equipment>(model);

                //newSubBrand.IBy = User.Identity.Name;
                _equipmentService.Create(newEquipment);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Equipment/Edit/5
        public ActionResult Edit(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var equipment = _equipmentService.GetById(id);

            Mapper.CreateMap<Equipment, EditEquipmentViewModel>();
            var model = Mapper.Map<Equipment, EditEquipmentViewModel>(equipment);

            return View(model);
        }

        // POST: Equipment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EditEquipmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<EditEquipmentViewModel, Equipment>();
                    //.ForMember(s => s.Brand, o => o.Ignore());

                var editEquipment = Mapper.Map<EditEquipmentViewModel, Equipment>(model);
                //editBrand.UBy = User.Identity.Name;

                _equipmentService.Update(editEquipment);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Equipment/Delete/5
        public ActionResult Delete(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var equipment = _equipmentService.GetById(id);

            Mapper.CreateMap<Equipment, EditEquipmentViewModel>();
            var model = Mapper.Map<Equipment, EditEquipmentViewModel>(equipment);

            return View(model);
        }

        // POST: Equipment/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, EditEquipmentViewModel model)
        {
            try
            {
                Mapper.CreateMap<EditEquipmentViewModel, Beverage>();
                var deleteEquipment = Mapper.Map<EditEquipmentViewModel, Beverage>(model);

                _equipmentService.Delete(deleteEquipment.Id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public JsonResult GetEquipment()
        {
            var equipmentList = _equipmentService.GetEquipment();

            //JsonResult categoryJson = new JsonResult();
            //categoryJson.Data = equipmentList;

            //return categoryJson;

            return Json(equipmentList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllEquipment()
        {
            var equipmentList = _equipmentService.GetAll();
            return Json(equipmentList, JsonRequestBehavior.AllowGet);
        }

        //public DataTablesResult<EquipmentGridViewModel> GetEquipment(DataTablesParam dataTableParam, string filterData = "")
        //{
        //    if (dataTableParam.iDisplayLength == 0)
        //        dataTableParam.iDisplayLength = GlobalSetting.DEFAULT_PAGESIZE;

        //    var pageIndex = Convert.ToInt32(Math.Ceiling((double)dataTableParam.iDisplayStart / dataTableParam.iDisplayLength) + 1);

        //    List<Equipment> filter = null;
        //    if (!String.IsNullOrEmpty(filterData))
        //        filter = JsonConvert.DeserializeObject<List<Equipment>>(filterData);

        //    Equipment search = null;
        //    if (!String.IsNullOrEmpty(dataTableParam.sSearch))
        //    {
        //        search = new Equipment();
        //        search.meetingRoomEquipmentTitle = dataTableParam.sSearch;
        //        search.meetingRoomEquipmentDescription = dataTableParam.sSearch;
        //    }

        //    var pagedData = _equipmentService.GetAll(filter, search, pageIndex, dataTableParam.iDisplayLength);

        //    Mapper.CreateMap<Equipment, EquipmentGridViewModel>();
        //    var data = Mapper.Map<IEnumerable<Equipment>, IEnumerable<EquipmentGridViewModel>>(pagedData.Data);

        //    dataTableParam.iDisplayStart = 0;
        //    dataTableParam.sSearch = "";
        //    var result = DataTablesResult.Create(data.AsQueryable(), dataTableParam,
        //        uv => new
        //        {
        //            //IOn = uv.Cre.ToString("dd MMM yyyy")
        //        });


        //    result.Data.iTotalRecords = pagedData.TotalRecord;
        //    result.Data.iTotalDisplayRecords = pagedData.TotalRecord;

        //    return result;
        //}

        public JsonResult GetPriceEquipment(int Id, int Qty)
        {
            var equipmentList = _equipmentService.GetPriceEquipment(Id, Qty);
            return Json(equipmentList, JsonRequestBehavior.AllowGet);
        }
    }
}
