﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class BookingListController : Controller
    {
        private readonly IBooking _bookingService;
        private readonly IBeverageDetail _bookingBeverageService;
        private readonly IEquipmentDetail _bookingEquipmentService;      

        public BookingListController(IBooking bookingService, IBeverageDetail bookingBeverageService, IEquipmentDetail bookingEquipmentService)
        {
            _bookingService = bookingService;
            _bookingBeverageService = bookingBeverageService;
            _bookingEquipmentService = bookingEquipmentService;
        }

        // GET: BookingList
        public ActionResult Index()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _bookingService.GetAll();

            return View(output);
        }

        public ActionResult ViewDetail(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var bookingView = _bookingService.GetById(id);

            Mapper.CreateMap<Booking, EditBookingViewModel>();
            var model = Mapper.Map<Booking, EditBookingViewModel>(bookingView);

            ViewBag.bookingId = bookingView.Id;

            return View(model);
        }

        public JsonResult GetBeverageById(int Id)
        {
            var beverageList = _bookingBeverageService.GetBeverageDetailsById(Id);
            return Json(beverageList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEquipmentById(int Id)
        {
            var equipmentList = _bookingEquipmentService.GetEquipmentDetailsById(Id);
            return Json(equipmentList, JsonRequestBehavior.AllowGet);
        }      

        [HttpPost]
        public JsonResult DeleteBeverageById(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _bookingBeverageService.DeleteBeverageById(id);
                //return RedirectToAction("Index");
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                //return View();
                return Json("Gagal", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteEquipmentById(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _bookingEquipmentService.DeleteEquipmentById(id);
                //return RedirectToAction("Index");
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                //return View();
                return Json("Gagal", JsonRequestBehavior.AllowGet);
            }
        }
    }
}