﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class PaymentController : Controller
    {
        private readonly IPayment _paymentService;
        private readonly IBooking _bookingService;

        public PaymentController(IPayment paymentService, IBooking bookingService)
        {
            _paymentService = paymentService;
            _bookingService = bookingService;
        }
        
        // GET: Payment
        public ActionResult Index(int Id)
        {
            var model = _bookingService.GetById(Id);
            ViewBag.bookingId = Id;
            return View();
        }

        public JsonResult SaveData(
                            string BookingCode,
                            string BookingDate,
                            string ClientName,
                            string RoomId,
                            string TotalPayment,
                            string PaymentMethod,
                            string Amount,
                            string AmountToBePaid,
                            string PhoneNumber
            )
        {
            if (ModelState.IsValid)
            {
                var paymentData = new CreatePaymentViewModel();

                paymentData.BookingCode = BookingCode;
                paymentData.BookingDate = Convert.ToDateTime(BookingDate);
                paymentData.ClientName = ClientName;
                paymentData.RoomId = Convert.ToInt32(RoomId);
                paymentData.TotalPayment = Convert.ToDecimal(TotalPayment);
                paymentData.PaymentMethod = Convert.ToInt32(PaymentMethod);
                paymentData.Amount = Convert.ToDecimal(Amount);
                paymentData.AmountToBePaid = Convert.ToDecimal(AmountToBePaid);
                paymentData.PhoneNumber = Convert.ToInt32(PhoneNumber);

                Mapper.CreateMap<CreatePaymentViewModel, Payment>();
                var newPayment = Mapper.Map<CreatePaymentViewModel, Payment>(paymentData);
               _paymentService.Create(newPayment);

                return Json("Success", JsonRequestBehavior.AllowGet);
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPayment(int Id)
        {
            var payment = _bookingService.GetBookingPayRoom(Id);
            return Json(payment, JsonRequestBehavior.AllowGet);
        }
    }
}