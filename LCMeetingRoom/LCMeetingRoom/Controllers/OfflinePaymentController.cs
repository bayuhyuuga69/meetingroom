﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class OfflinePaymentController : Controller
    {
        private readonly IOfflinePayment _offlinePaymentService;

        public OfflinePaymentController(IOfflinePayment offlinePaymentService)
        {
            _offlinePaymentService = offlinePaymentService;
        }

        // GET: OfflinePayment
        public ActionResult Index()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _offlinePaymentService.GetAll();

            return View(output);
        }

        // GET: OfflinePayment/Edit/5
        public ActionResult Edit(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var payment = _offlinePaymentService.GetById(id);

            Mapper.CreateMap<Payment, EditPaymentViewModel>();
            var model = Mapper.Map<Payment, EditPaymentViewModel>(payment);

            return View(model);
        }

        // POST: OfflinePayment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EditPaymentViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<EditPaymentViewModel, Payment>();
                //.ForMember(s => s.Brand, o => o.Ignore());

                var editPayment = Mapper.Map<EditPaymentViewModel, Payment>(model);
                //editBrand.UBy = User.Identity.Name;

                _offlinePaymentService.Update(editPayment);

                return RedirectToAction("Index");
            }

            return View(model);
        }
    }
}