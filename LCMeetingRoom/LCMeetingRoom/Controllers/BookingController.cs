﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;
using System.Web.Hosting;
using System.IO;

namespace LCMeetingRoom.Controllers
{
    public class BookingController : Controller
    {
        private readonly IBooking _bookingService;
        private readonly IBeverageDetail _bookingBeverageService;
        private readonly IEquipmentDetail _bookingEquipmentService;
        private readonly ILayout _layoutService;
        private readonly IBeverage _beverageService;
        private readonly IEquipment _equipmentService;
        private static List<CreateBookingBeverageDetail> _beverageDetail;
        private static List<CreateBookingEquipmentDetail> _equipmentDetail;
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private static int bookingId = 0;
        private static string strMessage = "";

        public BookingController(IBooking bookingService, IBeverageDetail bookingBeverageService, IEquipmentDetail bookingEquipmentService,
            ILayout layoutService, IBeverage beverageService, IEquipment equipmentService)
        {
            _bookingService = bookingService;
            _bookingBeverageService = bookingBeverageService;
            _bookingEquipmentService = bookingEquipmentService;
            _layoutService = layoutService;
            _beverageService = beverageService;
            _equipmentService = equipmentService;
        }

        // GET: Booking
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Payment()
        {
            return View();
        }

        public ActionResult Thanks()
        {
            return View();
        }

        public JsonResult SaveData(string Name,
                            string Phone,
                            string Email,
                            string Company,
                            string BookingCode,
                            string BookingDate,
                            string SpecialRequest,
                            string StartTime,
                            string FinishTime,
                            string MeetingRoomType,
                            string NumberOfPeople,
                            string Facilities,
                            string LayoutId,
                            string PaymentMethod,
                            string RoomPrice,
                            string EquipmentPrice,
                            string FoodAndDrinkPrice,
                            string SubTotal,
                            string Tax,
                            string Total,
                            string Deposit)
        {
            var a = _equipmentDetail;
            string EmailSent = "Email";
            try
            {
                if (ModelState.IsValid)
                {
                    var bookingData = new CreateBookingViewModel();

                    if (EquipmentPrice == "")
                    {
                        EquipmentPrice = "0";
                    }

                    if (FoodAndDrinkPrice == "")
                    {
                        FoodAndDrinkPrice = "0";
                    }

                    if (SubTotal == "")
                    {
                        SubTotal = "0";
                    }

                    if (Tax == "")
                    {
                        Tax = "0";
                    }

                    if (Total == "")
                    {
                        Total = "0";
                    }
                                        
                    bookingData.Name = Name;
                    bookingData.Phone = Phone;
                    bookingData.Email = Email;
                    bookingData.Company = Company;
                    bookingData.BookingCode = BookingCode;
                    bookingData.BookingDate = Convert.ToDateTime(BookingDate);
                    bookingData.SpecialRequest = SpecialRequest;
                    bookingData.StartTime = TimeSpan.Parse(StartTime);
                    bookingData.FinishTime = TimeSpan.Parse(FinishTime);
                    bookingData.MeetingRoomType = Convert.ToInt32(MeetingRoomType);
                    bookingData.NumberOfPeople = Convert.ToInt32(NumberOfPeople);
                    bookingData.Facilities = Convert.ToInt32(Facilities);
                    bookingData.LayoutId = Convert.ToInt32(LayoutId);
                    bookingData.PaymentMethod = Convert.ToInt32(PaymentMethod);
                    bookingData.RoomPrice = Convert.ToDecimal(RoomPrice);
                    bookingData.EquipmentPrice = Convert.ToDecimal(EquipmentPrice);
                    bookingData.FoodAndDrinkPrice = Convert.ToDecimal(FoodAndDrinkPrice) > 0 ? Convert.ToDecimal(FoodAndDrinkPrice) : 0;
                    bookingData.SubTotal = Convert.ToDecimal(SubTotal) > 0 ? Convert.ToDecimal(SubTotal) : 0;
                    bookingData.Tax = Convert.ToDecimal(Tax) > 0 ? Convert.ToDecimal(Tax) : 0;
                    bookingData.Total = Convert.ToDecimal(Total) > 0 ? Convert.ToDecimal(Total) : 0;
                    bookingData.Deposit = Convert.ToDecimal(Deposit) > 0 ? Convert.ToDecimal(Total) : 0;

                    Mapper.CreateMap<CreateBookingViewModel, Booking>();

                    var newBooking = Mapper.Map<CreateBookingViewModel, Booking>(bookingData);

                    SmtpServer smtpServer = new SmtpServer();

                    strMessage = _bookingService.Create(newBooking);
                    string strMessagePath = strMessage;
                    try
                    {
                        string success = strMessage.Substring(0, 7);
                        if (success.Equals("Success"))
                        {
                            strMessage = "Success";
                        }
                    }
                    catch (Exception exception)
                    {
                        logger.Error(exception);
                    }

                    switch (strMessage)
                    {
                        case "Full":
                            strMessage = "Full";
                            break;
                        case "NotGenerated":
                            strMessage = "NotGenerated";
                            break;
                        case "Success":
                            strMessage = "Success";
                            string x = strMessagePath.Substring(7, strMessagePath.Length - 7);
                            bookingId = Convert.ToInt32(x);

                            string messageBody = System.IO.File.ReadAllText(HostingEnvironment.MapPath(@"~/MailTemplate/Invoice_Reservation.html"));
                            string messageBodyAttachment = System.IO.File.ReadAllText(HostingEnvironment.MapPath(@"~/MailTemplate/JIC_payment.html"));

                            Booking booking = _bookingService.GetById(bookingId);
                            Layout layoutModel = _layoutService.GetById(booking.LayoutId.Value);

                            if (booking.EquipmentPrice == 0.0000M)
                            {
                                booking.EquipmentPrice = 0;
                            }

                            if (booking.FoodAndDrinkPrice == 0.0000M)
                            {
                                booking.FoodAndDrinkPrice = 0;
                            }

                            DateTime dt = DateTime.Now;
                            
                            TimeSpan timeAdd = TimeSpan.FromHours(3);
                            TimeSpan time = DateTime.Now.TimeOfDay;
                            time = time.Add(timeAdd);
                            messageBody = messageBody.Replace("@ReservationDate+3hours", dt.ToString("dd/MM/yyyy") + " - " + time.ToString(@"hh\:mm"));

                            messageBody = messageBody.Replace("@NamaCustomer", Name);

                            messageBody = messageBody.Replace("@NamaCustomer", Name);
                            messageBody = messageBody.Replace("@NomorReservasi", booking.ReservedCode);
                            messageBody = messageBody.Replace("@NamaRuangan", layoutModel.meetingRoomLayoutTitle);
                            messageBody = messageBody.Replace("@CaraPembayaran", "Transfer");
                            messageBody = messageBody.Replace("@EmailCustomer", booking.Email);
                            messageBody = messageBody.Replace("@JamMulai", booking.BookingDate.ToString());
                            messageBody = messageBody.Replace("@JamSelesai", booking.BookingDate.ToString());
                            messageBody = messageBody.Replace("@WaktuMulai", booking.BookingDate.Value.ToString("dd/MM/yyyy") + " - " + booking.StartTime.ToString());
                            messageBody = messageBody.Replace("@WaktuSelesai", booking.BookingDate.Value.ToString("dd/MM/yyyy") + " - " + booking.FinishTime.ToString());
                            messageBody = messageBody.Replace("@HargaRuangan", booking.RoomPrice.Value.ToString("#,###"));
                            messageBody = messageBody.Replace("@HargaPerlengkapan", booking.EquipmentPrice == 0 ? "0.0000" : booking.EquipmentPrice.Value.ToString("#,###"));
                            messageBody = messageBody.Replace("@HargaKonsumsi", booking.FoodAndDrinkPrice == 0 ? "0.0000" : booking.FoodAndDrinkPrice.Value.ToString("#,###"));
                            messageBody = messageBody.Replace("@HargaBruto", booking.SubTotal.Value.ToString("#,###"));
                            messageBody = messageBody.Replace("@Pajak", booking.Tax.Value.ToString("#,###"));
                            messageBody = messageBody.Replace("@HargaNet", booking.Total.Value.ToString("#,###"));

                            //using (FileStream file = new FileStream(filePath, FileMode.Create))
                            //{
                            //    file.Write(pdfFile, 0, pdfFile.Length);
                            //}

                            _bookingService.SendMailInvoice(newBooking.Email, newBooking.Id.ToString(), messageBody, smtpServer);//, filePath);
                            //EmailSent = _bookingService.SendMailWOA(newBooking.Email, newBooking.Id.ToString(), messageBody, smtpServer);
                            break;
                        default:
                            strMessage = "NA";
                            break;
                    }

                    return Json(strMessage, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exception)
            {
                logger.Error(exception);
            }

            return Json(strMessage, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveDataBeverageDetail(
                                               List<CreateBookingBeverageDetail> foodData
                                                )
        {
            if (ModelState.IsValid)
            {
                var bookingData = new CreateBookingBeverageDetail();

                if (foodData != null)
                {
                    foreach (var item in foodData)
                    {
                        bookingData.BookingId = bookingId;
                        bookingData.BeverageId = item.Id;
                        bookingData.Qty = Convert.ToInt32(item.Qty);
                        bookingData.Price = Convert.ToInt32(item.Price);

                        Mapper.CreateMap<CreateBookingBeverageDetail, BeverageDetail>();
                        var newBooking = Mapper.Map<CreateBookingBeverageDetail, BeverageDetail>(bookingData);

                        _bookingBeverageService.Create(newBooking);
                    }
                }

                //return Json("Success", JsonRequestBehavior.AllowGet);
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveDataEquipmentDetail(
                                                List<CreateBookingEquipmentDetail> equipmentData
                                                )
        {
            if (ModelState.IsValid)
            {
                var bookingData = new CreateBookingEquipmentDetail();

                if (equipmentData != null)
                {
                    foreach (var item in equipmentData)
                    {
                        bookingData.BookingId = bookingId;
                        bookingData.EquipmentId = item.Id;
                        bookingData.Qty = Convert.ToInt32(item.Qty);
                        bookingData.Price = Convert.ToInt32(item.Price);

                        Mapper.CreateMap<CreateBookingEquipmentDetail, EquipmentDetail>();
                        var newBooking = Mapper.Map<CreateBookingEquipmentDetail, EquipmentDetail>(bookingData);

                        _bookingEquipmentService.Create(newBooking);
                    }
                }
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetBookingByBookCode(string bookCode)
        {
            var bookingList = _bookingService.GetBookingByBookCode(bookCode);

            return Json(bookingList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult BeverageLength(string bookCode)
        {
            var allBeverage = _beverageService.GetAllBeverage();

            return Json(allBeverage, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult EquipmentLength(string bookCode)
        {
            int allEquipment = _equipmentService.GetAllEquipment();

            return Json(allEquipment, JsonRequestBehavior.AllowGet);
        }
    }
}