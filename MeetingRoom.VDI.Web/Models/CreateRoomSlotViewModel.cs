﻿using Mvc.JQuery.Datatables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Models
{
    public class CreateRoomSlotViewModel
    {
        [DataTables(DisplayName = "Start Month", Sortable = false)]
        public int startMonth { get; set; }
        [DataTables(DisplayName = "End Month", Sortable = false)]
        public int endMonth { get; set; }
        [DataTables(DisplayName = "Year Month", Sortable = false)]
        public int year { get; set; }
        [DataTables(DisplayName = "Room", Sortable = false)]
        public int roomId { get; set; }
        //public bool ShowDialog { get; set; }

        //public int Id { get; set; }
        //public int roomId { get; set; }
        //public int layoutId { get; set; }
        //public int capacity { get; set; }
        //public Nullable<int> month { get; set; }
        //public Nullable<int> year { get; set; }
        //public Nullable<int> days { get; set; }
        //public Nullable<byte> hour1 { get; set; }
        //public Nullable<byte> hour2 { get; set; }
        //public Nullable<byte> hour3 { get; set; }
        //public Nullable<byte> hour4 { get; set; }
        //public Nullable<byte> hour5 { get; set; }
        //public Nullable<byte> hour6 { get; set; }
        //public Nullable<byte> hour7 { get; set; }
        //public Nullable<byte> hour8 { get; set; }
        //public Nullable<byte> hour9 { get; set; }
        //public Nullable<byte> hour10 { get; set; }
        //public Nullable<byte> hour11 { get; set; }
        //public Nullable<byte> hour12 { get; set; }
        //public Nullable<byte> hour13 { get; set; }
        //public Nullable<byte> isActive { get; set; }
    }
}