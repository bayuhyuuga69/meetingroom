﻿using Mvc.JQuery.Datatables;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Models
{
    public class RoomViewModel
    {
        public int Id { get; set; }
        [DisplayName("Capacity")]
        [DataTables(Width = "100px")]
        public int MeetingRoomCapacity { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string MeetingRoomDescription { get; set; }
        [DisplayName("Prices")]
        [DataTables(Width = "100px")]
        public float MeetingRoomPrice { get; set; }
        [DisplayName("Status")]
        [DataTables(Width = "100px")]
        public bool MeetingRoomStatus { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "100px")]
        public string MeetingRoomTitle { get; set; }
        [DisplayName("Room Image")]
        [DataTables(Width = "100px")]
        public byte[] MeetingRoomImage { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        [DisplayName("Layout")]
        [DataTables(Width = "100px")]
        public int meetingRoomLayoutId { get; set; }
        [DisplayName("Book")]
        [DataTables(Width = "100px")]
        public byte meetingRoomBookFor { get; set; }
        [DisplayName("Layout")]
        [DataTables(Width = "100px")]
        public string LayoutName { get; set; }
    }

    public class CreateRoomViewModel
    {
        public int Id { get; set; }
        [DisplayName("Capacity")]
        [DataTables(Width = "100px")]
        public int MeetingRoomCapacity { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string MeetingRoomDescription { get; set; }
        [DisplayName("Prices")]
        [DataTables(Width = "100px")]
        public float MeetingRoomPrice { get; set; }
        [DisplayName("Status")]
        [DataTables(Width = "100px")]
        public bool MeetingRoomStatus { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "100px")]
        public string MeetingRoomTitle { get; set; }
        [DisplayName("Room Image")]
        [DataTables(Width = "100px")]
        public byte[] MeetingRoomImage { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        [DisplayName("Layout")]
        [DataTables(Width = "100px")]
        public int meetingRoomLayoutId { get; set; }
        [DisplayName("Book")]
        [DataTables(Width = "100px")]
        public byte meetingRoomBookFor { get; set; }
        [DisplayName("Layout")]
        [DataTables(Width = "100px")]
        public string LayoutName { get; set; }
    }

    public class EditRoomViewModel
    {
        public int Id { get; set; }
        [DisplayName("Capacity")]
        [DataTables(Width = "100px")]
        public int MeetingRoomCapacity { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string MeetingRoomDescription { get; set; }
        [DisplayName("Prices")]
        [DataTables(Width = "100px")]
        public float MeetingRoomPrice { get; set; }
        [DisplayName("Status")]
        [DataTables(Width = "100px")]
        public bool MeetingRoomStatus { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "100px")]
        public string MeetingRoomTitle { get; set; }
        [DisplayName("Room Image")]
        [DataTables(Width = "100px")]
        public byte[] MeetingRoomImage { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }       
        public int meetingRoomLayoutId { get; set; }
        [DisplayName("Book")]
        [DataTables(Width = "100px")]
        public byte meetingRoomBookFor { get; set; }
        [DisplayName("Layout")]
        [DataTables(Width = "100px")]
        public string LayoutName { get; set; }
    }
}