﻿using Mvc.JQuery.Datatables;
using Mvc.JQuery.Datatables.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Models
{
    public class EquipmentGridViewModel
    {
        [DataTables(Order = 0, Visible = false)]
        public int Id { get; set; }

        [DataTables(DisplayName = "Title", SortDirection = SortDirection.Ascending, Searchable = true, Width = "250px")]
        public string meetingRoomEquipmentTitle { get; set; }

        [DataTables(DisplayName = "Description", Sortable = false)]
        public string meetingRoomEquipmentDescription { get; set; }

        [DataTables(DisplayName = "Price", Sortable = true, Width = "100px")]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public string meetingRoomEquipmentPrice { get; set; }
    }

    public class EquipmentViewModel
    {
        public int Id { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string meetingRoomEquipmentDescription { get; set; }
        [DisplayName("Price")]
        [DataTables(Width = "50px")]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public float meetingRoomEquipmentPrice { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "50px")]
        public string meetingRoomEquipmentTitle { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }

    public class CreateEquipmentViewModel
    {
        public int Id { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string meetingRoomEquipmentDescription { get; set; }
        [DisplayName("Price")]
        [DataTables(Width = "50px")]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public float meetingRoomEquipmentPrice { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "50px")]
        public string meetingRoomEquipmentTitle { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
    }

    public class EditEquipmentViewModel
    {
        public int Id { get; set; }
        [DisplayName("Description")]
        [DataTables(Width = "200px")]
        public string meetingRoomEquipmentDescription { get; set; }
        [DisplayName("Price")]
        [DataTables(Width = "50px")]
        public float meetingRoomEquipmentPrice { get; set; }
        [DisplayName("Title")]
        [DataTables(Width = "50px")]
        public string meetingRoomEquipmentTitle { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }

    }
}