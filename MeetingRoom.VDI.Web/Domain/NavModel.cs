﻿using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Domain
{
    public class NavModel
    {
        public IEnumerable<Navbar> navbarItems()
        {
            var menu = new List<Navbar>();
            menu.Add(new Navbar { Id = 1, nameOption = "Dashboard", controller = "Home", action = "Index", imageClass = "fa fa-dashboard fa-fw", status = true, isParent = false, parentId = 0 });
            menu.Add(new Navbar { Id = 2, nameOption = "Master", imageClass = "fa fa-th-list  fa-fw", status = true, isParent = true, parentId = 0 });
            //menu.Add(new Navbar { Id = 3, nameOption = "Room", controller = "Room", action = "Index", status = true, isParent = false, parentId = 2 });
            menu.Add(new Navbar { Id = 4, nameOption = "Layout", controller = "Layout", action = "Index", status = true, isParent = false, parentId = 2 });
            menu.Add(new Navbar { Id = 5, nameOption = "Food And Drink", controller = "Beverage", action = "Index", status = true, isParent = false, parentId = 2 });
            menu.Add(new Navbar { Id = 6, nameOption = "Equipment", controller = "Equipment", action = "Index", status = true, isParent = false, parentId = 2 });
            menu.Add(new Navbar { Id = 7, nameOption = "Room Slot", controller = "RoomSlot", action = "Index", status = true, isParent = false, parentId = 2 });

            menu.Add(new Navbar { Id = 8, nameOption = "Transaction", imageClass = "fa fa-calendar-o fa-fw", status = true, isParent = true, parentId = 0 });
            
            //menu.Add(new Navbar { Id = 9, nameOption = "Booking", controller = "Booking", action = "Index", status = true, isParent = false, parentId = 8 });
            //menu.Add(new Navbar { Id = 10, nameOption = "Payment", controller = "Payment", action = "Index", status = true, isParent = false, parentId = 7 });
            //menu.Add(new Navbar { Id = 11, nameOption = "Offline Payment", controller = "OfflinePayment", action = "Index", status = true, isParent = false, parentId = 8 });
            menu.Add(new Navbar { Id = 12, nameOption = "List Booking", controller = "BookingList", action = "Index", status = true, isParent = false, parentId = 8 });

            menu.Add(new Navbar { Id = 13, nameOption = "User Management", imageClass = "fa fa-user fa-fw", status = true, isParent = true, parentId = 0 });
            menu.Add(new Navbar { Id = 14, nameOption = "User", controller = "User", action = "Index", status = true, isParent = false, parentId = 13 });
            
            menu.Add(new Navbar { Id = 15, nameOption = "Login", controller = "Account", action = "Login", status = true, isParent = false, parentId = 13 });

            return menu.ToList();
        }
    }
}