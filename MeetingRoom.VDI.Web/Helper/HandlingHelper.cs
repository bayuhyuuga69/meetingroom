﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Helper
{
    public static class HandlingHelper
    {
        public static List<SelectListItem> GetSelectList<TEnum>(params TEnum[] ignoreList)
        {
            var enumList = new List<SelectListItem>();
            foreach (TEnum data in Enum.GetValues(typeof(TEnum)))
            {
                if (!ignoreList.Contains(data))
                {
                    enumList.Add(new SelectListItem
                    {
                        Text = GetEnumDescription((Enum.Parse(typeof(TEnum), data.ToString()))),
                        Value = ((int)Enum.Parse(typeof(TEnum), data.ToString())).ToString()
                    });
                }
            }

            return enumList;
        }

        public static List<SelectListItem> GetSelectList<TEnum>(List<TEnum> list)
        {
            var enumList = new List<SelectListItem>();
            foreach (var data in list)
            {
                enumList.Add(new SelectListItem
                {
                    Text = GetEnumDescription((Enum.Parse(typeof(TEnum), data.ToString()))),
                    Value = ((int)Enum.Parse(typeof(TEnum), data.ToString())).ToString()
                });
            }

            return enumList;
        }
        private static string GetEnumDescription<TEnum>(TEnum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    var display = ((DisplayAttribute[])field.GetCustomAttributes(typeof(DisplayAttribute), false)).FirstOrDefault();
                    if (display != null)
                    {
                        return display.Name;
                    }

                    return name;
                }
            }
            return null;
        }
    }
}