﻿using MeetingRoom.VDI.Core.Helper;
using Mvc.JQuery.Datatables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace MeetingRoom.VDI.Web.Helper
{
    public static class CustomDTHelper
    {
        public static LengthMenuVm LengthMenuVm()
        {
            return new LengthMenuVm() {
                new Tuple<string, int>("10", 10),
                new Tuple<string, int>("20", 20),
                new Tuple<string, int>("25", 25),
                new Tuple<string, int>("50", 50),
                new Tuple<string, int>("100", 100),
                new Tuple<string, int>("150", 150)
            };
        }

        public static SortOrder[] GetColumnOrder<T>(DataTablesParam dataTableParam)
        {
            var sortOrders = new List<SortOrder>();

            for (int index = 0; index < dataTableParam.iSortingCols; index++)
            {
                var column = DataTablesHelper.ColDefs<T>()[dataTableParam.iSortCol[index]];

                sortOrders.Add(new SortOrder()
                {
                    ColumnName = column.Name,
                    ColumnOrder = dataTableParam.sSortDir[index] == "asc" ? SortOrder.OrderBy.Ascending : SortOrder.OrderBy.Descending
                });

            }

            return sortOrders.ToArray();
        }
    }
}