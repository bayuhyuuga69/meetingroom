﻿using MeetingRoom.VDI.Infrastructure.DefaultServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Helper
{
    public static class SecurityHelper
    {
        //public static bool IsUserInRole(string roleName)
        //{
        //    //var userService = new UserService();

        //    //return userService.IsUserInRole(HttpContext.Current.User.Identity.GetUserName(), roleName);
        //}

        //public static bool IsUserInTeam(string teamName)
        //{
        //    //var userService = new UserService();

        //    //return userService.IsUserInTeam(HttpContext.Current.User.Identity.GetUserName(), teamName);
        //}

        public static bool IsUserLogin(string email, string password)
        {
            var userService = new UserService();

            return userService.IsUserExist(email, password);
        }
    }
}