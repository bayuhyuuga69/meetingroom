﻿using MeetingRoom.VDI.Infrastructure.DefaultServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetingRoom.VDI.Web.Helper
{
    public static class GlobalHelper
    {
        public static int GetMainPoolId()
        {
            var globalService = new GlobalService();
            return globalService.Get().MainPoolId;
        }
    }
}