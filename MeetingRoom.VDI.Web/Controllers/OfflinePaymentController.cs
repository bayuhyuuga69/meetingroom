﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class OfflinePaymentController : Controller
    {
        private readonly IOfflinePayment _offlinePaymentService;

        public OfflinePaymentController(IOfflinePayment offlinePaymentService)
        {
            _offlinePaymentService = offlinePaymentService;
        }

        // GET: OfflinePayment
        public ActionResult Index()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _offlinePaymentService.GetAll();

            return View(output);
        }

        // GET: Layout/Create
        public ActionResult Create()
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var model = new CreatePaymentViewModel();

            return View(model);
        }

        // POST: Layout/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreatePaymentViewModel model)
        {
            if (ModelState.IsValid)
            {
                byte[] rawBytes = null;
                if (Request.Files.Count > 0)
                {

                    var inputFile = Request.Files[0];
                    var fileNm = Request.Files[0].FileName;
                    HttpPostedFileBase hpf = Request.Files[0] as HttpPostedFileBase;
                    Stream fileStream = inputFile.InputStream;
                    rawBytes = new byte[fileStream.Length];
                    fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));
                }

                Mapper.CreateMap<CreatePaymentViewModel, Payment>();

                var newPayment = Mapper.Map<CreatePaymentViewModel, Payment>(model);
                newPayment.StrukImage = rawBytes;

                _offlinePaymentService.Create(newPayment);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: OfflinePayment/Edit/5
        public ActionResult Edit(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var payment = _offlinePaymentService.GetById(id);

            Mapper.CreateMap<Payment, EditPaymentViewModel>();
            var model = Mapper.Map<Payment, EditPaymentViewModel>(payment);

            if (model.StrukImage != null)
            {
                ViewBag.Base64String = "data:image/png;base64," + Convert.ToBase64String(model.StrukImage, 0, model.StrukImage.Length);
            }


            return View(model);
        }

        // POST: OfflinePayment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EditPaymentViewModel model)
        {
            if (ModelState.IsValid)
            {
                byte[] rawBytes = null;
                if (Request.Files.Count > 0)
                {
                    var inputFile = Request.Files[0];
                    var fileNm = Request.Files[0].FileName;
                    HttpPostedFileBase hpf = Request.Files[0] as HttpPostedFileBase;
                    Stream fileStream = inputFile.InputStream;
                    rawBytes = new byte[fileStream.Length];
                    fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));
                }

                Mapper.CreateMap<EditPaymentViewModel, Payment>();
                //.ForMember(s => s.Brand, o => o.Ignore());

                var editPayment = Mapper.Map<EditPaymentViewModel, Payment>(model);
                editPayment.StrukImage = rawBytes;
                //editBrand.UBy = User.Identity.Name;

                _offlinePaymentService.Update(editPayment);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult UpdatePayment(string reservedCode, HttpPostedFileBase fileUpload)
        {
            string message = "";
            if (ModelState.IsValid)
            {
                byte[] rawBytes = null;
                if (Request.Files.Count > 0)
                {
                    var inputFile = Request.Files[0];
                    var fileNm = Request.Files[0].FileName;
                    HttpPostedFileBase hpf = Request.Files[0] as HttpPostedFileBase;
                    Stream fileStream = inputFile.InputStream;
                    rawBytes = new byte[fileStream.Length];
                    fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));
                }

                var payment = _offlinePaymentService.GetByReservedCode(reservedCode);
                payment.StrukImage = rawBytes;

                message = _offlinePaymentService.Update(payment);
                //return RedirectToAction("Index");
            }
      
            return Json(message, JsonRequestBehavior.AllowGet);
            //return View();
        }
    }
}