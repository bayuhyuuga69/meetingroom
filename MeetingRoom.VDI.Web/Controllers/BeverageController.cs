﻿using AutoMapper;
using MeetingRoom.VDI.Core.Const;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
using Mvc.JQuery.Datatables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class BeverageController : Controller
    {
        private readonly IBeverage _beverageService;

        public BeverageController(IBeverage beverageService)
        {
            _beverageService = beverageService;
        }

        public ActionResult BeverageList()
        {
            return PartialView();
        }

        // GET: Beverage
        public ActionResult Index()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _beverageService.GetAll();

            return View(output);
        }

        // GET: Beverage/Details/5
        public ActionResult Details(int id)
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var Beverage = _beverageService.GetById(id);

            Mapper.CreateMap<Beverage, BeverageViewModel>();
            var model = Mapper.Map<Beverage, BeverageViewModel>(Beverage);

            return View(model);
        }

        // GET: Beverage/Create
        public ActionResult Create()
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var model = new CreateBeverageViewModel();

            return View(model);
        }

        // POST: Beverage/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateBeverageViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<CreateBeverageViewModel, Beverage>();
                //.ForMember(s => s.Brand, o => o.Ignore());

                var newBeverage = Mapper.Map<CreateBeverageViewModel, Beverage>(model);

                //newSubBrand.IBy = User.Identity.Name;
                _beverageService.Create(newBeverage);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Beverage/Edit/5
        public ActionResult Edit(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var Beverage = _beverageService.GetById(id);

            Mapper.CreateMap<Beverage, EditBeverageViewModel>();
            var model = Mapper.Map<Beverage, EditBeverageViewModel>(Beverage);

            return View(model);
        }

        // POST: Beverage/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EditBeverageViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<EditBeverageViewModel, Beverage>();
                //.ForMember(s => s.Brand, o => o.Ignore());

                var editBeverage = Mapper.Map<EditBeverageViewModel, Beverage>(model);
                //editBrand.UBy = User.Identity.Name;

                _beverageService.Update(editBeverage);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Beverage/Delete/5
        public ActionResult Delete(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var Beverage = _beverageService.GetById(id);

            Mapper.CreateMap<Beverage, EditBeverageViewModel>();
            var model = Mapper.Map<Beverage, EditBeverageViewModel>(Beverage);

            return View(model);
        }

        // POST: Beverage/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, EditBeverageViewModel model)
        {
            try
            {
                Mapper.CreateMap<EditBeverageViewModel, Beverage>();
                var deleteBeverage = Mapper.Map<EditBeverageViewModel, Beverage>(model);

                _beverageService.Delete(deleteBeverage.Id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public DataTablesResult<BeverageViewModel> GetIndex(DataTablesParam dataTableParam, string filterData = "")
        {
            return GetAll<BeverageViewModel>(dataTableParam, filterData);
        }

        public DataTablesResult<T> GetAll<T>(DataTablesParam dataTableParam, string filterData = "")
        {
            if (dataTableParam.iDisplayLength == 0)
                dataTableParam.iDisplayLength = GlobalSetting.DEFAULT_PAGESIZE;

            var pageIndex = Convert.ToInt32(Math.Ceiling((double)dataTableParam.iDisplayStart / dataTableParam.iDisplayLength) + 1);

            List<Beverage> filter = null;
            if (!String.IsNullOrEmpty(filterData))
                filter = JsonConvert.DeserializeObject<List<Beverage>>(filterData);

            Beverage search = null;
            if (!String.IsNullOrEmpty(dataTableParam.sSearch))
            {
                search = new Beverage();
                search.meetingRoomFoodDrinkTitle = dataTableParam.sSearch;
            }

            var pagedData = _beverageService.GetAll(filter, search, pageIndex, dataTableParam.iDisplayLength);

            Mapper.CreateMap<Beverage, T>();
            var data = Mapper.Map<IEnumerable<Beverage>, IEnumerable<T>>(pagedData.Data);

            dataTableParam.iDisplayStart = 0;
            dataTableParam.sSearch = "";
            var result = DataTablesResult.Create(data.AsQueryable(), dataTableParam);

            result.Data.iTotalRecords = pagedData.TotalRecord;
            result.Data.iTotalDisplayRecords = pagedData.TotalRecord;

            return result;
        }

        public JsonResult GetBeverage()
        {
            var foodList = _beverageService.GetBeverage();
            return Json(foodList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllBeverage()
        {
            var foodList = _beverageService.GetAll();
            return Json(foodList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPriceFood(int Id, int Qty)
        {
            var foodList = _beverageService.GetPriceFood(Id, Qty);
            return Json(foodList, JsonRequestBehavior.AllowGet);
        }
    }
}