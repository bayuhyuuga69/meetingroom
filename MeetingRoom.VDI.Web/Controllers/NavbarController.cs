﻿using MeetingRoom.VDI.Web.Domain;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class NavbarController : Controller
    {
        // GET: Navbar
        public ActionResult Index()
        {
            var data = new NavModel();

            return PartialView("_Navbar", data.navbarItems().ToList());
        }
    }
}