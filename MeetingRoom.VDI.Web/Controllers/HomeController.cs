﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var usr = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            this.Session["UserProfile"] = "";
            this.Session["UserPass"] = "";
            this.Session["IsLogin"] = "";
            this.Session["IsAdmin"] = "";

            if ((usr != null) && (usr != ""))
            {
                //var strProfile = usr;
                //var strPass = strPass1;
                //var IsLogin = IsLogin1;
                //var IsAdmin = IsAdmin1;

                this.Session["UserProfile"] = usr;
                this.Session["UserPass"] = strPass;
                this.Session["IsLogin"] = IsLogin;
                this.Session["IsAdmin"] = IsAdmin;
            }
            else
            {
                this.Session["UserProfile"] = "";
                this.Session["UserPass"] = "";
                this.Session["IsLogin"] = "";
                this.Session["IsAdmin"] = "";
                return RedirectToAction("Login", "Account");
            }

            return View();
        }

        public ActionResult FlotCharts()
        {
            return View("FlotCharts");
        }

        public ActionResult MorrisCharts()
        {
            return View("MorrisCharts");
        }

        public ActionResult Tables()
        {
            return View("Tables");
        }

        public ActionResult Forms()
        {
            return View("Forms");
        }

        public ActionResult Panels()
        {
            return View("Panels");
        }

        public ActionResult Buttons()
        {
            return View("Buttons");
        }

        public ActionResult Notifications()
        {
            return View("Notifications");
        }

        public ActionResult Typography()
        {
            return View("Typography");
        }

        public ActionResult Icons()
        {
            return View("Icons");
        }

        public ActionResult Grid()
        {
            return View("Grid");
        }

        public ActionResult Blank()
        {
            return View("Blank");
        }

        public ActionResult Login()
        {
            return View("Login");
        }

    }
}