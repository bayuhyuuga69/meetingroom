﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Infrastructure.DefaultServices;
using MeetingRoom.VDI.Infrastructure.Helper;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingUser.VDI.Web.Controllers
{
    public class UserController : Controller
    {
        private IUser _userService;

        public IUser UserService
        {
            get
            {
                return _userService ?? new UserService();
            }
            private set
            {
                _userService = value;
            }
        }

        public UserController(IUser userService, ILayout LayoutService)
        {
            _userService = userService;
        }

        // GET: User
        public ActionResult Index()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _userService.GetAll();

            return View(output);
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var User = _userService.GetUserById(id);

            Mapper.CreateMap<User, UserViewModel>();
            var model = Mapper.Map<User, UserViewModel>(User);

            return View(model);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var model = new CreateUserViewModel();

            return View(model);
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<CreateUserViewModel, User>();

                var newUser = Mapper.Map<CreateUserViewModel, User>(model);
                if (!UserHelper.IsValidEmail(newUser.Email))
                {
                    return Content("Invalid Email Format");
                }
                else
                {
                    _userService.Create(newUser);
                }

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var User = _userService.GetUserById(id);

            Mapper.CreateMap<User, EditUserViewModel>();
            var model = Mapper.Map<User, EditUserViewModel>(User);

            return View(model);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<EditUserViewModel, User>();
                //.ForMember(s => s.Brand, o => o.Ignore());

                var editUser = Mapper.Map<EditUserViewModel, User>(model);

                _userService.Update(editUser);

                return RedirectToAction("Index");
            }

            return View(model);
        }
    }
}