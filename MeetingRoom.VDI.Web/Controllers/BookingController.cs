﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class BookingController : Controller
    {
        private readonly IBooking _bookingService;
        private readonly IBeverageDetail _bookingBeverageService;
        private readonly IEquipmentDetail _bookingEquipmentService;
        private static List<CreateBookingBeverageDetail> _beverageDetail;
        private static List<CreateBookingEquipmentDetail> _equipmentDetail;

        private static int bookingId = 0;

        public BookingController(IBooking bookingService, IBeverageDetail bookingBeverageService, IEquipmentDetail bookingEquipmentService)
        {
            _bookingService = bookingService;
            _bookingBeverageService = bookingBeverageService;
            _bookingEquipmentService = bookingEquipmentService;
        }

        public ActionResult BookingList()
        {
            return PartialView();
        }

        // GET: Booking
        public ActionResult Index()
        {
            if (this.Session["UserProfile"] != null)
            {
                var strProfile = this.Session["UserProfile"];
                var strPass = this.Session["UserPass"];
                var IsLogin = this.Session["IsLogin"];
                var IsAdmin = this.Session["IsAdmin"];
            }

            var model = new CreateBookingViewModel();

            return View(model);

            //return View(output);
        }

        // GET: Booking/Details/5
        public ActionResult Details(int id)
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var Booking = _bookingService.GetById(id);

            Mapper.CreateMap<Booking, BookingViewModel>();
            var model = Mapper.Map<Booking, BookingViewModel>(Booking);

            return View(model);
        }

        // GET: Booking/Create
        public ActionResult Create()
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var model = new CreateBookingViewModel();

            return View(model);
        }

        public JsonResult SaveEquipment()
        {

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveFood()
        {

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        // POST: Booking/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveData(string Name,
                            string Phone,
                            string Email,
                            string Company,
                            string BookingCode,
                            string BookingDate,
                            string SpecialRequest,
                            string StartTime,
                            string FinishTime,
                            string MeetingRoomType,
                            string NumberOfPeople,
                            string Facilities,
                            string LayoutId,
                            string PaymentMethod,
                            string RoomPrice,
                            string EquipmentPrice,
                            string FoodAndDrinkPrice,
                            string SubTotal,
                            string Tax,
                            string Total,
                            string Deposit)
        {
            var a = _equipmentDetail;
            // int bookingId = 0;
            if (ModelState.IsValid)
            {
                var bookingData = new CreateBookingViewModel();

                if (EquipmentPrice == "")
                {
                    EquipmentPrice = "0";
                }

                if (FoodAndDrinkPrice == "")
                {
                    FoodAndDrinkPrice = "0";
                }

                if (SubTotal == "")
                {
                    SubTotal = "0";
                }

                if (Tax == "")
                {
                    Tax = "0";
                }

                if (Total == "")
                {
                    Total = "0";
                }

                bookingData.Name = Name;
                bookingData.Phone = Phone;
                bookingData.Email = Email;
                bookingData.Company = Company;
                bookingData.BookingCode = BookingCode;
                bookingData.BookingDate = Convert.ToDateTime(BookingDate);
                bookingData.SpecialRequest = SpecialRequest;
                bookingData.StartTime = TimeSpan.Parse(StartTime);
                bookingData.FinishTime = TimeSpan.Parse(FinishTime);
                bookingData.MeetingRoomType = Convert.ToInt32(MeetingRoomType);
                bookingData.NumberOfPeople = Convert.ToInt32(NumberOfPeople);
                bookingData.Facilities = Convert.ToInt32(Facilities);
                bookingData.LayoutId = Convert.ToInt32(LayoutId);
                bookingData.PaymentMethod = Convert.ToInt32(PaymentMethod);
                bookingData.RoomPrice = Convert.ToDecimal(RoomPrice);
                bookingData.EquipmentPrice = Convert.ToDecimal(EquipmentPrice);
                bookingData.FoodAndDrinkPrice = Convert.ToDecimal(FoodAndDrinkPrice) > 0 ? Convert.ToDecimal(FoodAndDrinkPrice) : 0;
                bookingData.SubTotal = Convert.ToDecimal(SubTotal) > 0 ? Convert.ToDecimal(SubTotal) : 0;
                bookingData.Tax = Convert.ToDecimal(Tax) > 0 ? Convert.ToDecimal(Tax) : 0;
                bookingData.Total = Convert.ToDecimal(Total) > 0 ? Convert.ToDecimal(Total) : 0;
                bookingData.Deposit = Convert.ToDecimal(Deposit) > 0 ? Convert.ToDecimal(Total) : 0;

                Mapper.CreateMap<CreateBookingViewModel, Booking>();
                //.ForMember(s => s.Brand, o => o.Ignore());

                var newBooking = Mapper.Map<CreateBookingViewModel, Booking>(bookingData);

                //newSubBrand.IBy = User.Identity.Name;
                bookingId = 1;// _bookingService.Create(newBooking).Id;

                if (bookingId == 0)
                {
                    return Json("Failed", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }

            }

            return Json(bookingId, JsonRequestBehavior.AllowGet);
            //return View(model);
        }

        public JsonResult SaveDataBeverageDetail(
                                               List<CreateBookingBeverageDetail> foodData
                                                )
        {
            if (ModelState.IsValid)
            {
                var bookingData = new CreateBookingBeverageDetail();

                foreach (var item in foodData)
                {
                    bookingData.BookingId = bookingId;
                    bookingData.BeverageId = item.Id;
                    bookingData.Qty = Convert.ToInt32(item.Qty);
                    bookingData.Price = Convert.ToInt32(item.Price);

                    Mapper.CreateMap<CreateBookingBeverageDetail, BeverageDetail>();
                    var newBooking = Mapper.Map<CreateBookingBeverageDetail, BeverageDetail>(bookingData);

                    _bookingBeverageService.Create(newBooking);
                }

                //return Json("Success", JsonRequestBehavior.AllowGet);
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
            //return View(model);
        }

        public JsonResult SaveDataEquipmentDetail(
                                                List<CreateBookingEquipmentDetail> equipmentData
                                                )
        {
            if (ModelState.IsValid)
            {
                var bookingData = new CreateBookingEquipmentDetail();

                foreach (var item in equipmentData)
                {
                    bookingData.BookingId = bookingId;
                    bookingData.EquipmentId = item.Id;
                    bookingData.Qty = Convert.ToInt32(item.Qty);
                    bookingData.Price = Convert.ToInt32(item.Price);

                    Mapper.CreateMap<CreateBookingEquipmentDetail, EquipmentDetail>();
                    var newBooking = Mapper.Map<CreateBookingEquipmentDetail, EquipmentDetail>(bookingData);

                    _bookingEquipmentService.Create(newBooking);
                }
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetBookingByBookCode(string bookCode)
        {
            var bookingList = _bookingService.GetBookingByBookCode(bookCode);

            return Json(bookingList, JsonRequestBehavior.AllowGet);
        }
    }
}