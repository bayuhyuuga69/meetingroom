﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Helper;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class RoomController : Controller
    {

        private readonly IRoom _RoomService;
        private readonly ILayout _LayoutService;

        public RoomController(IRoom RoomService, ILayout LayoutService)
        {
            _RoomService = RoomService;
            _LayoutService = LayoutService;
        }

        public ActionResult RoomList()
        {
            return PartialView();
        }

        public FileContentResult GetFile(int id)
        {
            Room mediaImage = _RoomService.GetById(id);
            return new FileContentResult(mediaImage.MeetingRoomImage, "image/*");
        }

        // GET: Room
        public ActionResult Index()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _RoomService.GetAll();            

            return View(output);
        }

        // GET: Room/Details/5
        public ActionResult Details(int id)
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var Room = _RoomService.GetById(id);

            Mapper.CreateMap<Room, RoomViewModel>();
            var model = Mapper.Map<Room, RoomViewModel>(Room);

            ViewBag.Base64String = "data:image/png;base64," + Convert.ToBase64String(model.MeetingRoomImage, 0, model.MeetingRoomImage.Length);

            return View(model);
        }

        // GET: Room/Create
        public ActionResult Create()
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var model = new CreateRoomViewModel();

            return View(model);
        }

        // POST: Room/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateRoomViewModel model)
        {
            if (ModelState.IsValid)
            {
                byte[] rawBytes = null;
                if (Request.Files.Count > 0)
                {
                    var inputFile = Request.Files[0];
                    var fileNm = Request.Files[0].FileName;
                    HttpPostedFileBase hpf = Request.Files[0] as HttpPostedFileBase;
                    Stream fileStream = inputFile.InputStream;
                    rawBytes = new byte[fileStream.Length];
                    fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));
                    //string filePath = Path.Combine(HttpContext.Server.MapPath("~//App_Data//Layout//"));
                    //hpf.SaveAs(filePath + fileNm);

                    //var file = Request.Files[0];
                    //Stream fileStream = file.InputStream;
                    //rawBytes = new byte[fileStream.Length];
                    //fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));
                }

                //foreach (string filesData in Request.Files)
                //{
                //    var fileNm = Request.Files[filesData].FileName;
                //    HttpPostedFileBase hpf = Request.Files[filesData] as HttpPostedFileBase;
                //    string filePath = Path.Combine(HttpContext.Server.MapPath("~//Your Folder Path//"));
                //    hpf.SaveAs(filePath + fileNm);
                //}

                Mapper.CreateMap<CreateRoomViewModel, Room>();
                
                var newRoom = Mapper.Map<CreateRoomViewModel, Room>(model);
                newRoom.CreationTime = System.DateTime.Now;
                newRoom.MeetingRoomImage = rawBytes;
                newRoom.MeetingRoomFilePath = "";

                //newSubBrand.IBy = User.Identity.Name;
                _RoomService.Create(newRoom);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Room/Edit/5
        public ActionResult Edit(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var Room = _RoomService.GetById(id);

            Mapper.CreateMap<Room, EditRoomViewModel>();
            var model = Mapper.Map<Room, EditRoomViewModel>(Room);

            ViewBag.Base64String = "data:image/png;base64," + Convert.ToBase64String(model.MeetingRoomImage, 0, model.MeetingRoomImage.Length);

            return View(model);
        }

        // POST: Room/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EditRoomViewModel model)
        {
            if (ModelState.IsValid)
            {
                byte[] rawBytes = null;
                if (Request.Files.Count > 0)
                {
                    var inputFile = Request.Files[0];
                    var fileNm = Request.Files[0].FileName;
                    HttpPostedFileBase hpf = Request.Files[0] as HttpPostedFileBase;
                    Stream fileStream = inputFile.InputStream;
                    rawBytes = new byte[fileStream.Length];
                    fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));
                    //string filePath = Path.Combine(HttpContext.Server.MapPath("~//App_Data//Layout//"));
                    //hpf.SaveAs(filePath + fileNm);

                    //var file = Request.Files[0];
                    //Stream fileStream = file.InputStream;
                    //rawBytes = new byte[fileStream.Length];
                    //fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));
                }

                //foreach (string filesData in Request.Files)
                //{
                //    var fileNm = Request.Files[filesData].FileName;
                //    HttpPostedFileBase hpf = Request.Files[filesData] as HttpPostedFileBase;
                //    string filePath = Path.Combine(HttpContext.Server.MapPath("~//Your Folder Path//"));
                //    hpf.SaveAs(filePath + fileNm);
                //}

                Mapper.CreateMap<EditRoomViewModel, Room>();
                //.ForMember(s => s.Brand, o => o.Ignore());

                var editRoom = Mapper.Map<EditRoomViewModel, Room>(model);
                editRoom.MeetingRoomImage = rawBytes;
                editRoom.MeetingRoomFilePath = "filePath";

                _RoomService.Update(editRoom);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Room/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Room/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public JsonResult GetRoom(int id)
        {
            var roomList = _RoomService.GetRoom(id);
            return Json(roomList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllRoom()
        {
            var roomList = _RoomService.GetAllRoom();
            return Json(roomList, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetRoomById(int id)
        {
            var roomList = _RoomService.GetLayoutImageByRoomId(id);
            return Json(roomList, JsonRequestBehavior.AllowGet);
        }
    }
}