﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class LayoutController : Controller
    {
        private readonly ILayout _LayoutService;

        public LayoutController(ILayout LayoutService)
        {
            _LayoutService = LayoutService;
        }
        public ActionResult LayoutList()
        {
            var output = _LayoutService.GetAll();
            return PartialView(output);
        }

        public FileContentResult GetFile(int id)
        {
            Layout mediaImage = _LayoutService.GetById(id);
            return new FileContentResult(mediaImage.meetingRoomLayoutImages, "image/*");
        }

        // GET: Layout
        public ActionResult Index()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _LayoutService.GetAll();

            return View(output);
        }

        // GET: Layout/Details/5
        public ActionResult Details(int id)
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var Layout = _LayoutService.GetById(id);

            Mapper.CreateMap<Layout, LayoutViewModel>();
            var model = Mapper.Map<Layout, LayoutViewModel>(Layout);

            if (model.meetingRoomLayoutImages != null)
            {
                ViewBag.Base64String = "data:image/png;base64," + Convert.ToBase64String(model.meetingRoomLayoutImages, 0, model.meetingRoomLayoutImages.Length);
            }

            return View(model);
        }

        // GET: Layout/Create
        public ActionResult Create()
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var model = new CreateLayoutViewModel();

            return View(model);
        }

        // POST: Layout/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateLayoutViewModel model)
        {
            if (ModelState.IsValid)
            {
                byte[] rawBytes = null;
                if (Request.Files.Count > 0)
                {
                    //var inputFile = Request.Files[0];
                    //Stream fileStream = inputFile.InputStream;
                    //rawBytes = new byte[fileStream.Length];
                    //fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));

                    var inputFile = Request.Files[0];
                    var fileNm = Request.Files[0].FileName;
                    HttpPostedFileBase hpf = Request.Files[0] as HttpPostedFileBase;
                    Stream fileStream = inputFile.InputStream;
                    rawBytes = new byte[fileStream.Length];
                    fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));
                    //string filePath = Path.Combine(HttpContext.Server.MapPath("~//App_Data//Layout//"));
                    //hpf.SaveAs(filePath + fileNm);
                }

                //foreach (string filesData in Request.Files)
                //{
                //    var fileNm = Request.Files[filesData].FileName;
                //    HttpPostedFileBase hpf = Request.Files[filesData] as HttpPostedFileBase;
                //    string filePath = Path.Combine(HttpContext.Server.MapPath("~//Your Folder Path//"));
                //    hpf.SaveAs(filePath + fileNm);
                //}

                Mapper.CreateMap<CreateLayoutViewModel, Layout>();

                var newLayout = Mapper.Map<CreateLayoutViewModel, Layout>(model);
                newLayout.meetingRoomLayoutImages = rawBytes;
                newLayout.meetingRoomLayoutFilePath = "";

                _LayoutService.Create(newLayout);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Layout/Edit/5
        public ActionResult Edit(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var Layout = _LayoutService.GetById(id);

            Mapper.CreateMap<Layout, EditLayoutViewModel>();
            var model = Mapper.Map<Layout, EditLayoutViewModel>(Layout);

            //ViewBag.Base64String = "data:image/png;base64," + Convert.ToBase64String(model.meetingRoomLayoutImages, 0, model.meetingRoomLayoutImages.Length);

            if (model.meetingRoomLayoutImages != null)
            {
                ViewBag.Base64String = "data:image/png;base64," + Convert.ToBase64String(model.meetingRoomLayoutImages, 0, model.meetingRoomLayoutImages.Length);
            }

            return View(model);
        }

        // POST: Layout/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EditLayoutViewModel model)
        {
            if (ModelState.IsValid)
            {
                byte[] rawBytes = null;
                if (Request.Files.Count > 0)
                {
                    var inputFile = Request.Files[0];
                    var fileNm = Request.Files[0].FileName;
                    HttpPostedFileBase hpf = Request.Files[0] as HttpPostedFileBase;
                    Stream fileStream = inputFile.InputStream;
                    rawBytes = new byte[fileStream.Length];
                    fileStream.Read(rawBytes, 0, Convert.ToInt32(fileStream.Length));
                    //string filePath = Path.Combine(HttpContext.Server.MapPath("~//App_Data//Layout//"));
                    //hpf.SaveAs(filePath + fileNm);
                }

                //foreach (string filesData in Request.Files)
                //{
                //    var fileNm = Request.Files[filesData].FileName;
                //    HttpPostedFileBase hpf = Request.Files[filesData] as HttpPostedFileBase;
                //    string filePath = Path.Combine(HttpContext.Server.MapPath("~//Your Folder Path//"));
                //    hpf.SaveAs(filePath + fileNm);
                //}

                Mapper.CreateMap<EditLayoutViewModel, Layout>();

                var editLayout = Mapper.Map<EditLayoutViewModel, Layout>(model);
                editLayout.meetingRoomLayoutImages = rawBytes;
                editLayout.meetingRoomLayoutFilePath = "FilePath";

                _LayoutService.Update(editLayout);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Layout/Delete/5
        public ActionResult Delete(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var Layout = _LayoutService.GetById(id);

            Mapper.CreateMap<Layout, EditLayoutViewModel>();
            var model = Mapper.Map<Layout, EditLayoutViewModel>(Layout);

            //ViewBag.Base64String = "data:image/png;base64," + Convert.ToBase64String(model.meetingRoomLayoutImages, 0, model.meetingRoomLayoutImages.Length);

            if (model.meetingRoomLayoutImages != null)
            {
                ViewBag.Base64String = "data:image/png;base64," + Convert.ToBase64String(model.meetingRoomLayoutImages, 0, model.meetingRoomLayoutImages.Length);
            }

            return View(model);
        }

        // POST: Layout/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, EditLayoutViewModel model)
        {
            try
            {
                // TODO: Add delete logic here
                Mapper.CreateMap<EditLayoutViewModel, Layout>();

                var deleteLayout = Mapper.Map<EditLayoutViewModel, Layout>(model);

                _LayoutService.Delete(deleteLayout.Id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public JsonResult GetLayoutCapacity(int id)
        {
            var layoutList = _LayoutService.GetCapacity(id);
            return Json(layoutList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetLayout()
        {
            var layoutList = _LayoutService.GetLayoutImage();
            return Json(layoutList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetLayoutById(int id)
        {
            var layoutList = _LayoutService.GetLayoutImageById(id);
            return Json(layoutList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllLayout()
        {
            var layoutList = _LayoutService.GetLayout();
            return Json(layoutList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetPriceLayout(int Id)
        {
            var layoutList = _LayoutService.GetPriceLayout(Id);
            return Json(layoutList, JsonRequestBehavior.AllowGet);
        }
    }
}