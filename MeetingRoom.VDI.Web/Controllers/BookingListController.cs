﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Core.Models;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class BookingListController : Controller
    {
        private readonly IBooking _bookingService;
        private readonly IBeverageDetail _bookingBeverageService;
        private readonly IEquipmentDetail _bookingEquipmentService;
        private readonly IOfflinePayment _paymentService;
        private readonly ILayout _layoutService;

        private static string strMessage = "";

        public BookingListController(IBooking bookingService, IBeverageDetail bookingBeverageService, IEquipmentDetail bookingEquipmentService, 
            IOfflinePayment paymentService, ILayout layoutService)
        {
            _bookingService = bookingService;
            _bookingBeverageService = bookingBeverageService;
            _bookingEquipmentService = bookingEquipmentService;
            _paymentService = paymentService;
            _layoutService = layoutService;

            Mapper.CreateMap<Booking, BookingViewModel>();
            Mapper.CreateMap<BookingViewModel, Booking>();
        }

        // GET: BookingList
        public ActionResult Index()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _bookingService.GetAll();

            Mapper.CreateMap<IEnumerable<BookingViewModel>, IEnumerable<Booking>>();
            var model = Mapper.Map<IEnumerable<Booking>, IEnumerable<BookingViewModel>>(output);

            return View(model);
        }

        public ActionResult IndexData(int submit)
        {
            var output = _bookingService.GetSubmitted(submit);

            Mapper.CreateMap<IEnumerable<Booking>, IEnumerable<BookingViewModel>>();
            var model = Mapper.Map<IEnumerable<Booking>, IEnumerable<BookingViewModel>>(output);

            return PartialView("IndexData", model);
        }

        public ActionResult ViewDetail(int id)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var bookingView = _bookingService.GetById(id);
            var paymentView = _paymentService.GetByResCode(bookingView.ReservedCode);
            var layoutView = _layoutService.GetById(bookingView.LayoutId.Value);

            Mapper.CreateMap<Booking, EditBookingViewModel>();
            var model = Mapper.Map<Booking, EditBookingViewModel>(bookingView);
           
            ViewBag.bookingId = bookingView.Id;
            ViewBag.IsUpload = 0;
            ViewBag.IsPaid = 0;

            if (paymentView != null)
            {
                if (paymentView.StrukImage != null)
                {
                    ViewBag.Base64String = "data:image/png;base64," + Convert.ToBase64String(paymentView.StrukImage, 0, paymentView.StrukImage.Length);
                    ViewBag.IsUpload = 1;
                }

                int isPaid = paymentView.Paid ? 1 : 0;
                ViewBag.IsPaid = isPaid;
            }

            if (layoutView.meetingRoomLayoutImages != null)
            {
                ViewBag.Layout64String = "data:image/png;base64," + Convert.ToBase64String(layoutView.meetingRoomLayoutImages, 0, layoutView.meetingRoomLayoutImages.Length);
                @ViewBag.LabelImage = layoutView.meetingRoomLayoutTitle;
            }

            return View(model);
        }

        public JsonResult GetBeverageById(int Id)
        {
            var beverageList = _bookingBeverageService.GetBeverageDetailsById(Id);
            return Json(beverageList, JsonRequestBehavior.AllowGet);
        }
       
        public JsonResult GetEquipmentById(int Id)
        {
            var equipmentList = _bookingEquipmentService.GetEquipmentDetailsById(Id);
            return Json(equipmentList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateData(string Id,
                         string Name,
                         string Phone,
                         string Email,
                         string Company,
                         string BookingCode,
                         string BookingDate,
                         string SpecialRequest,
                         string StartTime,
                         string FinishTime,
                         string MeetingRoomType,
                         string NumberOfPeople,
                         string Facilities,
                         string LayoutId,
                         string PaymentMethod,
                         string RoomPrice,
                         string EquipmentPrice,
                         string FoodAndDrinkPrice,
                         string SubTotal,
                         string Tax,
                         string Total,
                         string Deposit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var bookingData = new EditBookingViewModel();

                    if (EquipmentPrice == "")
                    {
                        EquipmentPrice = "0";
                    }

                    if (FoodAndDrinkPrice == "")
                    {
                        FoodAndDrinkPrice = "0";
                    }

                    if (SubTotal == "")
                    {
                        SubTotal = "0";
                    }

                    if (Tax == "")
                    {
                        Tax = "0";
                    }

                    if (Total == "")
                    {
                        Total = "0";
                    }

                    bookingData.Id = Convert.ToInt32(Id);
                    bookingData.Name = Name;
                    bookingData.Phone = Phone;
                    bookingData.Email = Email;
                    bookingData.Company = Company;
                    bookingData.BookingCode = BookingCode;
                    bookingData.BookingDate = Convert.ToDateTime(BookingDate);
                    bookingData.SpecialRequest = SpecialRequest;
                    bookingData.StartTime = TimeSpan.Parse(StartTime);
                    bookingData.FinishTime = TimeSpan.Parse(FinishTime);
                    bookingData.MeetingRoomType = Convert.ToInt32(MeetingRoomType);
                    bookingData.NumberOfPeople = Convert.ToInt32(NumberOfPeople);
                    bookingData.Facilities = Convert.ToInt32(Facilities);
                    bookingData.LayoutId = Convert.ToInt32(LayoutId);
                    bookingData.PaymentMethod = Convert.ToInt32(PaymentMethod);
                    bookingData.RoomPrice = Convert.ToDecimal(RoomPrice);
                    bookingData.EquipmentPrice = Convert.ToDecimal(EquipmentPrice);
                    bookingData.FoodAndDrinkPrice = Convert.ToDecimal(FoodAndDrinkPrice) > 0 ? Convert.ToDecimal(FoodAndDrinkPrice) : 0;
                    bookingData.SubTotal = Convert.ToDecimal(SubTotal) > 0 ? Convert.ToDecimal(SubTotal) : 0;
                    bookingData.Tax = Convert.ToDecimal(Tax) > 0 ? Convert.ToDecimal(Tax) : 0;
                    bookingData.Total = Convert.ToDecimal(Total) > 0 ? Convert.ToDecimal(Total) : 0;
                    bookingData.Deposit = Convert.ToDecimal(Deposit) > 0 ? Convert.ToDecimal(Total) : 0;

                    Mapper.CreateMap<EditBookingViewModel, Booking>();

                    var newBooking = Mapper.Map<EditBookingViewModel, Booking>(bookingData);
                    SmtpServer smtpServer = new SmtpServer();

                    _bookingService.UpdateData(newBooking);
                    var bookingCode = _bookingService.GetById(bookingData.Id);

                    string messageBody = System.IO.File.ReadAllText(HostingEnvironment.MapPath(@"~/MailTemplate/Official_Receipt_OR.html"));
                    string messageBodyAttachment = System.IO.File.ReadAllText(HostingEnvironment.MapPath(@"~/MailTemplate/JIC_payment.html"));

                    Booking booking = _bookingService.GetById(bookingData.Id);
                    Layout layoutModel = _layoutService.GetById(booking.LayoutId.Value);

                    if (booking.EquipmentPrice == 0.0000M)
                    {
                        booking.EquipmentPrice = 0;
                    }

                    if (booking.FoodAndDrinkPrice == 0.0000M)
                    {
                        booking.FoodAndDrinkPrice = 0;
                    }

                    DateTime dt = DateTime.Now;

                    TimeSpan timeAdd = TimeSpan.FromHours(3);
                    TimeSpan time = DateTime.Now.TimeOfDay;
                    time = time.Add(timeAdd);

                    messageBody = messageBody.Replace("@NamaCustomer", Name);
                    string filePath = Path.Combine(HttpContext.Server.MapPath("~//MailTemplate//")) + bookingCode.BookingCode + ".pdf";

                    messageBodyAttachment = messageBodyAttachment.Replace("@NamaCustomer", Name);
                    messageBodyAttachment = messageBodyAttachment.Replace("@ReservationCode", booking.ReservedCode);
                    messageBodyAttachment = messageBodyAttachment.Replace("@NamaRuangan", layoutModel.meetingRoomLayoutTitle);
                    messageBodyAttachment = messageBodyAttachment.Replace("@Email", booking.Email);
                    messageBodyAttachment = messageBodyAttachment.Replace("@EmailCustomer", booking.Email);
                    messageBodyAttachment = messageBodyAttachment.Replace("@JamMulai", booking.BookingDate.Value.ToString("dd/MM/yyyy") + " - " + booking.StartTime.ToString());
                    messageBodyAttachment = messageBodyAttachment.Replace("@JamSelesai", booking.BookingDate.Value.ToString("dd/MM/yyyy") + " - " + booking.FinishTime.ToString());
                    messageBodyAttachment = messageBodyAttachment.Replace("@WaktuMulai", booking.BookingDate.Value.ToString("dd/MM/yyyy") + " - " + booking.StartTime.ToString());
                    messageBodyAttachment = messageBodyAttachment.Replace("@WaktuSelesai", booking.BookingDate.Value.ToString("dd/MM/yyyy") + " - " + booking.FinishTime.ToString());
                    messageBodyAttachment = messageBodyAttachment.Replace("@HargaRuangan", booking.RoomPrice.Value.ToString("#,###"));
                    messageBodyAttachment = messageBodyAttachment.Replace("@HargaPerlengkapan", booking.EquipmentPrice == 0 ? "0.0000" : booking.EquipmentPrice.Value.ToString("#,###"));
                    messageBodyAttachment = messageBodyAttachment.Replace("@HargaKonsumsi", booking.FoodAndDrinkPrice == 0 ? "0.0000" : booking.FoodAndDrinkPrice.Value.ToString("#,###"));
                    messageBodyAttachment = messageBodyAttachment.Replace("@HargaBruto", booking.SubTotal.Value.ToString("#,###"));
                    messageBodyAttachment = messageBodyAttachment.Replace("@Pajak", booking.Tax.Value.ToString("#,###"));
                    messageBodyAttachment = messageBodyAttachment.Replace("@HargaNet", booking.Total.Value.ToString("#,###"));
                    messageBodyAttachment = messageBodyAttachment.Replace("@dateTime", DateTime.Now.ToString("dd/MMMM/yyyy"));

                    _bookingService.ConvertHTMLToPDF(messageBodyAttachment, filePath);
                    _bookingService.SendMailOR(newBooking.Email, newBooking.Id.ToString(), messageBody, smtpServer, filePath);

                    return Json("Sukses", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exception)
            {

            }

            return Json(strMessage, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteBeverageById(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _bookingBeverageService.DeleteBeverageById(id);
                //return RedirectToAction("Index");
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                //return View();
                return Json("Gagal", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteEquipmentById(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _bookingEquipmentService.DeleteEquipmentById(id);
                //return RedirectToAction("Index");
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                //return View();
                return Json("Gagal", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveEquipmentById(int id, int qty)
        {
            try
            {
                // TODO: Add delete logic here
                _bookingEquipmentService.Update(id, qty);
                //return RedirectToAction("Index");
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                //return View();
                return Json("Gagal", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveBeverageById(int id, int qty)
        {
            try
            {
                // TODO: Add delete logic here                
                _bookingBeverageService.Update(id, qty);
                //return RedirectToAction("Index");
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                //return View();
                return Json("Gagal", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetBeverageDetailTotalById(int id)
        {
            var prices = _bookingBeverageService.GetBeverageDetailTotalById(id);

            return Json(prices, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetEquipmentDetailTotalById(int id)
        {
            var prices = _bookingEquipmentService.GetEquipmentDetailTotalById(id);

            return Json(prices, JsonRequestBehavior.AllowGet);
        }        
    }
}