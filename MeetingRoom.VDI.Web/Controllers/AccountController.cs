﻿using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUser _userService;

        public AccountController(IUser userService)
        {
            _userService = userService;
        }

        // GET: Account/Login
        public ActionResult Login(LoginViewModel model)
        {
            //var model = new LoginViewModel();
            if (model.email != null)
            {
                bool IsExist = _userService.IsUserExist(model.email, model.password);
                if (!IsExist)
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
                else
                {
                    this.Session["UserProfile"] = model.email;
                    this.Session["UserPass"] = model.password;
                    this.Session["IsLogin"] = 1;
                    this.Session["IsAdmin"] = 1;

                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                model = new LoginViewModel();
            }

            return View(model);
        }

        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult Logout()
        {
            this.Session["UserProfile"] = "";
            this.Session["UserPass"] = "";
            this.Session["IsLogin"] = 0;
            this.Session["IsAdmin"] = 0;

            return RedirectToAction("Login", "Account");
        }
    }
}