﻿using AutoMapper;
using MeetingRoom.VDI.Core.Interfaces;
using MeetingRoom.VDI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetingRoom.VDI.Web.Controllers
{
    public class RoomSlotController : Controller
    {
        private readonly IRoomSlot _roomSlotService;
        private readonly ILayout _layoutService;

        public RoomSlotController(IRoomSlot roomSlotService, ILayout layoutService)
        {
            _roomSlotService = roomSlotService;
            _layoutService = layoutService;
        }

        // GET: RoomSlot
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet, ActionName("Index")]
        public ActionResult IndexWithVersion(int? month, int? year, int? roomId)
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var output = _roomSlotService.GetAll();

            List<int> last10Years = new List<int>();
            int currentYear = DateTime.Now.Year;

            for (int i = currentYear; i < currentYear + 10; i++)
            {
                last10Years.Add(i);
            }

            ViewBag.LastTenYears = new SelectList(last10Years);

            return View(output);
        }

        // GET: Room/Create
        public ActionResult Create()
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var model = new CreateRoomSlotViewModel();

            List<int> last10Years = new List<int>();
            int currentYear = DateTime.Now.Year;

            for (int i = currentYear; i < currentYear + 10; i++)
            {
                last10Years.Add(i);
            }

            //var Room = _layoutService.GetAll();
            //SelectList RoomId = new SelectList(Room);

            ViewBag.LastTenYears = new SelectList(last10Years);
            //ViewData["roomId"] = RoomId;

            return View(model);
        }

        // POST: Room/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateRoomSlotViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isSuccess = _roomSlotService.GenerateRoomSlot(model.startMonth, model.endMonth, model.year, model.roomId);

                if (isSuccess == false)
                {
                    List<int> last10Years = new List<int>();
                    int currentYear = DateTime.Now.Year;

                    for (int i = currentYear; i < currentYear + 10; i++)
                    {
                        last10Years.Add(i);
                    }

                    ViewBag.LastTenYears = new SelectList(last10Years);

                    ViewBag.MyErrorMessage = "Failed Generate Room Slot. Room Slot already exist!";
                    return View();
                }
                else
                {

                    List<int> last10Years = new List<int>();
                    int currentYear = DateTime.Now.Year;

                    for (int i = currentYear; i < currentYear + 10; i++)
                    {
                        last10Years.Add(i);
                    }

                    ViewBag.LastTenYears = new SelectList(last10Years);

                    ViewBag.MyErrorMessage = "Success Generate Room Slot!";
                    return View();
                    //return RedirectToAction("Index");
                }
            }

            return View(model);
        }

        public ActionResult Filter()
        {
            //if (!SecurityHelper.IsUserInTeam("SupervisorInventory"))
            //    return RedirectToAction("AccessDenied", "Account");

            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            var model = new CreateRoomSlotViewModel();

            List<int> last10Years = new List<int>();
            int currentYear = DateTime.Now.Year;

            for (int i = currentYear; i < currentYear + 10; i++)
            {
                last10Years.Add(i);
            }

            ViewBag.LastTenYears = new SelectList(last10Years);

            return View(model);
        }

        //public ActionResult Delete(int id)
        //{
        //    //return View();
        //    _roomSlotService.Delete(id);
        //    return RedirectToAction("Index");
        //}

        // GET: Room/Delete/5
        public ActionResult Delete()
        {
            var strProfile = this.Session["UserProfile"];
            var strPass = this.Session["UserPass"];
            var IsLogin = this.Session["IsLogin"];
            var IsAdmin = this.Session["IsAdmin"];

            if (Convert.ToInt16(IsLogin) == 0 && Convert.ToInt16(IsAdmin) == 0)
                return RedirectToAction("AccessDenied", "Account");

            List<int> last10Years = new List<int>();
            int currentYear = DateTime.Now.Year;

            for (int i = currentYear; i < currentYear + 10; i++)
            {
                last10Years.Add(i);
            }

            ViewBag.LastTenYears = new SelectList(last10Years);

            //return View(model);

            return View();
        }

        // POST: Room/Delete/5
        [HttpPost]
        public JsonResult DeleteData(string month, string year, string day, string roomId)
        {
            try
            {
                // TODO: Add delete logic here
                _roomSlotService.Delete(month, year, day, roomId);
                //return RedirectToAction("Index");
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                //return View();
                return Json("Gagal", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetRoomSlot(int? month, int? year, int? roomId)
        {
            var roomSlotList = _roomSlotService.GetFilter(month, year, roomId);
            return Json(roomSlotList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWebConfigHome()
        {
            var configServer = System.Configuration.ConfigurationManager.AppSettings["ServerHome"].ToString();
            return Json(configServer, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteById(int id)
        {
            try
            {
                // TODO: Add delete logic here
                _roomSlotService.DeleteById(id);
                //return RedirectToAction("Index");
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                //return View();
                return Json("Gagal", JsonRequestBehavior.AllowGet);
            }
        }
    }
}